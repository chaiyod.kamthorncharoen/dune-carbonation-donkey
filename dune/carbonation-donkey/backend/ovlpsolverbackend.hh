// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:
#ifndef DUNE_RASPEN_OVLPSOLVERBACKEND_HH
#define DUNE_RASPEN_OVLPSOLVERBACKEND_HH

#include <dune/pdelab/backend/istl/ovlpistlsolverbackend.hh>

namespace Dune {
  namespace PDELab {

  template<class GFS, class S, class M, class X, class Y, class Z>
  class MatrixAdapterSubdomainSolve : public AssembledLinearOperator<M,X,Y>
  {
  public:
    //! export types

    typedef GFS GridFunctionSpace;
    typedef S Solver;

    typedef M matrix_type;
    typedef X domain_type;
    typedef Y range_type;
    typedef typename X::field_type field_type;

    //! define the category
    virtual SolverCategory::Category category() const
     {
       return SolverCategory::sequential;
     }

    //! constructor: just store a reference to a matrix
    explicit MatrixAdapterSubdomainSolve (const M& A, const GFS& gfs, S& solver, Y& pu, Y& py, const Y& g, Z& itp, Z& ity) 
      : _A_(A), gfs_(gfs), solver_(solver), pu_(pu), py_(py), g_(g), itp_(itp), ity_(ity) {}

    //! apply operator to x:  \f$ y = A(x) \f$
    virtual void apply (const X& x, Y& y) const
    {
      using Backend::native;
    
      // RHS
      native(_A_).mv(native(x),native(y));

      //cutoff border
        // old fashion
        for (auto ity = y.begin(), itp = py_.begin(); ity != y.end(); ity++, itp++) {
          if (*itp==0.0)
            *ity = 0.0;
        }
        // new fashion
        /*for (auto i_g = ity_.begin(), i_p = itp_.begin(); i_g!= ity_.end(); i_g++, i_p++){
          **i_g = 0.0;
          }*/
        

      //LinearSolve
      Y z(x);
      z = 0;
      solver_.apply(_A_, z, y, 1e-4);
      auto itz = z.begin();
      auto itp = pu_.begin();
        for (;itz != z.end(); itz++, itp++) {
          //if (*itp!=0)
          *itz = (*itz)*(*itp);} 
   
      Dune::PDELab::AddDataHandle<GFS,domain_type> adh_(gfs_,z);
          if (gfs_.gridView().comm().size()>1)
            gfs_.gridView().communicate(adh_,Dune::All_All_Interface,Dune::ForwardCommunication);

      y = z;
    }

    //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
    virtual void applyscaleadd (field_type alpha, const X& x, Y& y) const
    {
        Y temp(y);
        temp = 0.0;
        this->apply(x,temp);
        temp*=alpha;
        y=g_;
        y+=temp;
    }


    //! get matrix via *
    virtual const M& getmat () const
    {
      return _A_;
    }

  private:
        //using DUNE::Backend::Native;
        //using DUNE::Backend::native;
    const M& _A_;
    const GFS& gfs_;
    S& solver_;
    Y& pu_;
    Y& py_;
    const Y& g_;    // RHS Vector

    Z& itp_;
    Z& ity_;

  };

    // really new scalar product assuming at least overlap 1
    // uses unique partitioning of nodes for parallelization
    // define because the category does not match with my newton code
    template<class GFS, class X>
    class OverlappingNewtonScalarProduct
      : public Dune::ScalarProduct<X>
    {
    public:
      //! export types
      typedef X domain_type;
      typedef typename X::ElementType field_type;

      //! define the category
      virtual SolverCategory::Category category() const
      {
       return SolverCategory::sequential;
      }

      /*! \brief Constructor needs to know the grid function space
       */
      OverlappingNewtonScalarProduct (const GFS& gfs_, const ISTL::ParallelHelper<GFS>& helper_, const X& pu_)
        : gfs(gfs_), helper(helper_), pu(pu_)
      {}


      /*! \brief Dot product of two vectors.
        It is assumed that the vectors are consistent on the interior+border
        partition.
      */
      virtual field_type dot (const X& x, const X& y) const override
      {
        // do local scalar product on unique partition
        field_type sum = helper.disjointDot(x,y);

        // do global communication
        return gfs.gridView().comm().sum(sum);
      }

      /*! \brief Norm of a right-hand side vector.
        The vector must be consistent on the interior+border partition
      */
      virtual double norm (const X& x) const override
      {
        /*X z(x);
        auto itz = z.begin();
        auto itp = pu.begin();        
        for (; itz != z.end(); itz++, itp++) {
          if (*itp==0.0)
            *itz = 0.0;         
        }*/
        
        //return sqrt(static_cast<double>(this->dot(z,z)));
        return sqrt(static_cast<double>(this->dot(x,x)));
      }

    private:
      const GFS& gfs;
      const ISTL::ParallelHelper<GFS>& helper;
      const X& pu;
    };

    //! \} Overlapping Solvers

    //! \} group Backend

  } // namespace PDELab
} // namespace Dune

#endif
