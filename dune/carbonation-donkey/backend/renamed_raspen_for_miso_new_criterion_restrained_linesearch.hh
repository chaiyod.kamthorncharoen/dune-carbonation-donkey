// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_RESTRAINED_RASPEN_HH
#define DUNE_RESTRAINED_RASPEN_HH

#include <iostream>
#include <iomanip>
#include <cmath>
#include <memory>

#include <math.h>

#include <dune/common/exceptions.hh>
#include <dune/common/ios_state.hh>
#include <dune/common/timer.hh>
#include <dune/common/parametertree.hh>

#include <dune/pdelab/backend/solver.hh>

#include <dune/carbonation-donkey/backend/ovlpsolverbackend.hh>
#include <dune/carbonation-donkey/istl/solvers.hh>

namespace Dune
{
  namespace PDELab::Raspen
  {
    // Exception classes used in NewtonSolver
    class NewtonError : public Exception {};
    class NewtonDefectError : public NewtonError {};
    class NewtonLinearSolverError : public NewtonError {};
    class NewtonLineSearchError : public NewtonError {};
    class NewtonNotConverged : public NewtonError {};

    // Status information of Newton's method
    template<class RFType>
    struct NewtonResult : LinearSolverResult<RFType>
    {
      RFType first_defect;       // the first defect
      RFType defect;             // the final defect
      double assembler_time;     // Cumulative time for matrix assembly
      double linear_solver_time; // Cumulative time for linear solver
      double local_nl_solver_time; // Cumulative time for local nonlinear solver
      double global_linear_solver_time; // Cumulative time for global linear solver
      double line_search_inner1_time; // Cumulative time for line_search_inner1
      double line_search_all_time; // Cumulative time for all line searches
      int linear_solver_iterations; // Total number of linear iterations
      int linear_solver_iterations_1; // Total number of linear iterations
      int linear_solver_iterations_2; // Total number of linear iterations

      NewtonResult() :
        first_defect(0.0), defect(0.0), assembler_time(0.0),
        linear_solver_time(0.0), local_nl_solver_time(0.0),
        global_linear_solver_time(0.0), line_search_inner1_time(0.0),
        line_search_all_time(0.0), linear_solver_iterations(0),
        linear_solver_iterations_1(0), linear_solver_iterations_2(0) {}
    };

    template<class GOS, class TrlV, class TstV>
    class NewtonBase
    {
      typedef GOS GridOperator;
      typedef TrlV TrialVector;
      typedef TstV TestVector;

      typedef typename TestVector::ElementType RFType;
      typedef typename GOS::Traits::Jacobian Matrix;


    public:
      // export result type
      typedef NewtonResult<RFType> Result;

      void setVerbosityLevel(unsigned int verbosity_level, unsigned int targetToPrint=0)
      {
        targetToPrint_ = targetToPrint;
        if (gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==targetToPrint_)
        {
          verbosity_level_ = verbosity_level;
          GMRES_verbosity_level_ = 1;
        }
        else
        {
          verbosity_level_ = 0;
          GMRES_verbosity_level_ = 0;
        }        
        //verbosity_level_ = verbosity_level;
      }

      //! Set whether the jacobian matrix should be kept across calls to apply().
      void setKeepMatrix(bool b)
      {
        keep_matrix_ = b;
      }

      //! Return whether the jacobian matrix is kept across calls to apply().
      bool keepMatrix() const
      {
        return keep_matrix_;
      }

      //! Discard the stored Jacobian matrix.
      void discardMatrix()
      {
        if(A_)
          A_.reset();
      }

    protected:
      const GridOperator& gridoperator_;
      TrialVector *u_;
      TrialVector *v_;
      TrialVector *p_;
      //TrialVector *py_;
      //TrialVector *y_;
      //TrialVector *g_;
      std::shared_ptr<TrialVector> z_;
      std::shared_ptr<TrialVector> y_;
      std::shared_ptr<TrialVector> g_;
      std::shared_ptr<TrialVector> gz_;
      std::shared_ptr<TrialVector> gc_;       
      std::shared_ptr<TrialVector> py_;      
      std::shared_ptr<TrialVector> b_;
      //std::shared_ptr<TrialVector> p_;
      std::shared_ptr<TestVector> r_;
      std::shared_ptr<TestVector> r2_;
      std::shared_ptr<TestVector> r3_;
      std::shared_ptr<Matrix> A_;
      std::shared_ptr<Matrix> At_;
      Result res_inner1_;
      Result res_inner2_;
      Result res_;
      unsigned int verbosity_level_;
      unsigned int GMRES_verbosity_level_;
      int targetToPrint_;
      RFType prev_defect_inner1_;
      RFType prev_defect_inner2_;
      RFType prev_defect_;
      RFType linear_reduction_inner1_;
      RFType linear_reduction_inner2_;
      RFType linear_reduction_;
      bool reassembled_;
      RFType reduction_;
      RFType reduction_inner1_;
      RFType reduction_inner2_;
      RFType abs_limit_;
      unsigned int restart_;
      bool keep_matrix_;
      bool local_flag_=0;
      bool global_flag_;

      NewtonBase(const GridOperator& go, TrialVector& u)
        : gridoperator_(go)
        , u_(&u)
        , v_(0)
        , g_(0)
        , y_(0)
        , verbosity_level_(1)
        , keep_matrix_(true)
      {
        if (gridoperator_.trialGridFunctionSpace().gridView().comm().rank()>0)
          verbosity_level_ = 0;
      }

      NewtonBase(const GridOperator& go)
        : gridoperator_(go)
        , u_(0)
        , v_(0)
        //, g_(0)
        , y_(0)
        , verbosity_level_(1)
        , keep_matrix_(true)
      {
        if (gridoperator_.trialGridFunctionSpace().gridView().comm().rank()>0)
          verbosity_level_ = 0;
      }

      NewtonBase(const GridOperator& go, TrialVector& u, TrialVector& v)
        : gridoperator_(go)
        , u_(&u)
        , v_(&v)
        //, g_(0)
        , y_(0)
        , verbosity_level_(1)
        , keep_matrix_(true)
      {
        if (gridoperator_.trialGridFunctionSpace().gridView().comm().rank()>0)
          verbosity_level_ = 0;
      }

      NewtonBase(const GridOperator& go, TrialVector& u, TrialVector& v, TrialVector& p)
        : gridoperator_(go)
        , u_(&u)
        , v_(&v)
        , p_(&p)
        //, g_(0)
        , y_(0)
        , verbosity_level_(1)
        , keep_matrix_(true)
      {
        if (gridoperator_.trialGridFunctionSpace().gridView().comm().rank()>0)
          verbosity_level_ = 0;
      }

      virtual ~NewtonBase() { }

      virtual bool terminate() = 0;
      virtual void prepare_step(Matrix& A, TestVector& r) = 0;
      virtual void line_search(TrialVector& z, TestVector& r) = 0;
      virtual void defect(TestVector& r) = 0;

      virtual bool terminate_inner1() = 0;
      virtual void prepare_step_inner1(Matrix& A, TestVector& r) = 0;
      virtual void line_search_inner1(TrialVector& z, TestVector& r) = 0;
      virtual void defect_inner1(TestVector& r2) = 0;

      virtual bool terminate_inner2() = 0;
      virtual void prepare_step_inner2(Matrix& A, TestVector& r) = 0;
      virtual void line_search_inner2(TrialVector& z, TestVector& r) = 0;
      virtual void defect_inner2(TestVector& r3) = 0;

      //virtual bool terminate_outer() = 0;
      //virtual void prepare_step_outer(Matrix& A, TestVector& r) = 0;
      virtual void line_search_outer(TrialVector& z, TestVector& r) = 0;
      virtual void defect_outer(TestVector& r1) = 0;

    }; // end class NewtonBase

    template<class GOS, class S, class TrlV, class TstV>
    class NewtonSolver : public virtual NewtonBase<GOS,TrlV,TstV>
    {
      typedef S Solver;
      typedef GOS GridOperator;
      typedef TrlV TrialVector;
      typedef TstV TestVector;

      typedef typename TestVector::ElementType RFType;
      typedef typename GOS::Traits::Jacobian Matrix;
      typedef typename GOS::Traits::TrialGridFunctionSpace GFS;

    public:
      typedef NewtonResult<RFType> Result;

      NewtonSolver(const GridOperator& go, TrialVector& u_, Solver& solver)
        : NewtonBase<GOS,TrlV,TstV>(go,u_)
        , solver_(solver)
        , result_valid_(false)
      {}

      NewtonSolver(const GridOperator& go, Solver& solver)
        : NewtonBase<GOS,TrlV,TstV>(go)
        , solver_(solver)
        , result_valid_(false)
      {}

      NewtonSolver(const GridOperator& go, TrialVector& u_, TrialVector& v_, Solver& solver)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_)
        , solver_(solver)
        , result_valid_(false)
      {}

      NewtonSolver(const GridOperator& go, TrialVector& u_, TrialVector& v_, TrialVector& p_, Solver& solver)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_,p_)
        , solver_(solver)
        , result_valid_(false)
      {}

      void apply();

      void apply(TrialVector& u_);

      void apply(TrialVector& u_, TrialVector& v_);

      void apply_precon();

      const Result& result() const
      {
        if (!result_valid_)
          DUNE_THROW(NewtonError,
                     "NewtonSolver::result() called before NewtonSolver::solve()");
        return this->res_;
      }

      //! Set number for RestartedGMRes.
      void setRestart(unsigned int restart)
      {
        this->restart_ = restart;
      }

      //! Set whether to use the maximum norm for stopping criteria.
      void setUseMaxNorm(bool b)
      {
        _useMaxNorm = b;
      }

      void updateDefect(TrialVector& solution)
      {
        *(this->r_)= 0.;
        this->gridoperator_.residual(solution, *(this->r_));
        // Use the maximum norm as a stopping criterion. This helps loosen the tolerance
        // when solving for stationary solutions of nonlinear time-dependent problems.
        // The default is to use the Euclidean norm (in the else-block) as before
        if (_useMaxNorm){
          auto rankMax = Backend::native(*(this->r_)).infinity_norm();
          this->res_.defect = this->gridoperator_.testGridFunctionSpace().gridView().comm().max(rankMax);
        }
        else{
          auto rankNorm = this->solver_.norm(*(this->r_));
          rankNorm *= rankNorm;
          this->res_.defect = this->gridoperator_.testGridFunctionSpace().gridView().comm().sum(rankNorm);
          this->res_.defect = sqrt(this->res_.defect);
        }
      }

    protected:
      virtual void defect_inner1(TestVector& r)
      {
        r = 0.0;                                        // TODO: vector interface
        this->gridoperator_.residual(*this->g_, r);
        if (_useMaxNorm)
        {
          this->res_inner1_.defect = Backend::native(r).infinity_norm();
        }
        else
          this->res_inner1_.defect = this->solver_.norm(r);     // TODO: solver interface
        //double temp = this->solver_.norm(*this->z_);

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().size()>1) 
          this->res_inner1_.defect = this->gridoperator_.trialGridFunctionSpace().gridView().comm().sum(this->res_inner1_.defect);*/

        using Backend::native;
        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)        {
        std::cout << "zz " << std::endl;      
        Dune::printvector(std::cout, native(*this->z_),"solution","row");
        std::cout << "gg " << std::endl;  
      Dune::printvector(std::cout, native(*this->g_),"solution","row");
        std::cout << "rr " << std::endl;      
      Dune::printvector(std::cout, native(*this->r_),"solution","row");}*/
        //std::cout << "print norm of z " << temp << std::endl;
        if (!std::isfinite(this->res_inner1_.defect))
          this->local_flag_=1;
          //DUNE_THROW(NewtonDefectError,
          //           "NewtonSolver::defect(): Non-linear defect is NaN or Inf");
      }

    protected:
      virtual void defect_inner2(TestVector& r2)
      {
        if (_useMaxNorm)
        {
          auto rankMax = Backend::native(r2).infinity_norm();
          this->res_inner2_.defect = this->gridoperator_.testGridFunctionSpace().gridView().comm().max(rankMax);
        }
        else
        {
          this->res_inner2_.defect = this->solver_.norm(r2);      // TODO: solver interface
          if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().size()>1) 
            this->res_inner2_.defect = this->gridoperator_.trialGridFunctionSpace().gridView().comm().sum(this->res_inner2_.defect);
        }
        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)        {
        std::cout << "g " << std::endl;     
        Dune::printvector(std::cout, this->g_->base(),"solution","row");
        std::cout << "z " << std::endl;  
      Dune::printvector(std::cout, this->z_->base(),"solution","row");
        std::cout << "y " << std::endl;      
      Dune::printvector(std::cout, this->y_->base(),"solution","row");
        std::cout << "r2 " << std::endl;      
      Dune::printvector(std::cout, r2.base(),"solution","row");}*/
        if (!std::isfinite(this->res_inner2_.defect))
          DUNE_THROW(NewtonDefectError,
                     "NewtonSolver::defect(): Non-linear defect is NaN or Inf");
      }

      virtual void defect_outer(TestVector& r3)
      {
        if (_useMaxNorm)
        {
          auto rankMax = Backend::native(r3).infinity_norm();
          this->res_.defect = this->gridoperator_.testGridFunctionSpace().gridView().comm().max(rankMax);
        }
        else
        {
          this->res_.defect = this->solver_.norm(r3);      // TODO: solver interface
          if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().size()>1) 
            this->res_.defect = this->gridoperator_.trialGridFunctionSpace().gridView().comm().sum(this->res_.defect);
        }
        if (!std::isfinite(this->res_.defect))
          DUNE_THROW(NewtonDefectError,
                     "NewtonSolver::defect(): Non-linear defect is NaN or Inf");
      }

      virtual void defect(TestVector& r3)
      {
        r3 = 0.0;                                        // TODO: vector interface
        this->gridoperator_.residual(*this->u_, r3);
        if (_useMaxNorm)
        {
          auto rankMax = Backend::native(r3).infinity_norm();
          this->res_.defect = this->gridoperator_.testGridFunctionSpace().gridView().comm().max(rankMax);
        }
        else
        {
          this->res_.defect = this->solver_.norm(r3);     // TODO: solver interface
          //double temp = this->solver_.norm(*this->z_);
          if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().size()>1) 
            this->res_.defect = this->gridoperator_.trialGridFunctionSpace().gridView().comm().sum(this->res_.defect);
        }
        if (!std::isfinite(this->res_.defect))
          DUNE_THROW(NewtonDefectError,
                     "NewtonSolver::defect(): Non-linear defect is NaN or Inf");
      }

    private:
      void linearSolve(Matrix& A, TrialVector& z, TestVector& r) const
      {
        if (this->verbosity_level_ >= 4)
          std::cout << "      Solving linear system..." << std::endl;
        z = 0.0;                                        // TODO: vector interface
        this->solver_.apply(A, z, r, this->linear_reduction_inner1_);        // TODO: solver interface

        ios_base_all_saver restorer(std::cout); // store old ios flags

        if (!this->solver_.result().converged)                 // TODO: solver interface
          DUNE_THROW(NewtonLinearSolverError,
                     "NewtonSolver::linearSolve(): Linear solver did not converge "
                     "in " << this->solver_.result().iterations << " iterations");
        if (this->verbosity_level_ >= 4)
          std::cout << "          linear solver iterations:     "
                    << std::setw(12) << solver_.result().iterations << std::endl
                    << "          linear defect reduction:      "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << solver_.result().reduction << std::endl;
      }

      void linearSolve2(Matrix& A, TrialVector& z, TestVector& r) const
      {
        if (this->verbosity_level_ >= 4)
          std::cout << "      Solving linear system..." << std::endl;
        z = 0.0;                                        // TODO: vector interface
        this->solver_.apply(A, z, r, this->reduction_inner2_);        // TODO: solver interface

        if (this->verbosity_level_ >= 4)
        ios_base_all_saver restorer(std::cout); // store old ios flags

        if (!this->solver_.result().converged)                 // TODO: solver interface
          DUNE_THROW(NewtonLinearSolverError,
                     "NewtonSolver::linearSolve(): Linear solver did not converge "
                     "in " << this->solver_.result().iterations << " iterations");
        if (this->verbosity_level_ >= 4)
          if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
          std::cout << "          linear solver iterations:     "
                    << std::setw(12) << solver_.result().iterations << std::endl
                    << "          linear defect reduction:      "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << solver_.result().reduction << std::endl;
      }

      Solver& solver_;
      bool result_valid_;
      bool _useMaxNorm = false;
    }; // end class NewtonSolver

    template<class GOS, class S, class TrlV, class TstV>
    void NewtonSolver<GOS,S,TrlV,TstV>::apply(TrialVector& u, TrialVector& v)
    {
      this->u_ = &u;
      this->v_ = &v;
      apply_precon();
    }

    template<class GOS, class S, class TrlV, class TstV>
    void NewtonSolver<GOS,S,TrlV,TstV>::apply(TrialVector& u)
    {
      this->u_ = &u;
      apply();
    }

    template<class GOS, class S, class TrlV, class TstV>
    void NewtonSolver<GOS,S,TrlV,TstV>::apply_precon()
    {

      if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
      std::cout << "Apply RASPEN" << std::endl;

      //for outer ieration
      this->res_.iterations = 0;
      this->res_.converged = false;
      this->res_.reduction = 1.0;
      this->res_.conv_rate = 1.0;
      this->res_.elapsed = 0.0;
      this->res_.assembler_time = 0.0;
      this->res_.global_linear_solver_time = 0.0;
      this->res_.local_nl_solver_time = 0.0;
      this->res_.linear_solver_time = 0.0;
      this->res_.line_search_inner1_time = 0.0;
      this->res_.line_search_all_time = 0.0;
      this->res_.linear_solver_iterations = 0;
      this->res_.linear_solver_iterations_1 = 0;
      this->res_.linear_solver_iterations_2 = 0;

      //for local subproblems
      this->res_inner1_.iterations = 0;
      this->res_inner1_.converged = false;
      this->res_inner1_.reduction = 1.0;
      this->res_inner1_.conv_rate = 1.0;
      this->res_inner1_.elapsed = 0.0;
      this->res_inner1_.assembler_time = 0.0;
      this->res_inner1_.linear_solver_time = 0.0;
      this->res_inner1_.linear_solver_iterations = 0;

      //for global linear system (subdomain computation needed)
      this->res_inner2_.iterations = 0;
      this->res_inner2_.converged = false;
      this->res_inner2_.reduction = 1.0;
      this->res_inner2_.conv_rate = 1.0;
      this->res_inner2_.elapsed = 0.0;
      this->res_inner2_.assembler_time = 0.0;
      this->res_inner2_.linear_solver_time = 0.0;
      this->res_inner2_.linear_solver_iterations = 0;

      this->local_flag_=0;
      this->global_flag_=0;

      //std::cout << this->gridoperator_.testGridFunctionSpace().size() << std::endl;
      //std::cout << this->gridoperator_.trialGridFunctionSpace().size() << std::endl;

      result_valid_ = true;
      Timer timer;

          //test print file
          std::string name = "result from processor_";
          name += std::to_string(this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank());
          name += ".txt";
          static std::ofstream tfile;
          tfile.open (name, std::ofstream::out | std::ofstream::app);
          //tfile << this->res_inner1_.defect << std::endl;
          //tfile.close();
          
      try
        {
          if(!this->r_) {
            //std::cout << "=== Setting up residual vector ..." << std::endl;
            this->r_ = std::make_shared<TestVector>(this->gridoperator_.testGridFunctionSpace());         //global nonlinear residual
            this->r2_ = std::make_shared<TestVector>(this->gridoperator_.testGridFunctionSpace());         //local residual            
            this->r3_ = std::make_shared<TestVector>(this->gridoperator_.testGridFunctionSpace());         //global linear (global criterion needed)
          }
          // residual calculation in member function "defect":
          //--------------------------------------------------
          // - set residual vector to zero
          // - calculate new residual
          // - store norm of residual in "this->res_.defect"

          if(!this->A_) {
            // std::cout << "==== Setting up jacobian matrix ... " << std::endl;
            this->A_ = std::make_shared<Matrix>(this->gridoperator_);
            
          }
          if(!this->z_) {
            // std::cout << "==== Setting up correction vector ... " << std::endl;
            this->z_ = std::make_shared<TrialVector>(this->gridoperator_.trialGridFunctionSpace());
            this->g_ = std::make_shared<TrialVector>(this->gridoperator_.trialGridFunctionSpace());
            this->y_ = std::make_shared<TrialVector>(this->gridoperator_.trialGridFunctionSpace());
            this->gz_ = std::make_shared<TrialVector>(this->gridoperator_.trialGridFunctionSpace());
            this->gc_ = std::make_shared<TrialVector>(this->gridoperator_.trialGridFunctionSpace());
            this->py_ = std::make_shared<TrialVector>(this->gridoperator_.trialGridFunctionSpace());
            this->b_ = std::make_shared<TrialVector>(this->gridoperator_.trialGridFunctionSpace());
          }

      //typedef typename GOS::Traits::TrialGridFunctionSpace GFS;
      typedef Dune::PDELab::AddDataHandle<GFS,TrlV> ADDDH;
      ADDDH adddhg(this->gridoperator_.trialGridFunctionSpace(), *this->g_);
      ADDDH adddhz(this->gridoperator_.trialGridFunctionSpace(), *this->z_);
      ADDDH adddhy(this->gridoperator_.trialGridFunctionSpace(), *this->y_);
      ADDDH adddhpy(this->gridoperator_.trialGridFunctionSpace(), *this->py_);
      ADDDH adddhb(this->gridoperator_.trialGridFunctionSpace(), *this->b_);

        using Backend::Native;
        using Backend::native;

          // Initialize vector to 1
          *this->py_ = 1.0;

          if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().size()>1)
          this->gridoperator_.trialGridFunctionSpace().gridView().communicate(adddhpy,Dune::All_All_Interface,Dune::ForwardCommunication);

          // Divide 1/x
          for (auto iter = this->py_->begin(); iter != this->py_->end(); iter++) {
            if (*iter > 0)
              *iter = 1.0 / *iter;
          }

        // print partition of unity for global solution
        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
        std::cout << "partition of unity for global solution " << std::endl;  
      Dune::printvector(std::cout, this->py_->base(),"solution","row");}*/


          //*this->z_ = *this->v_;
          //*this->v_ = *this->z_;
          //this->y_ = 0.0;

            //for (typename TrialVector::iterator it = this->u_->begin(),end_it = this->u_->end(), itt = this->g_->begin();it != end_it;++it, ++itt)
            //*itt = *it;

          //this->defect_inner1(*this->r_);
          //this->defect_inner2(*this->r2_);
          //this->defect(*this->r3_);
          
          //this->res_.first_defect = this->res_.defect;
          //this->prev_defect_inner1_ = this->res_inner1_.defect;
          //this->prev_defect_inner2_ = this->res_inner2_.defect;
          //this->prev_defect_ = this->res_.defect;

          /*if (this->verbosity_level_ >= 2)
            {
              // store old ios flags
              ios_base_all_saver restorer(std::cout);
              std::cout << "  Initial defect: "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << this->res_.defect << std::endl;

              tfile << "  Initial defect: "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << this->res_.defect << std::endl;
            }*/

          // new partition of unity handle
          std::vector<decltype(this->p_->begin())> itr_p0;    // collect all elements of part_unity that are 0
          std::vector<decltype(this->p_->begin())> itr_p1;    // collect all elements of part_unity that are not 1
          std::vector<decltype(this->p_->begin())> itr_g0;    // collect all elements of g in itr_p0
          std::vector<decltype(this->p_->begin())> itr_g1;    // collect all elements of g in itr_p1
          std::vector<decltype(this->p_->begin())> itr_b0;    // collect all elements of b in itr_p0 (might not be used)
          std::vector<decltype(this->p_->begin())> itr_b1;    // collect all elements of b in itr_p1 (might not be used)
          std::vector<decltype(this->p_->begin())> itr_u0;    // collect all constraint elements in itr_p0
          std::vector<decltype(this->p_->begin())> itr_v0;    // collect all constraint elements in itr_p0
          std::vector<decltype(this->p_->begin())> itr_v;     // collect all constraint elements
          std::vector<decltype(this->p_->begin())> itr_gv;    // collect all elements of g of constraint elements
          std::vector<int> itr_con;    // collect all elements of g of constraint elements
          int cnt = 0;

          for (const auto& col : this->gridoperator_.constraintsU()){
            itr_con.push_back(int(col.first[0]));
          }
          
          // sort the index in itr_con
          std::sort(itr_con.begin(),itr_con.end()); // sort
          itr_con.erase(std::unique(itr_con.begin(), itr_con.end()), itr_con.end()); // remove duplicates

          /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){          
          for (auto i : itr_con)
            std::cout << "constraints index " << i << std::endl;
          }*/

          for (auto itp = this->p_->begin(), itg = this->g_->begin(), itv = this->v_->begin(), itb = this->b_->begin(), itu = this->u_->begin(); itp != this->p_->end(); itp++, itg++, itv++, itb++, itu++, cnt++){
            if (*itp == 0.0){
              itr_p0.push_back(itp);
              itr_g0.push_back(itg);
              itr_b0.push_back(itb);
              itr_v0.push_back(itv);
              itr_u0.push_back(itu);
              //std::cout << &itp << " is a reference of reference of element which is equal to 0 in partition of unity" << std::endl;
              //std::cout << **&itp << " is a value reference of element which is equal to 0 in partition of unity" << std::endl;
              //std::cout << *&itp << " is a reference of element which is equal to 0 in partition of unity" << std::endl;
            }
            if (*itp != 1.0){
              itr_p1.push_back(itp);
              itr_g1.push_back(itg);
              itr_b1.push_back(itb);
            }
            if (!itr_con.empty() && cnt == itr_con.front()){
              itr_v .push_back(itv);
              itr_gv.push_back(itg);
              itr_con.erase(itr_con.begin());
            }
            
          }

          /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
            std::cout << "print all elements which are not equal to 1 in partition of unity" << std::endl;
            for (auto i = itr_p1.begin(); i!= itr_p1.end(); i++){
              std::cout << **i << std::endl;
            }

            std::cout << "print all elements of g in itr_p1" << std::endl;
            for (auto i: itr_g1){
              std::cout << *i << std::endl;
            }

          }*/

          /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
            std::cout << "print partition of unity" << std::endl;
            for (auto itp = this->p_->begin(); itp != this->p_->end(); itp++){
              std::cout << *itp << std::endl;}
          }    */  

          // print partition of unity for global solution
          /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
            std::cout << "print partition of unity" << std::endl;  
          Dune::printvector(std::cout, native(*this->p_),"solution","row");}*/

          /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==3){
            std::cout << "check boundary values" << std::endl;  
          Dune::printvector(std::cout, native(*this->v_),"solution","row");}*/

          // main loop
          this->defect(*this->r_);
          this->res_.first_defect = this->res_.defect;
          this->prev_defect_ = this->res_.defect;

          do{
                         
          if (this->verbosity_level_ >= 3){
            if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
              std::cout << "  Global Solve iteration " << this->res_.iterations
                      << " --------------------------------" << std::endl;}
          }
            tfile << "  Global Solve iteration " << this->res_.iterations
                          << " --------------------------------" << std::endl;

          *this->g_ = *this->u_;

          this->defect_inner1(*this->r_);
          this->res_inner1_.first_defect = this->res_inner1_.defect;
          this->prev_defect_inner1_ = this->res_inner1_.defect;

          if (this->verbosity_level_ >= 2)
            {
              // store old ios flags
              ios_base_all_saver restorer(std::cout);
              if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){

              std::cout << "  Initial defect: "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << this->res_inner1_.defect << std::endl;}
            }

              tfile << "  Initial defect: "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << this->res_inner1_.defect << std::endl;

          // inner iteration 1
              
          this->res_inner1_.iterations = 0;
          Timer local_nl_solve_timer;

          try{
            while (!this->terminate_inner1()){
                         
              if (this->verbosity_level_ >= 3){
                if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)         
                std::cout << "  Local Solve iteration " << this->res_inner1_.iterations
                          << " --------------------------------" << std::endl;}
                tfile << "  Local Solve iteration " << this->res_inner1_.iterations
                          << " --------------------------------" << std::endl;

              Timer assembler_timer;
              try
                {
                  // jacobian calculation in member function "prepare_step"
                  //-------------------------------------------------------
                  // - if above reassemble threshold
                  //   - set jacobian to zero
                  //   - calculate new jacobian
                  // - set linear reduction
                  this->prepare_step_inner1(*this->A_,*this->r_);
                }
              catch (...)
                {
                  this->res_.assembler_time += assembler_timer.elapsed();
                  throw;
                }
              double assembler_time = assembler_timer.elapsed();
              this->res_.assembler_time += assembler_time;
              if (this->verbosity_level_ >= 3){
                if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
                std::cout << "      matrix assembly time:             "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << assembler_time << std::endl;}
                tfile << "      matrix assembly time:             "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << assembler_time << std::endl;

              Timer linear_solver_timer;
              try
                {
                  // solution of linear system in member function "linearSolve"
                  //-----------------------------------------------------------
                  // - set initial guess for correction z to zero
                  // - call linear solver
                  this->linearSolve(*this->A_, *this->z_, *this->r_);
                }
              catch (...)
                {
                  this->res_.linear_solver_time += linear_solver_timer.elapsed();
                  this->res_.linear_solver_iterations += this->solver_.result().iterations;
                  throw;
                }
              double linear_solver_time = linear_solver_timer.elapsed();
              this->res_.linear_solver_time += linear_solver_time;
              this->res_.linear_solver_iterations += this->solver_.result().iterations;

              //*this->v_ = *this->z_;

              Timer line_search_inner1_timer;
              try
                {
                  // line search with correction z
                  // the undamped version is also integrated in here
                  this->line_search_inner1(*this->z_, *this->r_);
                }
              catch (NewtonLineSearchError&)
                {
                  double line_search_inner1_time = line_search_inner1_timer.elapsed();
                  this->res_.line_search_inner1_time += line_search_inner1_time;
                  this->res_.line_search_all_time += line_search_inner1_time;
                  if (this->reassembled_)
                    throw;
                  if (this->verbosity_level_ >= 3){
                    std::cout << "      line search failed - trying again with reassembled matrix" << std::endl;}
                    tfile << "      line search failed - trying again with reassembled matrix" << std::endl;
                  continue;
                }
              double line_search_inner1_time = line_search_inner1_timer.elapsed();
              this->res_.line_search_inner1_time += line_search_inner1_time;
              this->res_.line_search_all_time += line_search_inner1_time;

              this->res_inner1_.reduction = this->res_inner1_.defect/this->res_inner1_.first_defect;
              //this->res2_.reduction = this->res2_.defect/this->res2_.first_defect;
              this->res_inner1_.iterations++;
              this->res_inner1_.conv_rate = std::pow(this->res_inner1_.reduction, 1.0/this->res_inner1_.iterations);

              // store old ios flags
              if (this->verbosity_level_ >= 3) ios_base_all_saver restorer(std::cout);


              if (this->verbosity_level_ >= 3){
                if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
                std::cout << "      linear solver time:               "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << linear_solver_time << std::endl
                          << "      defect reduction (this iteration):"
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.defect/this->prev_defect_inner1_ << std::endl
                          << "      defect reduction (total):         "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.reduction << std::endl
                          << "      new defect (inner 1):               "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.defect << std::endl;}

                tfile << "      linear solver time:               "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << linear_solver_time << std::endl
                          << "      defect reduction (this iteration):"
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.defect/this->prev_defect_inner1_ << std::endl
                          << "      defect reduction (total):         "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.reduction << std::endl
                          << "      new defect (inner 1):               "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.defect << std::endl;
              if (this->verbosity_level_ == 2)
                std::cout << "  Newton iteration " << std::setw(2) << this->res_inner1_.iterations
                          << ".  New defect: "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.defect
                          << ".  Reduction (this): "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.defect/this->prev_defect_inner1_
                          << ".  Reduction (total): "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner1_.reduction << std::endl;
            

              } // end while inner1
            } // end try
            catch (...)
            {
              // set local flag to signal that this rank did not converge
              this->local_flag_ = 1;
            }

            double local_nl_solve_time = local_nl_solve_timer.elapsed();
            this->res_.local_nl_solver_time += local_nl_solve_time;

            // MPI_Barrier(MPI_COMM_WORLD);
            // find out whether all ranks converged
            MPI_Allreduce(&(this->local_flag_),&(this->global_flag_),1,MPI_C_BOOL,MPI_LOR,MPI_COMM_WORLD);

            if (this->global_flag_){
              this->res_.linear_solver_iterations_1 += this->res_inner1_.iterations;
              tfile.close();
              DUNE_THROW(NewtonNotConverged,
              "NewtonTerminate::terminate_inner1(): Maximum iteration count reached");
            }
            
            //Update inner1 iteration to res_ vector in order to print in implicitonestep.hh
            this->res_.linear_solver_iterations_1 += this->res_inner1_.iterations;

            //partition of unity
            // old fashion
            /*for (auto itg = this->g_->begin(), itp = this->p_->begin(); itg != this->g_->end(); itg++, itp++) {
              //if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
                //std::cout << "part " << *itp << std::endl;
              *itg = (*itg)*(*itp);
            }*/

            // new fashion
            for (auto i_g = itr_g1.begin(), i_p = itr_p1.begin(); i_g!= itr_g1.end(); i_g++, i_p++){
              **i_g = (**i_g)*(**i_p);
            }
            

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){       
        std::cout << "gg after applying partition of unity " << std::endl;      
        Dune::printvector(std::cout, this->g_->base(),"solution","row");}*/

            //communication
          if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().size()>1)
          this->gridoperator_.trialGridFunctionSpace().gridView().communicate(adddhg,Dune::All_All_Interface,Dune::ForwardCommunication);
            
            //cutoff the boundary
            // old fashion
            /*for (auto itg = this->g_->begin(), itp = this->p_->begin(); itg != this->g_->end(); itg++, itp++){
              if (*itp == 0.0)
                *itg = 0.0;
            }*/
            // new fashion
            for (auto i_g = itr_g0.begin(), i_p = itr_p0.begin(); i_g!= itr_g0.end(); i_g++, i_p++){
              **i_g = 0.0;
            }

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){       
        std::cout << "gg after communication " << std::endl;      
        Dune::printvector(std::cout, this->g_->base(),"solution","row");}*/

            //*this->g_ -= *this->v_; //add the correct boundary // old fashion

            for (auto i_g = itr_gv.begin(), i_v = itr_v.begin(); i_g!= itr_gv.end(); i_g++, i_v++){
              **i_g = (**i_g)-(**i_v);  //add the correct boundary // new fashion
            }

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){       
        std::cout << "gg after update boundary " << std::endl;      
        Dune::printvector(std::cout, this->g_->base(),"solution","row");}*/

        // g = u-g
        for (auto itg = this->g_->begin(), itb = this->b_->begin(), itu = this->u_->begin(); itg != this->g_->end(); itg++, itu++, itb++){
            *itb = *itu - *itg;
          }

            //*this->g_ -= *this->u_;       // g = g-u

            //partition of unity
            // old fashion
            for (auto itg = this->g_->begin(), itb = this->b_->begin(), itp = this->p_->begin(); itg != this->g_->end(); itg++, itb++, itp++) {
              *itg = (*itb)*(*itp);
            }
            // new fashion (cannot use here)
            //for (auto i_g = itr_g1.begin(), i_p = itr_p1.begin(), i_b = itr_b1.begin(); i_g!= itr_g1.end(); i_g++, i_p++, i_b++){
            //  **i_g = (**i_b)*(**i_p);
            //}


            //communication
          if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().size()>1)
          this->gridoperator_.trialGridFunctionSpace().gridView().communicate(adddhg,Dune::All_All_Interface,Dune::ForwardCommunication);

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){       
        std::cout << "gg before global linear " << std::endl;     
        Dune::printvector(std::cout, this->g_->base(),"solution","row");
}*/

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){       
        std::cout << "bb before global linear " << std::endl;     
        Dune::printvector(std::cout, this->b_->base(),"solution","row");
}*/

            this->defect_inner2(*this->b_);
            this->prev_defect_inner2_ = this->res_inner2_.defect;
            this->res_inner2_.first_defect = this->res_inner2_.defect;

            //inner iteration 2

            this->res_inner2_.iterations = 0;
            *this->z_ = 0.0;
            *this->y_ = 0.0;
            *this->gz_ = 0.0;

        this->prepare_step_inner2(*this->A_,*this->r_);

        typedef Dune::PDELab::MatrixAdapterSubdomainSolve<GFS,Solver, Matrix,TrialVector,TrialVector, std::vector<decltype(this->p_->begin())>> OPE;
        OPE ope(*this->A_, this->gridoperator_.trialGridFunctionSpace(), this->solver_, *this->p_, *this->p_, *this->g_, itr_p0, itr_g0);
        typedef OverlappingNewtonScalarProduct<GFS,TrialVector> PSP;
        Dune::PDELab::ISTL::ParallelHelper<GFS> helper(this->gridoperator_.trialGridFunctionSpace());
        PSP psp(this->gridoperator_.trialGridFunctionSpace(),helper,*this->p_);
        Dune::Richardson<TrialVector,TrialVector> prec;
        //Dune::RestartedGMResSolverForRASPEN<TrialVector,TrialVector,TrialVector> loop(ope,psp,prec,this->linear_reduction_inner2_,this->restart_,2000,this->verbosity_level_);  // set verb = 0, to avoid garbage result.
        Dune::RestartedGMResSolver<TrialVector,TrialVector,TrialVector> loop(ope,psp,prec,this->linear_reduction_inner2_,this->restart_,2000,this->verbosity_level_);  // set verb = 0, to avoid garbage result. // not using psp
        //Dune::LoopSubdomainSolver<TrialVector> loop(ope,psp,prec,this->reduction_inner2_,2000,2);
        Dune::InverseOperatorResult stat;
        loop.apply(*this->y_,*this->g_,this->linear_reduction_inner2_,stat);


      // fill statistics
      this->res_inner2_.iterations = stat.iterations;
      this->res_inner2_.reduction = stat.reduction;
      this->res_inner2_.conv_rate  = stat.conv_rate;
      this->res_inner2_.elapsed = stat.elapsed;


              if (this->verbosity_level_ >= 2){
              if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
                std::cout << "  Global Linear iteration " << this->res_inner2_.iterations
                          << " --------------------------------" << std::endl;}
                tfile << "  Global Linear iteration " << this->res_inner2_.iterations
                          << " --------------------------------" << std::endl;

              if (this->verbosity_level_ >= 2){
                if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
                std::cout << "      defect reduction (total):         "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner2_.reduction << std::endl
                          << "      conv rate:                       "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner2_.conv_rate << std::endl;}

                tfile << "      defect reduction (total):         "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner2_.reduction << std::endl
                          << "      conv rate:                       "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_inner2_.conv_rate << std::endl;
              // if (this->verbosity_level_ == 2)
              //   if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
              //   std::cout << "  Global Linear iteration " << std::setw(2) << this->res_inner2_.iterations
              //             << ".  New defect: "
              //             << std::setw(12) << std::setprecision(4) << std::scientific
              //             << this->res_inner2_.defect
              //             << ".  Reduction (this): "
              //             << std::setw(12) << std::setprecision(4) << std::scientific
              //             << this->res_inner2_.defect/this->prev_defect_inner2_
              //             << ".  Reduction (total): "
              //             << std::setw(12) << std::setprecision(4) << std::scientific
              //             << this->res_inner2_.reduction << std::endl;

        /*if (this->res_.iterations == 0){    //old criterion
          this->defect_outer(*this->b_);
          this->res_.first_defect = this->res_.defect;
          this->prev_defect_ = this->res_.defect;*/

          if (this->verbosity_level_ >= 2)
            {

            if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
            // store old ios flags
            ios_base_all_saver restorer(std::cout);

            std::cout << "  Initial Global Nonlinear defect: "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << this->res_.defect << std::endl;}
            }

            tfile << "  Initial Global Nonlinear defect: "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << this->res_.defect << std::endl;
          

          //}

        //Update inner2 iteration and elapsed() to res_ vector in order to print in implicitonestep.hh
        this->res_.linear_solver_iterations_2 += this->res_inner2_.iterations;
        this->res_.global_linear_solver_time += this->res_inner2_.elapsed;

        //Update global solution
        //cutoff y boundary
            /*for (auto ity = this->y_->begin(), itp = this->p_->begin(); ity != this->y_->end(); ity++, itp++) {
              if (*itp==0.0)
                *ity = 0.0;         
            }*/


        //Partition of unity of y and Communicate y
        /*for (auto ity = this->y_->begin(), itpy = this->p_->begin(); ity != this->y_->end(); ity++, itpy++) {
          if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_) std::cout << "party " << *itpy << std::endl;
          *ity = (*ity)*(*itpy);
        }*/

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
        std::cout << "yy after partition of unity" << std::endl;  
      Dune::printvector(std::cout, this->y_->base(),"solution","row");}*/

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().size()>1)
        this->gridoperator_.trialGridFunctionSpace().gridView().communicate(adddhy,Dune::All_All_Interface,Dune::ForwardCommunication);*/

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
        std::cout << "yy after communicate" << std::endl;  
      Dune::printvector(std::cout, this->y_->base(),"solution","row");}*/

        Timer line_search_outer_timer;
        try
          {
            this->line_search_outer(*this->y_, *this->b_);
          }
            catch (NewtonLineSearchError&)
              {
                this->res_.line_search_all_time += line_search_outer_timer.elapsed();
                if (this->reassembled_)
                  throw;
                if (this->verbosity_level_ >= 3){
                  std::cout << "      line search failed - trying again with reassembled matrix" << std::endl;}
                  tfile << "      line search failed - trying again with reassembled matrix" << std::endl;
                continue;
              }
        this->res_.line_search_all_time += line_search_outer_timer.elapsed();

         //Update subdomain dirichlet boundary
         // old fashion
         /*for (auto itv = this->v_->begin(), itp = this->p_->begin(), itu = this->u_->begin(); itv != this->v_->end(); itv++, itp++, itu++) {
           if (*itp==0.0)
             *itv = -(*itu);         
         }*/
         // new fashion
         for (auto i_v = itr_v0.begin(), i_u = itr_u0.begin(); i_v != itr_v0.end(); i_v++, i_u++){
            **i_v = -(**i_u);
         }

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
        std::cout << "vv" << std::endl;  
      Dune::printvector(std::cout, this->v_->base(),"solution","row");}*/
        

              this->res_.reduction = this->res_.defect/this->res_.first_defect;
              //this->res2_.reduction = this->res2_.defect/this->res2_.first_defect;
              if ((this->res_inner1_.iterations + this->res_inner2_.iterations) != 0)
              this->res_.iterations++;
              this->res_.conv_rate = std::pow(this->res_.reduction, 1.0/this->res_.iterations);

              // store old ios flags
              if (this->verbosity_level_ >= 3) ios_base_all_saver restorer(std::cout);

              if (this->verbosity_level_ >= 3){
              if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
                std::cout << "  Global Nonlinear iteration " << std::setw(2) << this->res_.iterations << std::endl
                          << "      defect reduction (this iteration):"
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect/this->prev_defect_ << std::endl
                          << "      defect reduction (total):         "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.reduction << std::endl
                          << "      new defect:                       "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect << std::endl;}

                tfile << "  Global Nonlinear iteration " << std::setw(2) << this->res_.iterations << std::endl
                          << "      defect reduction (this iteration):"
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect/this->prev_defect_ << std::endl
                          << "      defect reduction (total):         "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.reduction << std::endl
                          << "      new defect:                       "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect << std::endl;
              if (this->verbosity_level_ == 2)
              if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
                std::cout << "  Global Newton iteration " << std::setw(2) << this->res_.iterations
                          << ".  New defect: "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect
                          << ".  Reduction (this): "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect/this->prev_defect_
                          << ".  Reduction (total): "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.reduction << std::endl;
              

              this->prev_defect_ = this->res_.defect;

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
        std::cout << "uu" << std::endl;  
      Dune::printvector(std::cout, this->u_->base(),"solution","row");
        }*/

        }while (!this->terminate()); // end main while


        } // end try
      catch(...)
        {
          this->res_inner1_.elapsed = timer.elapsed();
          this->res_.elapsed = timer.elapsed();
          throw;
        }

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
        std::cout << "uu" << std::endl;  
      Dune::printvector(std::cout, this->u_->base(),"solution","row");}*/

      this->res_inner1_.elapsed = timer.elapsed();
      this->res_.elapsed = timer.elapsed();

      if (this->verbosity_level_ >= 1) ios_base_all_saver restorer(std::cout); // store old ios flags


      if (this->verbosity_level_ >= 1){
        if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)
        std::cout << "  Newton converged after " << std::setw(2) << this->res_.iterations
                  << " iterations.  Reduction: "
                  << std::setw(12) << std::setprecision(4) << std::scientific
                  << this->res_.reduction
                  << "   (" << std::setprecision(4) << this->res_.elapsed << "s)"
                  << std::endl;}

        tfile << "  Newton converged after " << std::setw(2) << this->res_.iterations
                  << " iterations.  Reduction: "
                  << std::setw(12) << std::setprecision(4) << std::scientific
                  << this->res_.reduction
                  << "   (" << std::setprecision(4) << this->res_.elapsed << "s)"
                  << std::endl;


      if(!this->keep_matrix_)
        this->A_.reset();

      tfile.close();

    } // end apply_precon

    template<class GOS, class S, class TrlV, class TstV>
    void NewtonSolver<GOS,S,TrlV,TstV>::apply()
    {
      this->res_.iterations = 0;
      this->res_.converged = false;
      this->res_.reduction = 1.0;
      this->res_.conv_rate = 1.0;
      this->res_.elapsed = 0.0;
      this->res_.assembler_time = 0.0;
      this->res_.linear_solver_time = 0.0;
      this->res_.linear_solver_iterations = 0;
      result_valid_ = true;
      Timer timer;



      try
        {
          if(!this->r_) {
            // std::cout << "=== Setting up residual vector ..." << std::endl;
            this->r_ = std::make_shared<TestVector>(this->gridoperator_.testGridFunctionSpace());
          }
          // residual calculation in member function "defect":
          //--------------------------------------------------
          // - set residual vector to zero
          // - calculate new residual
          // - store norm of residual in "this->res_.defect"
          this->defect(*this->r_);
          this->res_.first_defect = this->res_.defect;
          this->prev_defect_ = this->res_.defect;

          if (this->verbosity_level_ >= 2)
            {
              // store old ios flags
              ios_base_all_saver restorer(std::cout);
              std::cout << "  Initial defect: "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << this->res_.defect << std::endl;
            }

          if(!this->A_) {
            // std::cout << "==== Setting up jacobian matrix ... " << std::endl;
            this->A_ = std::make_shared<Matrix>(this->gridoperator_);
          }
          if(!this->z_) {
            // std::cout << "==== Setting up correction vector ... " << std::endl;
            this->z_ = std::make_shared<TrialVector>(this->gridoperator_.trialGridFunctionSpace());
          }

          while (!this->terminate())
            {
              if (this->verbosity_level_ >= 3)
                std::cout << "  Newton iteration " << this->res_.iterations
                          << " --------------------------------" << std::endl;

              Timer assembler_timer;
              try
                {
                  // jacobian calculation in member function "prepare_step"
                  //-------------------------------------------------------
                  // - if above reassemble threshold
                  //   - set jacobian to zero
                  //   - calculate new jacobian
                  // - set linear reduction
                  this->prepare_step(*this->A_,*this->r_);
                }
              catch (...)
                {
                  this->res_.assembler_time += assembler_timer.elapsed();
                  throw;
                }
              double assembler_time = assembler_timer.elapsed();
              this->res_.assembler_time += assembler_time;
              if (this->verbosity_level_ >= 3)
                std::cout << "      matrix assembly time:             "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << assembler_time << std::endl;

              Timer linear_solver_timer;
              try
                {
                  // solution of linear system in member function "linearSolve"
                  //-----------------------------------------------------------
                  // - set initial guess for correction z to zero
                  // - call linear solver
                  this->linearSolve(*this->A_, *this->z_, *this->r_);
                }
              catch (...)
                {
                  this->res_.linear_solver_time += linear_solver_timer.elapsed();
                  this->res_.linear_solver_iterations += this->solver_.result().iterations;
                  throw;
                }
              double linear_solver_time = linear_solver_timer.elapsed();
              this->res_.linear_solver_time += linear_solver_time;
              this->res_.linear_solver_iterations += this->solver_.result().iterations;

              Timer line_search_timer;
              try
                {
                  // line search with correction z
                  // the undamped version is also integrated in here
                  this->line_search(*this->z_, *this->r_);
                }
              catch (NewtonLineSearchError&)
                {
                  this->res_.line_search_all_time = line_search_timer.elapsed();
                  if (this->reassembled_)
                    throw;
                  if (this->verbosity_level_ >= 3)
                    std::cout << "      line search failed - trying again with reassembled matrix" << std::endl;
                  continue;
                }

              this->res_.line_search_all_time = line_search_timer.elapsed();
              this->res_.reduction = this->res_.defect/this->res_.first_defect;
              this->res_.iterations++;
              this->res_.conv_rate = std::pow(this->res_.reduction, 1.0/this->res_.iterations);

              // store old ios flags
              ios_base_all_saver restorer(std::cout);

              if (this->verbosity_level_ >= 3)
                std::cout << "      linear solver time:               "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << linear_solver_time << std::endl
                          << "      defect reduction (this iteration):"
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect/this->prev_defect_ << std::endl
                          << "      defect reduction (total):         "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.reduction << std::endl
                          << "      new defect:                       "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect << std::endl;
              if (this->verbosity_level_ == 2)
                std::cout << "  Newton iteration " << std::setw(2) << this->res_.iterations
                          << ".  New defect: "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect
                          << ".  Reduction (this): "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.defect/this->prev_defect_
                          << ".  Reduction (total): "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res_.reduction << std::endl;
            } // end while
        } // end try
      catch(...)
        {
          this->res_.elapsed = timer.elapsed();
          throw;
        }
      this->res_.elapsed = timer.elapsed();

      ios_base_all_saver restorer(std::cout); // store old ios flags

      if (this->verbosity_level_ == 1)
        std::cout << "  Newton converged after " << std::setw(2) << this->res_.iterations
                  << " iterations.  Reduction: "
                  << std::setw(12) << std::setprecision(4) << std::scientific
                  << this->res_.reduction
                  << "   (" << std::setprecision(4) << this->res_.elapsed << "s)"
                  << std::endl;

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
        std::cout << "zz after communicate + cutoff boundary " << std::endl;  
      Dune::printvector(std::cout, this->z_->base(),"solution","row");}*/

        /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
        std::cout << "uu" << std::endl;  
      Dune::printvector(std::cout, this->u_->base(),"solution","row");}*/


      if(!this->keep_matrix_)
        this->A_.reset();
    } // end apply

    template<class GOS, class TrlV, class TstV>
    class NewtonTerminate : public virtual NewtonBase<GOS,TrlV,TstV>
    {
      typedef GOS GridOperator;
      typedef TrlV TrialVector;

      typedef typename TstV::ElementType RFType;

    public:
      NewtonTerminate(const GridOperator& go, TrialVector& u_, TrialVector& v_, TrialVector& p_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_,p_)
        , maxit_(40)
        , force_iteration_(false)
      {
        this->reduction_ = 1e-8;
        this->abs_limit_ = 1e-12;
      }

      NewtonTerminate(const GridOperator& go, TrialVector& u_, TrialVector& v_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_)
        , maxit_(40)
        , force_iteration_(false)
      {
        this->reduction_ = 1e-8;
        this->abs_limit_ = 1e-12;
      }

      NewtonTerminate(const GridOperator& go, TrialVector& u_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_)
        , maxit_(40)
        , force_iteration_(false)
      {
        this->reduction_ = 1e-8;
        this->abs_limit_ = 1e-12;
      }

      NewtonTerminate(const GridOperator& go)
        : NewtonBase<GOS,TrlV,TstV>(go)
        , maxit_(40)
        , force_iteration_(false)
      {
        this->reduction_ = 1e-8;
        //this->reduction_ = 1e-2;
        this->abs_limit_ = 1e-12;
      }

      void setReduction(RFType reduction)
      {
        this->reduction_ = reduction;
      }

      void setReduction_inner1(RFType reduction_inner1)
      {
        this->reduction_inner1_ = reduction_inner1;
      }

      void setReduction_inner2(RFType reduction_inner2)
      {
        this->reduction_inner2_ = reduction_inner2;
      }

      void setMaxIterations(unsigned int maxit)
      {
        maxit_ = maxit;
      }

      void setForceIteration(bool force_iteration)
      {
        force_iteration_ = force_iteration;
      }

      void setAbsoluteLimit(RFType abs_limit_)
      {
        this->abs_limit_ = abs_limit_;
      }

      virtual bool terminate()
      {
        //if (force_iteration_ && this->res_.iterations == 0)
          //return true;
        if (this->res_inner1_.iterations == 0 && this->res_inner2_.iterations == 0)
          return true;
        this->res_.converged = this->res_.defect < this->abs_limit_
          || this->res_.defect < this->res_.first_defect * this->reduction_;
        if (this->res_.iterations >= maxit_ && !this->res_.converged)
          DUNE_THROW(NewtonNotConverged,
                     "NewtonTerminate::terminate(): Maximum iteration count reached");
        return this->res_.converged;
      }

      virtual bool terminate_inner1()
      {
        if (this->local_flag_)
        {
          std::cout << "check point: " << this->local_flag_ << std::endl;
          this->res_inner1_.converged=1;
          return this->res_inner1_.converged;
        }
        //if ((this->res_inner1_.defect < this->reduction_inner1_) && this->res_inner1_.iterations == 0){
          //return true;}
        this->res_inner1_.converged = this->res_inner1_.defect < this->abs_limit_
          || this->res_inner1_.defect < this->res_inner1_.first_defect * this->reduction_inner1_;

        if (this->res_inner1_.converged)
        {
          this->local_flag_=0;
        }

        if (this->res_inner1_.iterations >= maxit_ && !this->res_inner1_.converged)
        {
          this->local_flag_=1;
          this->res_inner1_.converged=1;
        }
          //DUNE_THROW(NewtonNotConverged,
          //           "NewtonTerminate::terminate_inner1(): Maximum iteration count reached");

         /*std::cout << "check point: this->res_inner1_.converged = " << this->res_inner1_.converged << " this->res_inner1_.iterations = " << this->res_inner1_.iterations << std::endl;*/

        if (this->res_inner1_.converged && this->res_inner1_.iterations==0)
        {
          if (this->verbosity_level_ >= 3)
            std::cout << "      Reassembling matrix..." << std::endl;

          *this->A_ = 0.0;                                    // TODO: Matrix interface

          this->gridoperator_.jacobian(*this->g_, *this->A_); // still needs a Jacobian
        }

        return this->res_inner1_.converged;
      }

      virtual bool terminate_inner2()
      {
        //if (force_iteration_ && this->res_inner2_.iterations == 0)
          //return true;
        this->res_inner2_.converged = this->res_inner2_.defect < this->abs_limit_
          || this->res_inner2_.defect < this->res_inner2_.first_defect * this->reduction_inner2_;
        if (this->res_inner2_.iterations >= maxit_ && !this->res_inner2_.converged)
          DUNE_THROW(NewtonNotConverged,
                     "NewtonTerminate::terminate_inner2(): Maximum iteration count reached");
        if (!this->res_inner2_.converged){
              Timer line_search_inner2_timer;
              try
                {
                  // line search with correction z
                  // the undamped version is also integrated in here
                  this->line_search_inner2(*this->z_, *this->gz_);
                }
              catch (NewtonLineSearchError&)
                {
                  this->res_.line_search_all_time += line_search_inner2_timer.elapsed();
                  if (this->reassembled_)
                    throw;
                  if (this->verbosity_level_ >= 3){
                    std::cout << "      line search failed - trying again with reassembled matrix" << std::endl;
                    //tfile << "      line search failed - trying again with reassembled matrix" << std::endl;
                     }
                  //continue;
                  exit(-1);
                }
                this->res_.line_search_all_time += line_search_inner2_timer.elapsed();
        }
        return this->res_inner2_.converged;
      }

    private:
      unsigned int maxit_;
      bool force_iteration_;
    }; // end class NewtonTerminate

    template<class GOS, class TrlV, class TstV>
    class NewtonPrepareStep : public virtual NewtonBase<GOS,TrlV,TstV>
    {
      typedef GOS GridOperator;
      typedef TrlV TrialVector;

      typedef typename TstV::ElementType RFType;
      typedef typename GOS::Traits::Jacobian Matrix;

    public:
      NewtonPrepareStep(const GridOperator& go, TrialVector& u_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_)
        , min_linear_reduction_(1e-3)
        , fixed_linear_reduction_(0.0)
        , reassemble_threshold_(0.0)
      {}

      NewtonPrepareStep(const GridOperator& go, TrialVector& u_, TrialVector& v_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_)
        , min_linear_reduction_(1e-3)
        , fixed_linear_reduction_(0.0)
        , reassemble_threshold_(0.0)
      {}

      NewtonPrepareStep(const GridOperator& go, TrialVector& u_, TrialVector& v_, TrialVector& p_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_,p_)
        , min_linear_reduction_(1e-3)
        , fixed_linear_reduction_(0.0)
        , reassemble_threshold_(0.0)
      {}

      NewtonPrepareStep(const GridOperator& go)
        : NewtonBase<GOS,TrlV,TstV>(go)
        , min_linear_reduction_(1e-3)
        , fixed_linear_reduction_(0.0)
        , reassemble_threshold_(0.0)
      {}

      /**\brief set the minimal reduction in the linear solver

         \note with min_linear_reduction > 0, the linear reduction will be
         determined as mininum of the min_linear_reduction and the
         linear_reduction needed to achieve second order
         Newton convergence. */
      void setMinLinearReduction(RFType min_linear_reduction)
      {
        min_linear_reduction_ = min_linear_reduction;
      }

      /**\brief set a fixed reduction in the linear solver (overwrites setMinLinearReduction)

         \note with fixed_linear_reduction > 0, the linear reduction
         rate will always be fixed to min_linear_reduction. */
      void setFixedLinearReduction(bool fixed_linear_reduction)
      {
        fixed_linear_reduction_ = fixed_linear_reduction;
      }

      /**\brief set a threshold, when the linear operator is reassembled

         We allow to keep the linear operator over several newton
         iterations. If the reduction in the newton drops below a
         given threshold the linear operator is reassembled to ensure
         convergence.
       */
      void setReassembleThreshold(RFType reassemble_threshold)
      {
        reassemble_threshold_ = reassemble_threshold;
      }

      virtual void prepare_step(Matrix& A, TstV& )
      {
        this->reassembled_ = false;
        if (this->res_.defect/this->prev_defect_ > reassemble_threshold_)
          {

         if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){
            if (this->verbosity_level_ >= 3)
              std::cout << "      Reassembling matrix..." << std::endl;}
            A = 0.0;                                    // TODO: Matrix interface
            this->gridoperator_.jacobian(*this->u_, A);
            this->reassembled_ = true;
          }

        if (fixed_linear_reduction_ == true)
          this->linear_reduction_ = min_linear_reduction_;
        else {
          // determine maximum defect, where Newton is converged.
          RFType stop_defect =
            std::max(this->res_.first_defect * this->reduction_,
                     this->abs_limit_);

          /*
            To achieve second order convergence of newton
            we need a linear reduction of at least
            current_defect^2/prev_defect^2.
            For the last newton step a linear reduction of
            1/10*end_defect/current_defect
            is sufficient for convergence.
          */
          this->linear_reduction_ =
            std::max( stop_defect/(10*this->res_.defect),
              std::min(min_linear_reduction_,this->res_.defect*this->res_.defect/(this->prev_defect_*this->prev_defect_)) );
        }

        this->prev_defect_ = this->res_.defect;

      if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_){

        if (this->verbosity_level_ >= 3)

        ios_base_all_saver restorer(std::cout); // store old ios flags

          std::cout << "      requested linear reduction:       "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << this->linear_reduction_ << std::endl;
      }
      }

      virtual void prepare_step_inner1(Matrix& A, TstV& )
      {
        this->reassembled_ = false;
        if (this->res_inner1_.defect/this->prev_defect_inner1_ > reassemble_threshold_)
          {
            if (this->verbosity_level_ >= 3)
              std::cout << "      Reassembling matrix..." << std::endl;
            A = 0.0;                                    // TODO: Matrix interface
            this->gridoperator_.jacobian(*this->g_, A);
            //this->gridoperator_.make_consistent(A);
            //this->gridoperator_.update();
            this->reassembled_ = true;

            using Backend::native;

            /*if (this->gridoperator_.trialGridFunctionSpace().gridView().comm().rank()==this->targetToPrint_)        
          {Dune::printmatrix(std::cout, native(A),"solution"," ");
            std::cout << A.M() << " " << A.N() << std::endl;}*/
          }

        if (fixed_linear_reduction_ == true)
          this->linear_reduction_inner1_= min_linear_reduction_;
        else {
          // determine maximum defect, where Newton is converged.
          RFType stop_defect =
            std::max(this->res_inner1_.first_defect * this->reduction_,
                     this->abs_limit_);

          /*
            To achieve second order convergence of newton
            we need a linear reduction of at least
            current_defect^2/prev_defect^2.
            For the last newton step a linear reduction of
            1/10*end_defect/current_defect
            is sufficient for convergence.
          */
          this->linear_reduction_inner1_ =
            std::max( stop_defect/(10*this->res_inner1_.defect),
              std::min(min_linear_reduction_,this->res_inner1_.defect*this->res_inner1_.defect/(this->prev_defect_inner1_*this->prev_defect_inner1_)) );
        }

        this->prev_defect_inner1_ = this->res_inner1_.defect;

        ios_base_all_saver restorer(std::cout); // store old ios flags

        if (this->verbosity_level_ >= 3)
          std::cout << "      requested linear reduction (inner1):       "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << this->linear_reduction_inner1_ << std::endl;
      }

      virtual void prepare_step_inner2(Matrix& A, TstV& )
      {
        //*this->r2_ = 0.0;
        /*this->reassembled_ = false;
        if (this->res_inner2_.defect/this->prev_defect_inner2_ > reassemble_threshold_)
          {
            if (this->verbosity_level_ >= 3)
              std::cout << "      Reassembling matrix..." << std::endl;
            A = 0.0;                                    // TODO: Matrix interface
            this->gridoperator_.jacobian(*this->u_, A);
            this->reassembled_ = true;
            std::cout << A.M() << " " << A.N() << std::endl;
          }*/

        //Dune::PDELab::Backend::native(A).mv(Dune::PDELab::Backend::native(*this->y_), Dune::PDELab::Backend::native(*this->r2_));

        if (fixed_linear_reduction_ == true)
          this->linear_reduction_inner2_ = min_linear_reduction_;
        else {
          // determine maximum defect, where Newton is converged.
          RFType stop_defect =
            std::max(this->res_inner2_.first_defect * this->reduction_inner2_,
                     this->abs_limit_);

          /*
            To achieve second order convergence of newton
            we need a linear reduction of at least
            current_defect^2/prev_defect^2.
            For the last newton step a linear reduction of
            1/10*end_defect/current_defect
            is sufficient for convergence.
          */
          this->linear_reduction_inner2_ =
            std::max( stop_defect/(10*this->res_inner2_.defect),
              std::min(min_linear_reduction_,this->res_inner2_.defect*this->res_inner2_.defect/(this->prev_defect_inner2_*this->prev_defect_inner2_)) );
        }

        //this->prev_defect_ = this->res_.defect;
        //this->prev_defect_inner1_ = this->res_inner1_.defect;
        //this->prev_defect_inner2_ = this->res_inner2_.defect;

        if (this->verbosity_level_ >= 3) ios_base_all_saver restorer(std::cout); // store old ios flags

        if (this->verbosity_level_ >= 3)
          std::cout << "      requested linear reduction (inner2):       "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << this->linear_reduction_inner2_ << std::endl;
      }
    private:
      RFType min_linear_reduction_;
      bool fixed_linear_reduction_;
      RFType reassemble_threshold_;
    }; // end class NewtonPrepareStep

    template<typename D, typename R>
    struct TypeHolderForBoundedLineSearch
    {
      using Domain = D;
      using Real = R;
    };

    template<class GOS, class TrlV, class TstV>
    class NewtonLineSearch : public virtual NewtonBase<GOS,TrlV,TstV>
    {
      typedef GOS GridOperator;
      typedef TrlV TrialVector;
      typedef TstV TestVector;

      typedef typename TestVector::ElementType RFType;

    public:
      enum Strategy {
        /** \brief don't do any linesearch or damping */
        noLineSearch,
        /** \brief perform a linear search for the optimal damping parameter with multiples of damping

         the strategy was described in <a href="http://dx.doi.org/10.1007/BF01406516">[Hackbusch and Reusken, 1989]</a> */
        hackbuschReusken,
        /** \brief same as hackbuschReusken, but doesn't fail if the best update is still not good enough */
        hackbuschReuskenAcceptBest,
        /** \brief with limiters to keep chosen values inside defined interval, eg. to prevent negative concentration */
        boundedNoLineSearch,
        boundedHackbuschReusken
      };

      NewtonLineSearch(const GridOperator& go, TrialVector& u_, TrialVector& v_, TrialVector& p_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_,p_)
        , strategy_(noLineSearch)
        //, strategy_(hackbuschReusken)
        , maxit_(10)
        , damping_factor_(0.5)
        , correctSolution(pc)
      {}


      NewtonLineSearch(const GridOperator& go, TrialVector& u_, TrialVector& v_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_)
        , strategy_(noLineSearch)
        //, strategy_(hackbuschReusken)
        , maxit_(10)
        , damping_factor_(0.5)
        , correctSolution(pc)
      {}

      NewtonLineSearch(const GridOperator& go, TrialVector& u_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_)
        , strategy_(noLineSearch)
        //, strategy_(hackbuschReusken)
        , maxit_(10)
        , damping_factor_(0.5)
        , correctSolution(pc)
      {}

      NewtonLineSearch(const GridOperator& go)
        : NewtonBase<GOS,TrlV,TstV>(go)
        , strategy_(noLineSearch)
        //, strategy_(hackbuschReusken)
        , maxit_(10)
        , damping_factor_(0.5)
        , correctSolution(pc)
      {}

      void setLineSearchStrategy(Strategy strategy)
      {
        strategy_ = strategy;
      }

      void setLineSearchStrategy(std::string strategy)
      {
        strategy_ = strategyFromName(strategy);
      }

      void setLineSearchMaxIterations(unsigned int maxit)
      {
        maxit_ = maxit;
      }

      void setLineSearchDampingFactor(RFType damping_factor)
      {
        damping_factor_ = damping_factor;
      }

      void setBoundedParameters(const ParameterTree& ptree)
      {
        pc.setBoundedParameters(ptree);
      }

      virtual void line_search(TrialVector& z, TestVector& r)
      {
        if (strategy_ == noLineSearch || strategy_ == boundedNoLineSearch)
          {
            this->u_->axpy(-1.0, z);                     // TODO: vector interface
            this->defect(r);
            return;
          }

        if (this->verbosity_level_ >= 4)
          std::cout << "      Performing line search..." << std::endl;
        RFType lambda = 1.0;
        RFType best_lambda = 0.0;
        RFType best_defect = this->res_.defect;
        TrialVector prev_u(*this->u_);  // TODO: vector interface
        unsigned int i = 0;
        if (this->verbosity_level_ >= 4) ios_base_all_saver restorer(std::cout); // store old ios flags

        while (1)
          {
            if (this->verbosity_level_ >= 4)
              std::cout << "          trying line search damping factor:   "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << lambda
                        << std::endl;

            this->u_->axpy(-lambda, z);                  // TODO: vector interface
            try {
              this->defect(r);
            }
             catch (NewtonDefectError&)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          Nans detected" << std::endl;
              }       // ignore NaNs and try again with lower lambda

            if (this->res_.defect <= (1.0 - lambda/4) * this->prev_defect_)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          line search converged" << std::endl;
                break;
              }

            if (this->res_.defect < best_defect)
              {
                best_defect = this->res_.defect;
                best_lambda = lambda;
              }

            if (++i >= maxit_)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          max line search iterations exceeded" << std::endl;
                switch (strategy_)
                  {
                  case hackbuschReusken:
                    *this->u_ = prev_u;
                    this->defect(r);
                    DUNE_THROW(NewtonLineSearchError,
                               "NewtonLineSearch::line_search(): line search failed, "
                               "max iteration count reached, "
                               "defect did not improve enough");
                  case hackbuschReuskenAcceptBest:
                    if (best_lambda == 0.0)
                      {
                        *this->u_ = prev_u;
                        this->defect(r);
                        DUNE_THROW(NewtonLineSearchError,
                                   "NewtonLineSearch::line_search(): line search failed, "
                                   "max iteration count reached, "
                                   "defect did not improve in any of the iterations");
                      }
                    if (best_lambda != lambda)
                      {
                        *this->u_ = prev_u;
                        this->u_->axpy(-best_lambda, z);
                        this->defect(r);
                      }
                    break;
                  case noLineSearch:
                    break;
                  case boundedNoLineSearch:
                    break;
                  case boundedHackbuschReusken:
                    *this->u_ = prev_u;
                    DUNE_THROW(NewtonLineSearchError,
                      "NewtonLineSearch::line_search(): bounded line search failed, "
                               "max iteration count reached, "
                               "defect did not improve enough");
                    break;
                  }
                break;
              }

            lambda *= damping_factor_;
            *this->u_ = prev_u;                          // TODO: vector interface
          }
        if (this->verbosity_level_ >= 4)
          std::cout << "          line search damping factor:   "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << lambda << std::endl;
      } // end line_search

      virtual void line_search_outer(TrialVector& z, TestVector& r)
      { //damped Fixed-point and Newton fixed-point
        if (strategy_ == noLineSearch || strategy_ == boundedNoLineSearch)
          {
            this->u_->axpy(-1.0, z);                     // TODO: vector interface
            //this->u_->axpy(-0.5, z);                     // TODO: vector interface
            //this->u_->axpy(-1.0, r);                     // TODO: vector interface
            //this->defect_outer(*this->b_);
            this->defect(*this->r_);                      // not yet fixed for line search
            return;
          }

        if (this->verbosity_level_ >= 4)
          std::cout << "      Performing line search..." << std::endl;
        RFType lambda = 1.0;
        RFType best_lambda = 0.0;
        RFType best_defect = this->res_.defect;
        TrialVector prev_u(*this->u_);  // TODO: vector interface
        unsigned int i = 0;
        ios_base_all_saver restorer(std::cout); // store old ios flags

        while (1)
          {
            if (this->verbosity_level_ >= 4)
              std::cout << "          trying line search damping factor:   "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << lambda
                        << std::endl;

            this->u_->axpy(-lambda, z);                  // TODO: vector interface
            try {
              this->defect_outer(*this->b_);
            }
             catch (NewtonDefectError&)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          Nans detected" << std::endl;
              }       // ignore NaNs and try again with lower lambda

            if (this->res_.defect <= (1.0 - lambda/4) * this->prev_defect_)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          line search converged" << std::endl;
                break;
              }

            if (this->res_.defect < best_defect)
              {
                best_defect = this->res_.defect;
                best_lambda = lambda;
              }

            if (++i >= maxit_)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          max line search iterations exceeded" << std::endl;
                switch (strategy_)
                  {
                  case hackbuschReusken:
                    *this->u_ = prev_u;
                    this->defect_outer(*this->b_);
                    DUNE_THROW(NewtonLineSearchError,
                               "NewtonLineSearch::line_search(): line search failed, "
                               "max iteration count reached, "
                               "defect did not improve enough");
                  case hackbuschReuskenAcceptBest:
                    if (best_lambda == 0.0)
                      {
                        *this->u_ = prev_u;
                        this->defect_outer(*this->b_);
                        DUNE_THROW(NewtonLineSearchError,
                                   "NewtonLineSearch::line_search(): line search failed, "
                                   "max iteration count reached, "
                                   "defect did not improve in any of the iterations");
                      }
                    if (best_lambda != lambda)
                      {
                        *this->u_ = prev_u;
                        this->u_->axpy(-best_lambda, z);
                        this->defect_outer(*this->b_);
                      }
                    break;
                  case noLineSearch:
                    break;
                  case boundedNoLineSearch:
                    break;
                  case boundedHackbuschReusken:
                    *this->u_ = prev_u;
                    DUNE_THROW(NewtonLineSearchError,
                      "NewtonLineSearch::line_search(): bounded line search failed, "
                               "max iteration count reached, "
                               "defect did not improve enough");
                    break;
                  }
                break;
              }

            lambda *= damping_factor_;
            *this->u_ = prev_u;                          // TODO: vector interface
          }
        if (this->verbosity_level_ >= 4)
          std::cout << "          line search damping factor:   "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << lambda << std::endl;
      } // end line_search_outer

      virtual void line_search_inner1(TrialVector& z, TestVector& r)
      {
        if (strategy_ == noLineSearch) // || strategy_ == boundedNoLineSearch)
          {
            this->g_->axpy(-1.0, z);                     // TODO: vector interface
            this->defect_inner1(r);
            return;
          }
        else if (strategy_ == boundedNoLineSearch)
          {
            this->g_->axpy(-1.0,z);
            this->correctSolution(*this->g_);
            this->defect_inner1(r);
            return;
          }

        if (this->verbosity_level_ >= 4)
          std::cout << "      Performing line search..." << std::endl;
        RFType lambda = 1.0;
        RFType best_lambda = 0.0;
        RFType best_defect = this->res_inner1_.defect;
        TrialVector prev_u(*this->g_);  // TODO: vector interface
        unsigned int i = 0;
        ios_base_all_saver restorer(std::cout); // store old ios flags

        while (1)
          {
            if (this->verbosity_level_ >= 4)
              std::cout << "          trying line search damping factor:   "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << lambda
                        << std::endl;

            this->g_->axpy(-lambda, z);                  // TODO: vector interface
            if (strategy_ == boundedHackbuschReusken)
              this->correctSolution(*this->g_);
            try {
              this->defect_inner1(r);
            }
             catch (NewtonDefectError&)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          Nans detected" << std::endl;
              }       // ignore NaNs and try again with lower lambda

            if (this->res_inner1_.defect <= (1.0 - lambda/4) * this->prev_defect_inner1_)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          line search converged" << std::endl;
                break;
              }

            if (this->res_inner1_.defect < best_defect)
              {
                best_defect = this->res_inner1_.defect;
                best_lambda = lambda;
              }

            if (++i >= maxit_)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          max line search iterations exceeded" << std::endl;
                switch (strategy_)
                  {
                  case hackbuschReusken:
                    *this->g_ = prev_u;
                    this->defect_inner1(r);
                    DUNE_THROW(NewtonLineSearchError,
                               "NewtonLineSearch::line_search(): line search failed, "
                               "max iteration count reached, "
                               "defect did not improve enough");
                  case hackbuschReuskenAcceptBest:
                    if (best_lambda == 0.0)
                      {
                        *this->g_ = prev_u;
                        this->defect_inner1(r);
                        DUNE_THROW(NewtonLineSearchError,
                                   "NewtonLineSearch::line_search(): line search failed, "
                                   "max iteration count reached, "
                                   "defect did not improve in any of the iterations");
                      }
                    if (best_lambda != lambda)
                      {
                        *this->g_ = prev_u;
                        this->g_->axpy(-best_lambda, z);
                        this->defect_inner1(r);
                      }
                    break;
                  case noLineSearch:
                    break;
                  case boundedNoLineSearch:
                    break;
                  case boundedHackbuschReusken:
                    *this->g_ = prev_u;
                    this->defect_inner1(r);
                    DUNE_THROW(NewtonLineSearchError,
                      "NewtonLineSearch::line_search(): bounded line search failed, "
                               "max iteration count reached, "
                               "defect did not improve enough");
                    break;
                  }
                break;
              }

            lambda *= damping_factor_;
            *this->g_ = prev_u;                          // TODO: vector interface
          }
        if (this->verbosity_level_ >= 4)
          std::cout << "          line search damping factor:   "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << lambda << std::endl;
      } // end line_search_inner1

      virtual void line_search_inner2(TrialVector& z, TestVector& r)
      {
        if (strategy_ == noLineSearch || strategy_ == boundedNoLineSearch)
          {
            this->y_->axpy(-1.0, r);                     // TODO: vector interface
            //this->defect_inner2(r);
            return;
          }

        if (this->verbosity_level_ >= 4)
          std::cout << "      Performing line search..." << std::endl;
        RFType lambda = 1.0;
        RFType best_lambda = 0.0;
        RFType best_defect = this->res_inner2_.defect;
        TrialVector prev_u(*this->y_);  // TODO: vector interface
        unsigned int i = 0;
        ios_base_all_saver restorer(std::cout); // store old ios flags

        while (1)
          {
            if (this->verbosity_level_ >= 4)
              std::cout << "          trying line search damping factor:   "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << lambda
                        << std::endl;

            this->y_->axpy(-lambda, r);                  // TODO: vector interface
            try {
              //this->defect_inner2(r);
            }
             catch (NewtonDefectError&)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          Nans detected" << std::endl;
              }       // ignore NaNs and try again with lower lambda

            if (this->res_inner2_.defect <= (1.0 - lambda/4) * this->prev_defect_inner2_)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          line search converged" << std::endl;
                break;
              }

            if (this->res_inner2_.defect < best_defect)
              {
                best_defect = this->res_inner2_.defect;
                best_lambda = lambda;
              }

            if (++i >= maxit_)
              {
                if (this->verbosity_level_ >= 4)
                  std::cout << "          max line search iterations exceeded" << std::endl;
                switch (strategy_)
                  {
                  case hackbuschReusken:
                    *this->y_ = prev_u;
                    //this->defect_inner2(r);
                    DUNE_THROW(NewtonLineSearchError,
                               "NewtonLineSearch::line_search(): line search failed, "
                               "max iteration count reached, "
                               "defect did not improve enough");
                  case hackbuschReuskenAcceptBest:
                    if (best_lambda == 0.0)
                      {
                        *this->y_ = prev_u;
                        //this->defect_inner2(r);
                        DUNE_THROW(NewtonLineSearchError,
                                   "NewtonLineSearch::line_search(): line search failed, "
                                   "max iteration count reached, "
                                   "defect did not improve in any of the iterations");
                      }
                    if (best_lambda != lambda)
                      {
                        *this->y_ = prev_u;
                        this->y_->axpy(-best_lambda,r);
                        //this->defect_inner2(r);
                      }
                    break;
                  case noLineSearch:
                    break;
                  case boundedNoLineSearch:
                    break;
                  case boundedHackbuschReusken:
                    *this->y_ = prev_u;
                    DUNE_THROW(NewtonLineSearchError,
                      "NewtonLineSearch::line_search(): bounded line search failed, "
                               "max iteration count reached, "
                               "defect did not improve enough");
                    break;
                  }
                break;
              }

            lambda *= damping_factor_;
            *this->y_ = prev_u;                          // TODO: vector interface
          }
        if (this->verbosity_level_ >= 4)
          std::cout << "          line search damping factor:   "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << lambda << std::endl;
      } // end line_search_inner2

    protected:
      /** helper function to get the different strategies from their name */
      Strategy strategyFromName(const std::string & s) {
        if (s == "noLineSearch")
          return noLineSearch;
        if (s == "hackbuschReusken")
          return hackbuschReusken;
        if (s == "hackbuschReuskenAcceptBest")
          return hackbuschReuskenAcceptBest;
        if (s == "boundedNoLineSearch")
          return boundedNoLineSearch;
        if (s == "boundedHackbuschReusken")
          return boundedHackbuschReusken;
        DUNE_THROW(Exception, "unknown linesearch strategy" << s);
      }

    private:
      Strategy strategy_;
      unsigned int maxit_;
      RFType damping_factor_;
      using ParameterClass = Dune::PDELab::Impl::BoundedLineSearchParametersInterface<TypeHolderForBoundedLineSearch<TrlV,typename TrlV::ElementType> >;
      ParameterClass pc;
      using CorrectionType = typename Dune::PDELab::Impl::SetCorrectSolution<TypeHolderForBoundedLineSearch<TrlV,typename TrlV::ElementType> >::type;
      CorrectionType correctSolution;
    }; // end class NewtonLineSearch

    template<class GOS, class S, class TrlV, class TstV = TrlV>
    class RestrainedNewton : public NewtonSolver<GOS,S,TrlV,TstV>
                           , public NewtonTerminate<GOS,TrlV,TstV>
                           , public NewtonLineSearch<GOS,TrlV,TstV>
                           , public NewtonPrepareStep<GOS,TrlV,TstV>
    {
      typedef GOS GridOperator;
      typedef S Solver;
      typedef TrlV TrialVector;

    public:
      RestrainedNewton(const GridOperator& go, TrialVector& u_, Solver& solver_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_)
        , NewtonSolver<GOS,S,TrlV,TstV>(go,u_,solver_)
        , NewtonTerminate<GOS,TrlV,TstV>(go,u_)
        , NewtonLineSearch<GOS,TrlV,TstV>(go,u_)
        , NewtonPrepareStep<GOS,TrlV,TstV>(go,u_)
      {}
      RestrainedNewton(const GridOperator& go, Solver& solver_)
        : NewtonBase<GOS,TrlV,TstV>(go)
        , NewtonSolver<GOS,S,TrlV,TstV>(go,solver_)
        , NewtonTerminate<GOS,TrlV,TstV>(go)
        , NewtonLineSearch<GOS,TrlV,TstV>(go)
        , NewtonPrepareStep<GOS,TrlV,TstV>(go)
      {}
      RestrainedNewton(const GridOperator& go, TrialVector& u_, TrialVector& v_, Solver& solver_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_)
        , NewtonSolver<GOS,S,TrlV,TstV>(go,u_,v_,solver_)
        , NewtonTerminate<GOS,TrlV,TstV>(go,u_,v_)
        , NewtonLineSearch<GOS,TrlV,TstV>(go,u_,v_)
        , NewtonPrepareStep<GOS,TrlV,TstV>(go,u_,v_)
      {}
      RestrainedNewton(const GridOperator& go, TrialVector& u_, TrialVector& v_, TrialVector& p_, Solver& solver_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_,v_,p_)
        , NewtonSolver<GOS,S,TrlV,TstV>(go,u_,v_,p_,solver_)
        , NewtonTerminate<GOS,TrlV,TstV>(go,u_,v_,p_)
        , NewtonLineSearch<GOS,TrlV,TstV>(go,u_,v_,p_)
        , NewtonPrepareStep<GOS,TrlV,TstV>(go,u_,v_,p_)
      {}
      //! interpret a parameter tree as a set of options for the newton solver
      /**

         example configuration:

         \code
         [NewtonParameters]

         ReassembleThreshold = 0.1
         LineSearchMaxIterations = 10
         MaxIterations = 7
         AbsoluteLimit = 1e-6
         Reduction = 1e-4
         LinearReduction = 1e-3
         LineSearchDamping  = 0.9
         \endcode

         and invocation in the code:
         \code
         newton.setParameters(param.sub("NewtonParameters"));
         \endcode
      */
      void setParameters(Dune::ParameterTree & param)
      {
        typedef typename TstV::ElementType RFType;
        if (param.hasKey("VerbosityLevel"))
          this->setVerbosityLevel(
            param.get<unsigned int>("VerbosityLevel"));
        if (param.hasKey("Reduction"))
          this->setReduction(
            param.get<RFType>("Reduction"));
        if (param.hasKey("MaxIterations"))
          this->setMaxIterations(
            param.get<unsigned int>("MaxIterations"));
        if (param.hasKey("ForceIteration"))
          this->setForceIteration(
            param.get<bool>("ForceIteration"));
        if (param.hasKey("AbsoluteLimit"))
          this->setAbsoluteLimit(
            param.get<RFType>("AbsoluteLimit"));
        if (param.hasKey("MinLinearReduction"))
          this->setMinLinearReduction(
            param.get<RFType>("MinLinearReduction"));
        if (param.hasKey("FixedLinearReduction"))
          this->setFixedLinearReduction(
            param.get<bool>("FixedLinearReduction"));
        if (param.hasKey("ReassembleThreshold"))
          this->setReassembleThreshold(
            param.get<RFType>("ReassembleThreshold"));
        if (param.hasKey("LineSearchStrategy"))
          this->setLineSearchStrategy(
            param.get<std::string>("LineSearchStrategy"));
        if (param.hasKey("LineSearchMaxIterations"))
          this->setLineSearchMaxIterations(
            param.get<unsigned int>("LineSearchMaxIterations"));
        if (param.hasKey("LineSearchDampingFactor"))
          this->setLineSearchDampingFactor(
            param.get<RFType>("LineSearchDampingFactor"));
        if (param.hasKey("KeepMatrix"))
          this->setKeepMatrix(
            param.get<bool>("KeepMatrix"));
        // if (param.hasKey("restrained")) // TODO: check!
        this->setBoundedParameters(param);
      }
    }; // end class Newton
  } // end namespace PDELab
} // end namespace Dune

#endif // DUNE_RESTRAINED_RASPEN_HH
