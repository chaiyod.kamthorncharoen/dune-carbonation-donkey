template<typename Number>
class MakeComponents
{
  // Number eta;
public:
  typedef Number value_type;

  //! Linear in x
  template<typename E, typename X>
  Number linx (const E& e, const X& x) const
  {
    auto global = e.geometry().global(x);
    return global[0];
  }

  //! Linear in y
  template<typename E, typename X>
  Number liny (const E& e, const X& x) const
  {
    auto global = e.geometry().global(x);
    return global[1];
  }
};
