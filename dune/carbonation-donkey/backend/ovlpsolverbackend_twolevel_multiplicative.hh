// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:
#ifndef DUNE_RASPEN_OVLPSOLVERBACKEND_HH
#define DUNE_RASPEN_OVLPSOLVERBACKEND_HH

#include <dune/pdelab/backend/istl/ovlpistlsolverbackend.hh>
#include <dune/grid/utility/globalindexset.hh>
#include <dune/carbonation-donkey/backend/geometric_multigrid_components.hh>
#include <dune/istl/matrixmatrix.hh>
#include <dune/istl/umfpack.hh>


namespace Dune {
  namespace PDELab {

  template<class GFS, class R0, class MAT, class S, class M, class X, class Y, class Z>
  class MatrixAdapterSubdomainSolve : public AssembledLinearOperator<M,X,Y>
  {
  public:
    //! export types

    typedef GFS GridFunctionSpace;
    typedef S Solver;

    typedef ProlongationOperatorHierarchy<GFS> POH;
    typedef Dune::PDELab::ISTL::ParallelHelper<GFS> PIH;
    typedef typename GFS::template ConstraintsContainer<double>::Type CC;
    typedef typename GFS::Traits::GridViewType GV;
    typedef Dune::GlobalIndexSet<GV> GlobalId;


    typedef M matrix_type;
    typedef X domain_type;
    typedef Y range_type;
    typedef typename X::field_type field_type;

    //typedef typename Dune::MatMultMatResult<M,M>::type Mat;

    //! define the category
    SolverCategory::Category category() const
     {
       return SolverCategory::sequential;
     }

    //! constructor: just store a reference to a matrix
    explicit MatrixAdapterSubdomainSolve (const M& A, const GFS& gfs, S& solver, Y& pu, Y& py, const Y& g, PIH& pihf, const M& Ac, Z& itp, Z& ity, R0& r0, MAT& A0matrix, unsigned int verbosity_level, int targetToPrint, bool skipCoarse)
      : _A_(A), _Ac_(Ac), gfs_(gfs), solver_(solver), pu_(pu), py_(py), g_(g), pihf_(pihf), itp_(itp), ity_(ity), r0_(r0), A0matrix_(A0matrix), verbosity_level_(verbosity_level), targetToPrint_(targetToPrint), skipCoarse_(skipCoarse)
      {

      Ar = std::make_shared<Y>(gfs_);
      eiTR0 = std::make_shared<Y>(gfs_);
      R0Tej = std::make_shared<Y>(gfs_);

      }

    //! apply operator to x:  \f$ y = A(x) \f$
    virtual void apply (const X& x, Y& y) const
    {
      using Backend::native;
    
      // RHS
      native(_A_).mv(native(x),native(y));

      //cutoff border
        // old fashion
        for (auto ity = y.begin(), itp = py_.begin(); ity != y.end(); ity++, itp++) {
          if (*itp==0.0)
            *ity = 0.0;
        }
/*
      if (gfs_.gridView().comm().rank()==0){
        std::cout << "y" << std::endl;  
	    Dune::printvector(std::cout, native(y),"solution","row");}*/
        

      //LinearSolve
      Y z(x);
      z = 0;
      solver_.apply(_A_, z, y, 1e-4);
      auto itz = z.begin();
      auto itp = pu_.begin();
        for (;itz != z.end(); itz++, itp++) {
          //if (*itp!=0)
          *itz = (*itz)*(*itp);
        } 
   
      Dune::PDELab::AddDataHandle<GFS,domain_type> adh_(gfs_,z);
          if (gfs_.gridView().comm().size()>1)
            gfs_.gridView().communicate(adh_,Dune::All_All_Interface,Dune::ForwardCommunication);

      // if (gfs_.gridView().comm().rank()==targetToPrint_){
      //   std::cout << "z" << std::endl;  
      // Dune::printvector(std::cout, native(z),"solution","row");}

    if(!skipCoarse_){      
      Y yc(x);
      yc = 0;
      auto itx = x.begin();
      auto ityc=yc.begin();
      itz = z.begin();
        for (;itx != x.end(); itx++, ityc++, itz++) {
          //if (*itp!=0)
          *ityc = (*itx)-(*itz);
        }
    
      // RHS for coarse
      native(_Ac_).mv(native(yc),native(y));

      //cutoff border
        // old fashion
        for (auto ity = y.begin(), itp = py_.begin(); ity != y.end(); ity++, itp++) {
          if (*itp==0.0)
            *ity = 0.0;
        }



      // global coarse grid solve
      int components = r0_.size();
      int size_rank = gfs_.gridView().comm().size();
      int this_rank = gfs_.gridView().comm().rank();


      std::vector<field_type> RHS_Coarse(size_rank*components);
      std::vector<field_type> RHS_Coarse_(size_rank*components);

      // using size_type = typename std::vector<int>::size_type;
      // for (size_type j = 0; j < localV_.size();j++){  // iterate over coarse index
      //   if (CoarseIndexSet_.at(localV_.at(j))==gfs_.gridView().comm().rank()){
      //     auto itri = ei->begin();
      //     std::advance(itri,j);
      //     RHS_Coarse[localV_.at(j)] = *itri;
      //   }
      // }

     for (int I = 0; I < components ; I++) RHS_Coarse[this_rank*components+I] = (*(r0_[I])).dot(y);

      /*if (gfsc_.gridView().comm().rank()==targetToPrint_){
        std::cout << "check RHS_Coarse" << std::endl;
        for (size_type j = 0; j < size_rank; ++j)
        {
          std::cout << RHS_Coarse[j] << ' ' << std::endl;
        }
      }*/

      MPI_Barrier(MPI_COMM_WORLD);

      // communicate
      MPI_Allreduce(&RHS_Coarse[0],&RHS_Coarse_[0],size_rank*components,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

      // clear out coarse constrained dofs
      // for (int x:CoarseBoundary_){ // iterate over rows corresponding to constrained dofs
      //   RHS_Coarse_[x] = 0.;       // constrained dofs' value = 0
      // }

      using ScalarVector = Dune::BlockVector<Dune::FieldVector<field_type,1>>;
      ScalarVector zc(size_rank*components);
      // ScalarVector zc_(size_rank*components);
      ScalarVector rhs(size_rank*components);

      // Build BCRS Matrix
      typedef Dune::BCRSMatrix<Dune::FieldMatrix<field_type,1,1> > A0;
      A0 _A0matrix_(size_rank*components, size_rank*components, 9, 3, A0::implicit);

      if (this_rank==0){
        for (int i = 0; i<size_rank*components; i++){
          rhs[i] = RHS_Coarse_[i];
          // for (int j=0; j<size_rank*components; j++){
          //   if (std::abs(A0matrix_[i][j]) > 1e-4) _A0matrix_.entry(i,j) = A0matrix_[i][j];  // select only non-zeros
          //   // if (gfsc_.gridView().comm().rank()==targetToPrint_){
          //   //   std::cout << "_A0matrix_.entry(" << i << "," << j << ") = " << _A0matrix_.entry(i,j) << std::endl;
          //   //   std::cout << "A0matrix_[" << i << "][" << j <<"] = " << A0matrix_[i][j] << std::endl;
          //   // }
          // }
        }
      }
      MPI_Barrier(MPI_COMM_WORLD);

      // auto stats0 = _A0matrix_.compress();

      // if (gfsc_.gridView().comm().rank()==targetToPrint_){
      //   std::cout << "check RHS_Coarse after Comm and constrained" << std::endl;
      //   for (size_type j = 0; j < size_rank; ++j)
      //   {
      //     std::cout << RHS_Coarse_[j] << ' ' << std::endl;
      //   }
      // }

      // linear coarse grid solve
      zc = 0.;  // coarse linear solution vector (global)
      if (this_rank==0){    // solve coarse problem on rank 0
        Dune::UMFPack<A0> coarsesolver_(A0matrix_,0);
        Dune::InverseOperatorResult stat;
        coarsesolver_.apply(zc, rhs, stat);      
      }

      MPI_Barrier(MPI_COMM_WORLD);
      // MPI_Bcast(zc,size_rank*components,MPI_DOUBLE,0,MPI_COMM_WORLD);
      gfs_.gridView().comm().broadcast(&zc[0],size_rank*components,0);  

      // if (gfs_.gridView().comm().rank()==0){
      //   std::cout << "zc from rank 0" << std::endl;  
      //   Dune::printvector(std::cout, native(zc),"solution","row");
      // }

      // if (gfs_.gridView().comm().rank()==1){
      //   std::cout << "zc from rank 1" << std::endl;  
      //   Dune::printvector(std::cout, native(zc),"solution","row");
      // }

      // prolongate coarse solution
      Y d(gfs_);
      d = 0.;
      for (typename X::size_type i=0; i<d.N(); i++)
        for (int I = 0; I<components; I++)
          native(d)[i].axpy(native(*(r0_[I]))[i],zc[this_rank*components+I]);

      // auto itd = d.begin();
      // itp = pu_.begin();
      //   for (;itd != d.end(); itd++, itp++) {
      //     //if (*itp!=0)
      //     *itd = (*itd)*(*itp);
      //   } 

      // if (gfs_.gridView().comm().rank()==targetToPrint_){
      //   std::cout << "d before comm" << std::endl;  
      //   Dune::printvector(std::cout, native(d),"solution","row");
      // }
   
      Dune::PDELab::AddDataHandle<GFS,domain_type> adhd_(gfs_,d);
      if (gfs_.gridView().comm().size()>1)
        gfs_.gridView().communicate(adhd_,Dune::All_All_Interface,Dune::ForwardCommunication);

      // if (gfs_.gridView().comm().rank()==targetToPrint_){
      //   std::cout << "d after comm" << std::endl;  
      //   Dune::printvector(std::cout, native(d),"solution","row");
      // }

      // if (gfs_.gridView().comm().rank()==targetToPrint_){
      //   std::cout << "pu" << std::endl;  
      //   Dune::printvector(std::cout, native(pu_),"solution","row");
      // }

      // if (gfs_.gridView().comm().rank()==targetToPrint_){
      //   std::cout << "z" << std::endl;  
      //   Dune::printvector(std::cout, native(z),"solution","row");
      // }

      y = z;
      y += d;              
    }
    else
      y = z;

    }

    //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
    virtual void applyscaleadd (field_type alpha, const X& x, Y& y) const
    {
        using Backend::native;

        Y temp1(y);
        temp1 = 0.0;
        this->apply(x,temp1);
        temp1*=alpha;
        y=g_;
        // y+=g_;

        y+=temp1;
    }


    //! get matrix via *
    virtual const M& getmat () const
    {
      return _A_;
    }

  private:
        //using DUNE::Backend::Native;
        //using DUNE::Backend::native;
    const M& _A_;
    const M& _Ac_;
    const GFS& gfs_;
    S& solver_;
    Y& pu_;
    Y& py_;
    const Y& g_;    // RHS Vector

    const PIH& pihf_;
    Z& itp_, ity_;  // not use
    R0& r0_;
    typedef Dune::BCRSMatrix<Dune::FieldMatrix<field_type,1,1> > A0;
    A0 A0matrix_;

    unsigned int verbosity_level_;
    int targetToPrint_;
    bool skipCoarse_;

    std::shared_ptr<Y> ei;
    std::shared_ptr<Y> ej;
    std::shared_ptr<Y> eiTR0;
    std::shared_ptr<Y> R0Tej;
    std::shared_ptr<Y> Ar;

    // std::vector<std::vector<field_type>> A0matrix_;


  };

    // really new scalar product assuming at least overlap 1
    // uses unique partitioning of nodes for parallelization
    // define because the category does not match with my newton code
    template<class GFS, class X>
    class OverlappingNewtonScalarProduct
      : public Dune::ScalarProduct<X>
    {
    public:
      //! export types
      typedef X domain_type;
      typedef typename X::ElementType field_type;

      //! define the category
      SolverCategory::Category category() const
      {
       return SolverCategory::sequential;
      }

      /*! \brief Constructor needs to know the grid function space
       */
      OverlappingNewtonScalarProduct (const GFS& gfs_, const ISTL::ParallelHelper<GFS>& helper_, const X& pu_)
        : gfs(gfs_), helper(helper_), pu(pu_)
      {}


      /*! \brief Dot product of two vectors.
        It is assumed that the vectors are consistent on the interior+border
        partition.
      */
      virtual field_type dot (const X& x, const X& y) const override
      {
        // do local scalar product on unique partition
        field_type sum = helper.disjointDot(x,y);

        // do global communication
        return gfs.gridView().comm().sum(sum);
      }

      /*! \brief Norm of a right-hand side vector.
        The vector must be consistent on the interior+border partition
      */
      virtual double norm (const X& x) const override
      {
        /*X z(x);
        auto itz = z.begin();
        auto itp = pu.begin();        
        for (; itz != z.end(); itz++, itp++) {
          if (*itp==0.0)
            *itz = 0.0;         
        }*/
        
        //return sqrt(static_cast<double>(this->dot(z,z)));
        return sqrt(static_cast<double>(this->dot(x,x)));
      }

    private:
      const GFS& gfs;
      const ISTL::ParallelHelper<GFS>& helper;
      const X& pu;
    };

    //! \} Overlapping Solvers

    //! \} group Backend

  } // namespace PDELab
} // namespace Dune

#endif
