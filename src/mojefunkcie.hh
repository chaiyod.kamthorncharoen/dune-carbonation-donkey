namespace mojefunkcie
{
  class MojefunkcieException : public Dune::Exception {};

  template<typename RF>
  class CubicSmoother
  {
    // connects two points with cubic curve, given values and derivations in those points
    // Problem: monotonicity is NOT preserved
    RF a,b,c,d;
    void construct(const RF& x1, const RF& x2, const RF& v1, const RF& v2, const RF& d1, const RF& d2)
    {
      RF dx = x2-x1;
      a = (d1+d1-2.*(v2-v1)/dx)/dx/dx;
      b = (d2-d1)/2./dx -1.5*(x1+x2)*a;
      c = d1-(3.*x1*a+2.*b)*x1;
      d = v1 - x1*(c+x1*(b+x1*a));
    }
  public:
    CubicSmoother(const std::array<RF,2>& x_, const std::array<RF,2>& fx_, const std::array<RF,2>& dfx_)
    {
      construct(x_[0],x_[1],fx_[0],fx_[1],dfx_[0],dfx_[1]);
    }

    CubicSmoother(const std::tuple<RF,RF>& x_, const std::tuple<RF,RF>& fx_, const std::tuple<RF,RF>& dfx_)
    {
      construct(std::get<0>(x_),std::get<1>(x_),std::get<0>(fx_),std::get<1>(fx_),std::get<0>(dfx_),std::get<1>(dfx_));
    }

    RF value(const RF& x) const
    {
      // a x^3 + b x^2 + c x + d
      return d+x*(c+x*(b+x*a));
    }

    RF derivation(const RF& x) const
    {
      // 3 a x^2 + 2 b x + c
      return c+x*(2.*b+x*3.*a);
    }
  };

  template<typename RF>
  class Smoother
  {
    std::vector<RF> nodes,values;
    RF margin; // margin = 0 is without smoothing, margin = 1 smoothes everything, making it a spline of connected 1/8 of circles
  public:
    Smoother(const std::vector<RF>& nodes_, const std::vector<RF>& values_, const RF& margin_=0.1)
    : nodes(nodes_)
    , values(values_)
    {
      margin = margin_/2.; // I do margin from top and bottom -devide by 2 to keep total margin same
      if (nodes.size() != values.size()+1)
      {
        std::cout << "Smoother needs size of vector of nodes be 1 larger than size of values!" << std::endl;
      }
    }

    RF evaluate(const RF& x)
    {
      if (x<nodes[0])
        // if outside nodes zone, simply extend value[0]
        return values[0];
      for (int i=1; i<nodes.size()-1; ++i)
      {
        // check if it is around node[i], where it should be interpolated
        auto bottom = nodes[i]-margin*(nodes[i]-nodes[i-1]);
        auto top = nodes[i]+margin*(nodes[i+1]-nodes[i]);
        bool notunder = x > bottom;
        bool notover = x < top;
        if (notunder && notover)
        {
          auto interval = std::make_tuple(bottom,top);
          auto vals = std::make_tuple(values[i-1],values[i]);
          return smooth_cirq(interval,vals,x);
        }
        else
        {
          // give flat value otside interpolate zone
          if (x <= bottom)
            return values[i-1];
        }
      }
      // x is between the last two nodes or higher
      // x >= top( nodes[nodes.size()-2] )
      return values[values.size()-1];
    }
  };

  template<typename T>
  constexpr T PI = T(3.141'592'653'589'793'238'462'643'383'279'502'884'197'169'399'375'1);

  inline double l2_norm(std::vector<double> const& u)
  {
    double accum = 0.;
    for (const double& x : u) {
        accum += x * x;
    }
    return sqrt(accum);
  }

  template<typename RF>
  inline RF ReconstructFlowDeterminant(const std::vector<Dune::FieldVector<RF,2> >& vertex)
  {
    return  (vertex[0][0]-vertex[1][0])*(vertex[0][1]-vertex[2][1])
           -(vertex[0][1]-vertex[1][1])*(vertex[0][0]-vertex[2][0]);
  }
  template<typename RF>
  inline RF ReconstructFlowDeterminant(const std::array<Dune::FieldVector<RF,2>,3 >& vertex)
  {
    return  (vertex[0][0]-vertex[1][0])*(vertex[0][1]-vertex[2][1])
           -(vertex[0][1]-vertex[1][1])*(vertex[0][0]-vertex[2][0]);
  }

  template<typename RF>
  inline std::vector<RF> ReconstructFlow(const std::vector<RF>& values, const std::vector<Dune::FieldVector<RF,2> >& vertex)
  {
    const std::size_t dim = values.size()-1;
    // assemble matrix
    RF det = ReconstructFlowDeterminant(vertex);

    // assemble right hand side
    std::vector<RF> rhs(dim);
    for (std::size_t i = 0; i < dim; ++i)
    {
      rhs[i] = values[0] - values[i+1];
    }
    // compute result
    std::vector<RF> res(2);
    res[0]=(rhs[0]*(vertex[0][1]-vertex[2][1])-rhs[1]*(vertex[0][1]-vertex[1][1]))/det;
    res[1]=(rhs[1]*(vertex[0][0]-vertex[1][0])-rhs[0]*(vertex[0][0]-vertex[2][0]))/det;
    return res;
  }

  template<typename RF>
  inline std::array<RF,2> ReconstructFlow(const std::array<RF,3>& values, const std::array<Dune::FieldVector<RF,2>,3>& vertex)
  {
    // assemble matrix
    RF det = ReconstructFlowDeterminant(vertex);

    // assemble right hand side
    std::array<RF,2> rhs;
    for (std::size_t i = 0; i < 2; ++i)
    {
      rhs[i] = values[0] - values[i+1];
    }
    // compute result
    std::array<RF,2> res;
    res[0]=(rhs[0]*(vertex[0][1]-vertex[2][1])-rhs[1]*(vertex[0][1]-vertex[1][1]))/det;
    res[1]=(rhs[1]*(vertex[0][0]-vertex[1][0])-rhs[0]*(vertex[0][0]-vertex[2][0]))/det;
    return res;
  }

  template<typename RF>
  inline std::array<RF,2> ReconstructFlow(const std::vector<RF>& values, const std::array<Dune::FieldVector<RF,2>,3>& vertex)
  {
    // assemble matrix
    RF det = ReconstructFlowDeterminant(vertex);

    // assemble right hand side
    std::array<RF,2> rhs;
    for (std::size_t i = 0; i < 2; ++i)
    {
      rhs[i] = values[0] - values[i+1];
    }
    // compute result
    std::array<RF,2> res;
    res[0]=(rhs[0]*(vertex[0][1]-vertex[2][1])-rhs[1]*(vertex[0][1]-vertex[1][1]))/det;
    res[1]=(rhs[1]*(vertex[0][0]-vertex[1][0])-rhs[0]*(vertex[0][0]-vertex[2][0]))/det;
    return res;
  }

  template<typename RF>
  inline std::vector<std::vector<RF> > ReconstructFlowDerived(const std::vector<Dune::FieldVector<RF,2> >& vertex)
  {
    RF det = ReconstructFlowDeterminant(vertex);
    std::vector<std::vector<RF> > der;
    for (std::size_t i = 0; i < 3; ++i)
      {
        der[i][0] = (vertex[(i+1)%3][1]-vertex[(i+2)%3][1])/det;
        der[i][1] = (vertex[(i+2)%3][0]-vertex[(i+1)%3][0])/det;
      }
    return der;
  }
  template<typename RF>
  inline std::array<std::array<RF,2>,3> ReconstructFlowDerived(const std::array<Dune::FieldVector<RF,2>,3>& vertex)
  {
    RF det = ReconstructFlowDeterminant(vertex);
    std::array<std::array<RF,2>,3> der;
    for (std::size_t i = 0; i < 3; ++i)
      {
        der[i][0] = (vertex[(i+1)%3][1]-vertex[(i+2)%3][1])/det;
        der[i][1] = (vertex[(i+2)%3][0]-vertex[(i+1)%3][0])/det;
      }
    return der;
  }

  template <typename RF, std::size_t N>
  inline RF aritavg(const std::array<RF,N>& v)
  {
    RF pom=0;
    for (const auto& x : v)
      pom += x;
    return pom/RF(v.size());
  }

  template <typename RF>
  inline RF aritavg(const std::vector<RF>& v)
  {
    RF pom=0;
    for (const auto& x: v)
      pom += x;
    return pom/RF(v.size());
  }

  template <typename RF>
  inline RF h2avg(const RF& a, const RF& b)
  {
    if (a<=1e-100 || b<=1e-100)
    {
      return 0.;
    }
    return RF(2)*a*b/(a+b);
  }

  template <typename RF>
  inline RF h2avg(const RF& a, const RF& b, const std::string& s, int verbosity=1)
  {
    if (a<=1e-100 || b<=1e-100)
    {
      if (verbosity>=1)
      {
        std::cout << "regularizing harmonic average for " << s << ": " << a << ", " << b << std::endl;
      }
      // better get spammed than wonder over results...
      return 0.;
    }
    return RF(2)*a*b/(a+b);
  }

  template <typename RF>
  inline RF h2avg(const std::array<RF,2>& ab, std::string s="unspecified constant", int verbosity = 1)
  {
    return h2avg(ab[0],ab[1],s,verbosity);
  }

  template <typename RF>
  inline RF h2avg_deriv(const RF& a, const RF& b)
  {
    if (b<=1e-100 || a<1e-100)
    {
      return 0.;
    }
    return RF(2)*b*b/((a+b)*(a+b));
  }

  template <typename RF>
  inline RF h2avg_deriv(const RF& a, const RF& b, const std::string& s)
  {
    if (b<=1e-100 || a<1e-100)
    {
      std::cout << "regularizing harmonic average derivation for " << s << ": " << a << ", " << b << std::endl;
      return 0.;
    }
    return RF(2)*b*b/((a+b)*(a+b));
  }

  template <typename RF>
  inline RF hNavg(const std::vector<RF>& v, const std::string& s = "unknown variable")
  {
    RF invsum=0.;
    for (const auto& x : v)
    {
      if (x<=1e-100)
      {
        std::cout << "regularizing harmonic N average for " << s << std::endl;
        return RF(0);
      }
      invsum += RF(1)/x;
    }
    return RF(v.size())/invsum;
  }

  template <typename RF, std::size_t N>
  inline RF hNavg(const std::array<RF,N>& v, const std::string& s = "unknown variable")
  {
    RF invsum=0.;
    for (const auto& x : v)
    {
      if (x<=1e-100)
      {
        std::cout << "regularizing harmonic N average for " << s << std::endl;
        return RF(0);
      }
      invsum += RF(1)/x;
    }
    return RF(N)/invsum;
  }

  template <typename DFV> // Dune::FieldVector in array or vector
  inline bool correctlyOrientedTriangle(DFV vertex)
  {
    // check assumption triangles are labeled counterclockwise
    return  (vertex[1][0]-vertex[0][0])
           *(vertex[2][1]-vertex[0][1])
           -(vertex[2][0]-vertex[0][0])
           *(vertex[1][1]-vertex[0][1]) < 0. ? false : true;
        // (B-A)_x*(C-A)_y - (C-A)_x*(B-A)_y < 0
  }

  template <typename F>
  inline std::array<std::size_t,2> setactive(F&& lambda, const std::size_t& lfsusize=3)
  {
    std::array<std::size_t,2> active;
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      // lambda returns true if the vertex is NOT on the boundary
      // indices of other two vertices (=boundary) are set as active
      if (lambda(i))
      {
        active[0] = (i+1)%lfsusize;
        active[1] = (i+2)%lfsusize;
        return active;
      }
    }
    DUNE_THROW(MojefunkcieException,"Did not find active vertices for boundary");
    // throw std::string("did not find active vertices for boundary");
  }

  template <typename RF>
  RF smooth_cos(std::tuple<RF,RF> interval_, std::tuple<RF,RF> value_, RF x_)
  {
    // simple smoother of boundary conditions, derivations are zeros at interval boundaries
    RF a = std::get<0>(interval_);
    RF b = std::get<1>(interval_);
    assert(x_>=a && x_<=b);
    return std::get<0>(value_)+(1.-cos((x_-a)/(b-a)*PI<RF>))/2.*(std::get<1>(value_)-std::get<0>(value_));
  }

  template <typename RF>
  RF smooth_quad(std::tuple<RF,RF> interval_, std::tuple<RF,RF> value_, RF x_)
  {
    // quadratic smoother, sides have derivation 0, first derivation in the middle is continuous
    RF a = std::get<0>(interval_);
    RF b = std::get<1>(interval_);
    assert(x_>=a && x_<=b);
    if (x_<(a+b)/2.)
      return std::get<0>(value_)+(2.*(x_-a)/(b-a))*(2.*(x_-a)/(b-a))*(std::get<1>(value_)-std::get<0>(value_))/2.;
    else
      return std::get<1>(value_)+(2.*(b-x_)/(b-a))*(2.*(b-x_)/(b-a))*(std::get<0>(value_)-std::get<1>(value_))/2.;
  }

  template <typename RF>
  RF smooth_circ(std::tuple<RF,RF> interval_, std::tuple<RF,RF> value_, RF x_)
  {
    // smoother with two joint quarters of a circle, middle point has infinite derivation though
    RF a = std::get<0>(interval_);
    RF b = std::get<1>(interval_);
    assert(x_>=a && x_<=b);
    RF halfjump = (std::get<1>(value_)-std::get<0>(value_))/2.;
    if (x_<(a+b)/2.)
      return std::get<0>(value_)+(1.-sqrt(1.-(2.*(x_-a)/(b-a))*(2.*(x_-a)/(b-a))))*halfjump;
    else
      return std::get<1>(value_)-(1.-sqrt(1.-(2.*(b-x_)/(b-a))*(2.*(b-x_)/(b-a))))*halfjump;
      // return std::get<1>(value_)+sqrt(1.-(1.-2.*(b-x_)/(b-a))*(1.-2.*(b-x_)/(b-a)))*(std::get<0>(value_)-std::get<1>(value_))/2.;
  }

  template <typename RF>
  RF smooth_cirq(std::tuple<RF,RF> interval_, std::tuple<RF,RF> value_, RF x_)
  {
    // smoother with two 1/8 of a circle, middle point has first derivation continuous (and finite), but not second
    RF a = std::get<0>(interval_);
    RF b = std::get<1>(interval_);
    RF x = (x_-a)/(b-a);
    assert(0.<=x && x<=1.);
    RF jump = (std::get<1>(value_)-std::get<0>(value_));
    RF sq2 = sqrt(2.);
    if (x<0.5)
      return std::get<0>(value_)+(sq2+1.)*(sq2/2.-sqrt(0.5-x*x))*jump;
    else
      return std::get<0>(value_)+((sq2+1.)*sqrt(0.5-(1.-x)*(1.-x))-sq2/2.)*jump;
      // return std::get<1>(value_)-(1.-((sq2+1.)*sqrt(0.5-(1.-x)*(1.-x))-sq2/2.))*jump;
  }

  class writer
  {
  // simple wrapper that guarantees close() trigger
  public:
    std::ofstream text;
    writer(std::string name_) : text(name_) {};
    ~writer(){ text.close(); }
  };

  template <typename Z, typename RF=double>
  int check_vector_for_negatives(Z& z, RF treshold, const std::string& name_="Some")
  {
    // returns 0 if everything was OK, returns 1, if some value is too small and restarting the time step is necessary
    using Dune::PDELab::Backend::native;
    for (auto& v : native(z))
    {
      if (v<0.)
      {
        if (v<treshold)
        {
          std::cout << name_ << " value too negative" << std::endl;
          return 1;
        }
        v = 0.;
      }
    }
    return 0;
  }

  template <typename Z, typename RF=double>
  int check_blockvector_for_negatives(Z& z, RF treshold, std::size_t pos=0, const std::string& name_="Some")
  {
    // returns 0 if everything was OK, returns 1, if some value is too small and restarting the time step is necessary
    using Dune::PDELab::Backend::native;
    for (auto& block : native(z))
    {
      if (block[pos]<0.)
      {
        if (block[pos]<treshold)
        {
          std::cout << name_ << " value too negative" << std::endl;
          return 1;
        }
        block[pos] = 0.;
      }
    }
    return 0;
  }

  class Unfinished : public Dune::Exception
  {};
} // end namespace mojefunkcie