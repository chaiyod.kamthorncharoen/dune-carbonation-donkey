      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                     // overwriting enables using it as a template argument
      // constexpr int verbosity{0};

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      DF dx = c3[0]-a0[0];
      DF dy = c3[1]-a0[1];
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kwr, Karc; // better have vector of coefficients than re-compute pow()
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kwr[i] = param.Kwr(Sa[i],phi[i]);
        Karc[i] = param.Karc(Sa[i],pa[i],phi[i],co2_adv[i]);
      }

      for (int i=0; i<int(lfsusize); ++i)
      {
        // water and air diffusion
        RF rho_ay = p_h.getrho_a((pa[i]+pa[pairy[i]])/2.,(co2_adv[i]+co2_adv[pairy[i]])/2.);
        // RF rho_ay = (p_h.getrho_a(pa[i],co2[i])+p_h.getrho_a(pa[pairy[i]],co2[pairy[i]]))/2.; // TODO: check difference of inner and outer arit average
        RF qwx = -dy/2.*h2avg(Kwr[i],Kwr[pairx[i]],"Kwr in alpha_volume")  * (pw[pairx[i]]-pw[i])/dx;
        RF qwy = -dx/2.*h2avg(Kwr[i],Kwr[pairy[i]],"Kwr in alpha_volume")  *((pw[pairy[i]]-pw[i])/dy-(i-pairy[i])/2.*param.getgrav()*param.getrho_w());
        RF qax = -dy/2.*h2avg(Karc[i],Karc[pairx[i]],"Karc in alpha_volume")* (pa[pairx[i]]-pa[i])/dx;
        RF qay = -dx/2.*h2avg(Karc[i],Karc[pairy[i]],"Karc in alpha_volume")*((pa[pairy[i]]-pa[i])/dy-(i-pairy[i])/2.*param.getgrav()*rho_ay);

        // Ca and CO_2 convection
        qwy /= param.getrho_w();
        qwx /= param.getrho_w();
        qay *= param.getR()*param.getT();
        qax *= param.getR()*param.getT();

        // either I access the same edge twice and divide it by 2,
        // or accumulate only one edge -note that qwx, qwy have different sign then
        RF increment = qwx*(qwx>0 ? Ca_adv[i] : Ca_adv[pairx[i]]);
        r.accumulate(lfsv_Ca,      i, +increment );
           increment = qwy*(qwy>0 ? Ca_adv[i] : Ca_adv[pairy[i]]);
        r.accumulate(lfsv_Ca,      i, +increment );
        std::size_t branchco2 = qax>0 ? i : pairx[i];
           increment = qax/p_h.getMa(co2_adv[branchco2])*co2_adv[branchco2];
        r.accumulate(lfsv_co2,      i, +increment );
                    branchco2 = qay>0 ? i : pairy[i];
           increment = qay/p_h.getMa(co2_adv[branchco2])*co2_adv[branchco2];
        r.accumulate(lfsv_co2,      i, +increment );
      }

      // Ca and CO_2 diffusion
      std::array<RF,lfsusize> Dw, pa_Da_Ma;
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        Dw[i] = p_h.getdifCa(phi[i],1.-Sa[i]);
        pa_Da_Ma[i] = p_h.getdifco2(phi[i],Sa[i])*(pa[i]+param.getpa0())*p_h.getMa(co2_dif[i])/p_h.getMco2();
      }
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        r.accumulate(lfsv_Ca,i, dy/2.*(Ca_dif[i]-Ca_dif[pairx[i]])/dx *h2avg(Dw[i],Dw[pairx[i]],"Dw in alpha_volume")
                              +dx/2.*(Ca_dif[i]-Ca_dif[pairy[i]])/dy *h2avg(Dw[i],Dw[pairy[i]],"Dw in alpha_volume") );
        r.accumulate(lfsv_co2,i, dy/2.*(co2_dif[i]-co2_dif[pairx[i]])/dx *h2avg(pa_Da_Ma[i],pa_Da_Ma[pairx[i]],"pa*Da*Ma in alpha_volume")
                              +dx/2.*(co2_dif[i]-co2_dif[pairy[i]])/dy *h2avg(pa_Da_Ma[i],pa_Da_Ma[pairy[i]],"pa*Da*Ma in alpha_volume") );
      }

      // dispersion term TODO!
      const RF& a_T = p_h.getalfat();
      const RF& a_L = p_h.getalfal();
      RF avgKwr = mojefunkcie::hNavg(Kwr,"Kwr for dispersion in alpha_volume");
      RF vwx = -avgKwr* (pw[3]-pw[2]+pw[1]-pw[0])/2./dx/param.getrho_w();
      RF vwy = -avgKwr*((pw[3]-pw[1]+pw[2]-pw[0])/2./dy/param.getrho_w()-param.getgrav());
      RF vwsqrt = std::max(1e-5, sqrt(vwx*vwx+vwy*vwy));
      RF sat{0};
      for (const auto& v : Sa)
        sat+=v;
      sat/=static_cast<RF>(lfsusize);
      RF Dw11 = (a_L*vwx*vwx+a_T*vwy*vwy)/vwsqrt/((1.-sat)*(1-sat));
      RF Dw22 = (a_T*vwx*vwx+a_L*vwy*vwy)/vwsqrt/((1.-sat)*(1-sat));
      RF Dw12 = (a_L-a_T)*vwx*vwy/vwsqrt;
      const RF Caderx = (Ca_dif[3]-Ca_dif[2]+Ca_dif[1]-Ca_dif[0])/dx/2.;
      const RF Cadery = (Ca_dif[3]-Ca_dif[1]+Ca_dif[2]-Ca_dif[0])/dy/2.;

      RF avgKarc = mojefunkcie::hNavg(Karc,"Karc for dispersion in alpha_volume");
      RF rhoa = param.getrho_a(mojefunkcie::aritavg(pa));
      RF vax = -avgKarc* (pa[3]-pa[2]+pa[1]-pa[0])/2./dx/rhoa;
      RF vay = -avgKarc*((pa[3]-pa[1]+pa[2]-pa[0])/2./dy/rhoa-param.getgrav());
      RF vasqrt = std::max(1e-5, sqrt(vax*vax+vay*vay));
      RF Da11 = (a_L*vax*vax+a_T*vay*vay)/vasqrt/sat/sat;
      RF Da22 = (a_T*vax*vax+a_L*vay*vay)/vasqrt/sat/sat;
      RF Da12 = (a_L-a_T)*vax*vay/vasqrt;
      const RF co2derx = (co2_dif[3]-co2_dif[2]+co2_dif[1]-co2_dif[0])/dx/2.;
      const RF co2dery = (co2_dif[3]-co2_dif[1]+co2_dif[2]-co2_dif[0])/dy/2.;

      for (int i=0; i<int(lfsusize); ++i)
        {
          // here (i-pair[]) is used to determine signum, i must be int!
          // TODO: this is valid for saturated flow, unsaturated is balanced by Sw^{-2.2} (not sure which exact power to use)
          r.accumulate(lfsv_Ca,i,
                dy/2.*(Dw11*(Ca_dif[i]-Ca_dif[pairx[i]])/dx+Dw12*Cadery*(i-pairx[i])  ) // (i-pairx[i]) works as a unit normal vector
               +dx/2.*(Dw22*(Ca_dif[i]-Ca_dif[pairy[i]])/dy+Dw12*Caderx*(i-pairy[i])/2) );
          r.accumulate(lfsv_co2,i,
                dy/2.*(Da11*(co2_dif[i]-co2_dif[pairx[i]])/dx+Da12*co2dery*(i-pairx[i])  )
               +dx/2.*(Da22*(co2_dif[i]-co2_dif[pairy[i]])/dy+Da12*co2derx*(i-pairy[i])/2) );
        }