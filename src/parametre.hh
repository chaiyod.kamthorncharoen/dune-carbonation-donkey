class ParameterClassException : public Dune::Exception
{};

template<typename Number>
class Parametre
{
  Number aalfa, porosity_ini, n, m, Ks;
  Number height, width, grav;
  Number storedCa_ini;
  Number tau;
  Number t, othertime; // looks like I do not need time, only step
  Number inflowl, inflowr;
  Number visc_w, visc_a;
  Number visc_c = 1.48e-5; // rewritten when p_h class is created
  Number rho_w, Swr, Sar;
  Number etastab; // for Nische weak Dirichlet boundary condition
  Number R, T;   // ideal gas constant and temperature [K]
  Number Ma;     // air molar mass
  Number Mco2 = 44.01e-3;  // CO_2 molar mass kg/mol
  Number p0;  // atmospheric pressure
  Number Ka_correction = 50.; // air intristic permeability is 1-2 orders higher than water in. perm.

public:
  typedef Number value_type;
  Number l_co2 = 10.;

  //! Constructor filling material parameters
  Parametre (  const Number& aalfa_
             , const Number& porosty_ini_
             , const Number& n_
             , const Number& Ks_
             , const Number& height_
             , const Number& width_
             , const Number& grav_
             , const Number& visc_w_
             , const Number& visc_a_
             , const Number& rho_w_
             , const Number& Swr_
             , const Number& Sar_
             , const Number& etastab_
             , const Number& R_
             , const Number& T_
             , const Number& Ma_
             , const Number& Mc_
             , const Number& p0_
             , const Number& Ka_correction_
             , const Number& l_co2_)
            :  aalfa(aalfa_)
             , porosity_ini(porosty_ini_)
             , n(n_), m(1.-1./n_)
             , Ks(Ks_)
             , height(height_)
             , width(width_)
             , grav(grav_)
             , visc_w(visc_w_)
             , visc_a(visc_a_)
             , rho_w(rho_w_)
             , Swr(Swr_)
             , Sar(Sar_)
             , etastab(etastab_)
             , R(R_)
             , T(T_)
             , Ma(Ma_)
             , Mco2(Mc_)
             , p0(p0_)
             , Ka_correction(Ka_correction_)
             , l_co2(l_co2_)
  {
    setTimeStep(0.02);
    setTime(0.);
    setOtherTime(getTime());
    storedCa_ini = (1.-porosity_ini)/4.;
  }

  //! set time
  void setTime(Number t_)
  {
    t=t_;
  }
  Number getTime() const
  {
    return t;
  }

  void setOtherTime(const Number& t_)
  {
    othertime = t_;
  }

  Number getOtherTime() const
  {
    return othertime;
  }

  void addTime()
  {
    t+=tau;
  }

  void setTimeStep(const Number& tau_)
  {
    tau=tau_;
  }

  void adjustTimeStep(const Number& coef_)
  {
    tau*=coef_;
  }

  Number getTimeStep() const
  {
    return tau;
  }

  inline const Number& getgrav() const
  {
    return grav;
  }

  inline Number getporosity() const
  // used as an initial condition
  {
    return porosity_ini;
  }

  inline Number getstoredCa() const
  {
    return storedCa_ini;
  }

  Number pc(const Number& Sa) const
  {
    if (Sa<=Sar+1e-3)
    {
      if (Sa<=Sar)
        return 0.;
      else
        return (Sa-Sar)*1e3*pow(pow(1.-1e-3/(1.-Sar-Swr),-1./m)-1.,1./n)/aalfa; // linearized from 0 to 5.29787
    }
    else if (Sa>=1.-Swr-1e-3) // linearized, regularized blow-up
    {                         // stability needs Sa bounded away from 1-Swr
      // std::cout << "linearizing pc, Sa = " << Sa << std::endl;
      return pow( pow( (1e-3)/(1.-Swr-Sar), -1./m)-1., 1./n)/aalfa*(Sa+Swr+1e-3);
    }
    Number eSw = (1.-Sa-Swr)/(1.-Swr-Sar);
    Number result = pow( pow(eSw, -1./m)-1., 1./n)/aalfa;
    return result;
  }

  Number pc_deriv(const Number& Sa) const
  {
    if (Sa<=Sar+1e-3)
    {
      if (Sa<=Sar) // (Sa<=Sar+1e-3)
        return 0.;
      else
        return 1e3*pow(pow(1.-1e-3/(1.-Sar-Swr),-1./m)-1.,1./n)/aalfa; // =5297.87
    }
    else if (Sa>=1.-Swr-1e-3)
      return pow( pow( (1e-3)/(1.-Swr-Sar), -1./m)-1, 1./n)/aalfa; // e-1 yields =186.91, e-2  =673.58, and e-3 gives =2404.33
    Number eSw = (1.-Sa-Swr)/(1.-Swr-Sar);
    Number eSw_m = pow(eSw, 1./m);
    if (eSw_m<1.e-4)
    {
      eSw_m=1.e-4;
    }
    Number result = 1./((n-1.)*aalfa*(1.-Swr-Sar)*pow( 1.-eSw_m,m)*eSw_m);
    // remove Inf from zero division, may be pointless after eSw_m regularisation
    if (result != result)
    {
      // std::cout << "NaN in param.pc_deriv";
      DUNE_THROW(ParameterClassException,"capillary pressure derivation NaN");
    }
    return result;
  }

  template <int N>
  std::array<Number, N> pc_deriv(const std::array<Number, N>& Sa) const
  {
    std::array<Number, N> result;
    for (std::size_t i=0; i<N; ++i)
    {
      result[i] = pc_deriv(Sa[i]);
    }
    return result;
  }

  inline Number K(const Number& porosity_) const
  {
    Number pom1 = porosity_/porosity_ini;
    Number pom2 = (1.-porosity_ini)/(1.-porosity_); // *pom1
    return Ks*pom1*pom1*pom1*pom2*pom2;
    // it is possible to move pom1 into pom2 and save one multiplication,
    // but its less readable and thus (probably) not worth it (and it also accumulates (negligible) numerical error)
  }

  inline Number K_deriv(const Number& porosity_) const
  {
    Number pom1 = porosity_/porosity_ini;
    Number pom2 = (1.-porosity_ini)/(1.-porosity_); // *pom1
    return Ks*pom1*pom1*pom2*pom2*(3./porosity_ini+2./(1.-porosity_));
  }

  Number Kw(const Number& Sa, const Number& porosity_) const
  {
    Number K_ = K(porosity_);
    if (Sa<=Sar) // (Sa<=Sar+1e-3)
      return K_/visc_w;
    else if (Sa>=1.-Swr-1e-3)
      return 0.;
    else if (Sa<=Sar+1e-3)
    {
      Number fixSw = 1.-1e-3/(1.-Swr-Sar);
      Number fixpom = 1. - pow( 1.-pow(fixSw,1./m) ,m);
      return K_/visc_w*((1.-(Sa-Sar)*1e3)+(Sa-Sar)*1e3*sqrt(fixSw)*fixpom*fixpom); // linearized drop
    }
    Number eSw = (1.-Sa-Swr)/(1.-Swr-Sar);
    Number pom = 1. - pow( 1.-pow(eSw,1./m) ,m);
    Number result=K_/visc_w*sqrt(eSw)*pom*pom;
    if (result != result)
    {
      // std::cout << "NaN in param.Kw";
      DUNE_THROW(ParameterClassException,"water permeability NaN");
    }
    return result;
  }

  inline Number Kw_deriv(const Number& Sa, const Number& porosity_) const
  {
    // backward compatibility
    return dKw_dSa(Sa,porosity_);
  }


  Number dKw_dSa(const Number& Sa, const Number& porosity_) const
  {
    Number K_ = K(porosity_);
    if (Sa<=Sar || Sa>=1.-1e-3-Swr)
      return 0.;
    else if (Sa<=Sar+1e-3)
    {
      Number fixSw = 1.-1e-3/(1.-Swr-Sar);
      Number fixpom = 1. - pow( 1.-pow(fixSw,1./m) ,m);
      return K_/visc_w*1e3*(sqrt(fixSw)*fixpom*fixpom-1.); // linearized drop
    }
    Number eSw = (1.-Sa-Swr)/(1.-Swr-Sar);
    Number eSw_m = pow(eSw,1./m);
    Number pom = 1. - pow( 1.-eSw_m ,m);
    Number result=-K_/(visc_w*sqrt(eSw)*(1.-Swr-Sar))*pom*(pom/2.+2.*(1.-pom)*eSw_m/(1.-eSw_m));
    return result;
  }

  inline Number Kwr(const Number& Sa, const Number& porosity_) const
  {
    return Kw(Sa,porosity_)*getrho_w();
  }

  inline Number dKwr_dSa(const Number& Sa, const Number& porosity_) const
  {
    return dKw_dSa(Sa,porosity_)*getrho_w();
  }

  Number dKwr_dphi(const Number& Sa, const Number& porosity_) const
  {
    Number K_ = K_deriv(porosity_);
    if (Sa<=Sar) // (Sa<=Sar+1e-3)
      return K_/visc_w*getrho_w();
    else if (Sa>=1.-Swr-1e-3)
      return 0.;
    else if (Sa<=Sar+1e-3)
    {
      Number fixSw = 1.-1e-3/(1.-Swr-Sar);
      Number fixpom = 1. - pow( 1.-pow(fixSw,1./m) ,m);
      return K_/visc_w*((1.-(Sa-Sar)*1e3)+(Sa-Sar)*1e3*sqrt(fixSw)*fixpom*fixpom)*getrho_w(); // linearized drop
    }
    Number eSw = (1.-Sa-Swr)/(1.-Swr-Sar);
    Number pom = 1. - pow( 1.-pow(eSw,1./m) ,m);
    Number result=K_/visc_w*sqrt(eSw)*pom*pom;
    if (result != result)
    {
      // std::cout << "NaN in param.dKw_dphi";
      DUNE_THROW(ParameterClassException,"water permeability derived by porosity NaN");
    }
    return result*getrho_w();
  }

  Number Ka(const Number& Sa, const Number& porosity_) const
  {
    Number K_=K(porosity_)*Ka_correction;
    if (Sa<=Sar) // (Sa<=Sar+1e-3)
      return 0.;
    else if (Sa>=1.-Swr-1e-3)
      return K_/visc_a;
    else if (Sa<=Sar+1e-3)
    {
      Number fixSa = 1e-3/(1.-Sar-Swr);
      Number fixpom = pow( 1.- pow(1.-fixSa,1./m), m);
      return (Sa-Sar)*1e3*K_/visc_a*cbrt(fixSa)*fixpom*fixpom;
    }
    Number eSa = (Sa-Sar)/(1.-Sar-Swr);
    Number pom = pow( 1.- pow(1.-eSa,1./m), m);
    Number result = K_/visc_a*cbrt(eSa)*pom*pom;
    if (result != result)
    {
      // std::cout << "NaN in param.Ka";
      DUNE_THROW(ParameterClassException,"air permeability NaN");
    }
    return result;
  }

  inline Number Ka_deriv(const Number& Sa, const Number& porosity_) const
  {
    // backward compatibility
    return dKa_dSa(Sa,porosity_);
  }

  Number dKa_dSa(const Number& Sa, const Number& porosity_) const
  {
    Number K_ = K(porosity_)*Ka_correction;
    if( Sa<=Sar || Sa>=1.-1e-3-Swr)
      return 0.;
    else if (Sa<=Sar+1e-3)
    {
      Number fixSa = 1e-3/(1.-Sar-Swr);
      Number fixpom = pow( 1.- pow(1.-fixSa,1./m), m);
      return 1e3*K_/visc_a*cbrt(fixSa)*fixpom*fixpom;
    }
    Number eSw = (1.-Sa-Swr)/(1.-Swr-Sar);
    Number eSw_m = pow(eSw,1./m);
    Number pom = pow( 1.-eSw_m, m);
    Number result = K_/(visc_a*(1.-Sar-Swr))*cbrt(1.-eSw)*pom*pom*(1./(3.*(1.-eSw))+2.*eSw_m/(eSw*(1.-eSw_m)));
    return result;
  }

  inline Number Kar(const Number& Sa, const Number& pa, const Number& porosity_) const
  {
    return Ka(Sa,porosity_)*getrho_a(pa);
  }

  inline Number Kar_deriv(const Number& Sa, const Number& pa, const Number& porosity_) const
  {
    return dKar_dSa(Sa,pa,porosity_);
  }

  inline Number dKar_dSa(const Number& Sa, const Number& pa, const Number& porosity_) const
  {
    return dKa_dSa(Sa,porosity_)*getrho_a(pa)+Ka(Sa,porosity_)*rho_a_deriv()*pc_deriv(Sa);
  }

  Number dKar_dphi(const Number& Sa, const Number& pa, const Number& porosity_) const
  {
    Number K_=K_deriv(porosity_)*Ka_correction;
    if (Sa<=Sar) // (Sa<=Sar+1e-3)
      return 0.;
    else if (Sa>=1.-Swr-1e-3)
      return K_/visc_a*getrho_a(pa);
    else if (Sa<=Sar+1e-3)
    {
      Number fixSa = 1e-3/(1.-Sar-Swr);
      Number fixpom = pow( 1.- pow(1.-fixSa,1./m), m);
      return (Sa-Sar)*1e3*K_/visc_a*cbrt(fixSa)*fixpom*fixpom*getrho_a(pa);
    }
    Number eSa = (Sa-Sar)/(1.-Sar-Swr);
    Number pom = pow( 1.- pow(1.-eSa,1./m), m);
    Number result = K_/visc_a*cbrt(eSa)*pom*pom;
    if (result != result)
    {
      // std::cout << "NaN in param.dKar_dphi";
      DUNE_THROW(ParameterClassException,"air permeability derived by porosity NaN");
    }
    return result*getrho_a(pa);
  }

  Number Kac(const Number& Sa, const Number& porosity_, const Number& co2_) const
  {
    Number K_=K(porosity_)*Ka_correction;
    if (Sa<=Sar) // (Sa<=Sar+1e-3)
      return 0.;
    else if (Sa>=1.-Swr-1e-3)
      return K_/visc_mix(co2_);
    else if (Sa<=Sar+1e-3)
    {
      Number fixSa = 1e-3/(1.-Sar-Swr);
      Number fixpom = pow( 1.- pow(1.-fixSa,1./m), m);
      return (Sa-Sar)*1e3*K_/visc_mix(co2_)*cbrt(fixSa)*fixpom*fixpom;
    }
    Number eSa = (Sa-Sar)/(1.-Sar-Swr);
    Number pom = pow( 1.- pow(1.-eSa,1./m), m);
    Number result = K_/visc_mix(co2_)*cbrt(eSa)*pom*pom;
    if (result != result)
    {
      // std::cout << "NaN in param.Kac";
      DUNE_THROW(ParameterClassException,"air permeability NaN");
    }
    return result;
  }

  Number dKac_dSa(const Number& Sa, const Number& porosity_, const Number& co2_) const
  {
    Number K_=K(porosity_)*Ka_correction;
    if (Sa<=Sar) // (Sa<=Sar+1e-3)
      return 0.;
    else if (Sa>=1.-Swr-1e-3)
      return 0.;
    else if (Sa<=Sar+1e-3)
    {
      Number fixSa = 1e-3/(1.-Sar-Swr);
      Number fixpom = pow( 1.- pow(1.-fixSa,1./m), m);
      return 1e3*K_/visc_mix(co2_)*cbrt(fixSa)*fixpom*fixpom;
    }
    Number eSa = (Sa-Sar)/(1.-Sar-Swr);
    Number pom = pow( 1.- pow(1.-eSa,1./m), m);
    Number result = K_/visc_mix(co2_)/(1.-Sar-Swr)*(cbrt(eSa)/(3.*eSa)*pom*pom+cbrt(eSa)*2.*pom*pow(1.-pow(1.-eSa,1./m),m-1.)*pow(1.-eSa,1./m-1.));
                                                                                // two minus signs  and  m*1/m  are cancelled
    if (result != result)
    {
      // std::cout << "NaN in param.dKac_dSa";
      DUNE_THROW(ParameterClassException,"air permeability derived by saturation NaN");
    }
    return result;
  }

  Number dKac_dphi(const Number& Sa, const Number& porosity_, const Number& co2_) const
  {
    Number K_=K_deriv(porosity_)*Ka_correction;
    if (Sa<=Sar) // (Sa<=Sar+1e-3)
      return 0.;
    else if (Sa>=1.-Swr-1e-3)
      return K_/visc_mix(co2_);
    else if (Sa<=Sar+1e-3)
    {
      Number fixSa = 1e-3/(1.-Sar-Swr);
      Number fixpom = pow( 1.- pow(1.-fixSa,1./m), m);
      return (Sa-Sar)*1e3*K_/visc_mix(co2_)*cbrt(fixSa)*fixpom*fixpom;
    }
    Number eSa = (Sa-Sar)/(1.-Sar-Swr);
    Number pom = pow( 1.- pow(1.-eSa,1./m), m);
    Number result = K_/visc_mix(co2_)*cbrt(eSa)*pom*pom;
    if (result != result)
    {
      // std::cout << "NaN in param.dKac_dphi";
      DUNE_THROW(ParameterClassException,"air permeability derived by porosity NaN");
    }
    return result;
  }

  Number dKac_dco2(const Number& Sa, const Number& porosity_, const Number& co2_) const
  {
    Number K_=K(porosity_)*Ka_correction;
    Number visc_sq=visc_mix(co2_);
    visc_sq *= visc_sq;
    if (Sa<=Sar) // (Sa<=Sar+1e-3)
      return 0.;
    else if (Sa>=1.-Swr-1e-3)
      return K_*(-1.)/visc_sq*dvisc_mix_dco2(co2_);
    else if (Sa<=Sar+1e-3)
    {
      Number fixSa = 1e-3/(1.-Sar-Swr);
      Number fixpom = pow( 1.- pow(1.-fixSa,1./m), m);
      return (Sa-Sar)*1e3*K_*(-1.)/visc_sq*dvisc_mix_dco2(co2_)*cbrt(fixSa)*fixpom*fixpom;
    }
    Number eSa = (Sa-Sar)/(1.-Sar-Swr);
    Number pom = pow( 1.- pow(1.-eSa,1./m), m);
    Number result = K_*(-1.)/visc_sq*dvisc_mix_dco2(co2_)*cbrt(eSa)*pom*pom;
    if (result != result)
    {
      // std::cout << "NaN in param.dKac_dco2";
      DUNE_THROW(ParameterClassException,"air permeability derived by CO2 NaN");
    }
    return result;
  }

  inline Number Karc(const Number& Sa, const Number& pa, const Number& porosity_, const Number& co2_) const
  {
    return Kac(Sa,porosity_,co2_)*getrho_a(pa,co2_);
  }

  inline Number dKarc_dpw(const Number& Sa, const Number& pa, const Number& porosity_, const Number& co2_) const
  {
    return Kac(Sa,porosity_,co2_)*drho_a_dpa(pa,co2_);
  }

  inline Number dKarc_dSa(const Number& Sa, const Number& pa, const Number& porosity_, const Number& co2_) const
  {
    return dKac_dSa(Sa,porosity_,co2_)*getrho_a(pa,co2_)+dKarc_dpw(Sa,pa,porosity_,co2_)*pc_deriv(Sa);
  }

  inline Number dKarc_dphi(const Number& Sa, const Number& pa, const Number& porosity_, const Number& co2_) const
  {
    return dKac_dphi(Sa,porosity_,co2_)*getrho_a(pa,co2_);
  }

  inline Number dKarc_dco2(const Number& Sa, const Number& pa, const Number& porosity_, const Number& co2_) const
  {
    return dKac_dco2(Sa,porosity_,co2_)*getrho_a(pa,co2_)+Kac(Sa,porosity_,co2_)*drho_a_dco2(pa,co2_);
  }

  inline void setinflowl (const Number& inflowl_)
  {
    inflowl = inflowl_;
  }

  inline Number getinflowl() const
  {
    return inflowl;
  }

  inline void setinflowr (const Number& inflowr_)
  {
    inflowr = inflowr_;
  }

  inline Number getinflowr() const
  {
    return inflowr;
  }

  inline Number getheight() const
  {
    return height;
  }

  inline Number getwidth() const
  {
    return width;
  }

  inline Number getvisc_w() const
  {
    return visc_w;
  }

  inline Number getvisc_a() const
  {
    return visc_a;
  }

  inline Number getvisc_c() const
  {
    return visc_c;
  }

  inline void setvisc_c(const Number& visc_c_)
  {
    visc_c = visc_c_;
  }

  inline const Number& getrho_w() const
  {
    return rho_w;
  }

  inline Number getrho_a(const Number& pa_, const Number& co2_) const
  {
    return (getpa0()+pa_)/(R*T)*( (Ma-0.0004*Mco2)/(1.-0.0004)*(1.-co2_)+Mco2*co2_ );
  }

  inline Number drho_a_dpa(const Number& pa_, const Number& co2_) const
  {
    return ( (Ma-0.0004*Mco2)/(1.-0.0004)*(1.-co2_)+Mco2*co2_ )/(R*T);
  }

  Number drho_a_dco2(const Number& pa_, const Number& co2_) const
  {
    return (getpa0()+pa_)/(R*T)*( (Ma-0.0004*Mco2)/(1.-0.0004)*(-1.)+Mco2 );
  }

  Number visc_mix(const Number& co2_) const // viscosit of air mixture
  {
    Number fi_ac = (1.+sqrt(visc_a/visc_c)*sqrt(sqrt(Mco2/Ma)));
    fi_ac *= fi_ac;
    fi_ac /= (4./sqrt(2.)*sqrt(1.+Ma/Mco2));
    Number fi_ca = (1.+sqrt(visc_c/visc_a)*sqrt(sqrt(Ma/Mco2)));
    fi_ca *= fi_ca;
    fi_ca /= (4./sqrt(2.)*sqrt(1.+Mco2/Ma));
    Number visc_mix = (1.-co2_)*visc_a/((1.-co2_)+  co2_   *fi_ac)
                     +    co2_ *visc_c/(  co2_   +(1.-co2_)*fi_ca);
    // Number visc_mix = (1.-co2_+0.0004)*visc_a/((1.-co2_)+co2_*fi_ac) // since normal air contains 0.0004 CO2, it is weirdly shifted
    //                  +(   co2_-0.0004)*visc_c/(co2_+(1.-co2_)*fi_ca);// but it has no visible effect on resulting value
    return visc_mix;
  }

  Number dvisc_mix_dco2(const Number& co2_) const
  {
    Number fi_ac = (1.+sqrt(visc_a/visc_c)*sqrt(sqrt(Mco2/Ma)));
    fi_ac *= fi_ac;
    fi_ac /= (4./sqrt(2.)*sqrt(1.+Ma/Mco2));
    Number fi_ca = (1.+sqrt(visc_c/visc_a)*sqrt(sqrt(Ma/Mco2)));
    fi_ca *= fi_ca;
    fi_ca /= (4./sqrt(2.)*sqrt(1.+Mco2/Ma));
    Number dvisc_mixdco2 = (-1.)*visc_a/((1.-co2_)+  co2_   *fi_ac)+(1.-co2_)*visc_a*(-1.)/(((1.-co2_)+  co2_   *fi_ac)*((1.-co2_)+  co2_   *fi_ac))*(-1.+fi_ac)
                            +    visc_c/(  co2_   +(1.-co2_)*fi_ca)+    co2_ *visc_c*(-1.)/((  co2_   +(1.-co2_)*fi_ca)*(  co2_   +(1.-co2_)*fi_ca))*( 1.-fi_ca);
    return dvisc_mixdco2;
  }

  inline Number getetastab() const
  {
    return etastab;
  }

  inline Number getpa0() const
  {
    return p0;
  }

  inline Number getrho_a(const Number& pa) const
  {
    return Ma*(p0+pa)/(R*T);
  }

  inline Number rho_a_deriv() const
  {
    return Ma/(R*T);
  }

  inline Number getR() const
  {
    return R;
  }

  inline Number getT() const
  {
    return T;
  }

  inline Number getMa() const
  {
    return Ma;
  }

  inline Number getKa_correction() const
  {
    return Ka_correction;
  }

  //! boundary condition type function (true = Dirichlet), water
  template<typename I, typename X>
  bool b (const I& i, const X& x) const
  {
    // also affects boundary conditions of air and Ca, which use this function
    auto global = i.geometry().global(x);
    return (global[0]>=getwidth()-1e-7) ? true : false;
    // return (global[0]>=getwidth()-1e-7 || global[0]<=1e-7) ? true : false;
  }

  //! boundary condition type function
  template<typename I, typename X>
  bool ba (const I& i, const X& x) const
  {
    return b(i,x);
    // auto global = i.geometry().global(x);
    // return (global[0]>=getwidth()-1e-7) ? true : false;
    // return (global[0]>=getwidth()-1e-7 || global[0]<=1e-7) ? true : false;
  }

  //! Dirichlet extension, water
  // -works as dirichlet boundary condition and initial condition
  // boundary dirichlet is used only on the right, nuemann is elsewhere
  template<typename E, typename X>
  Number g (const E& e, const X& x) const
  {
    // gets value according to saturation and initial pressure pa=0
    // pw = pa-pc = -pc
    return -pc(ga(e,x));
    // auto global = e.geometry().global(x);
    // return -pc(0.051)+(pc(0.051)-pc(0.128))*global[0]/getwidth();
  }

  //! Dirichlet extension, air
  // -works as initial condition and dirichlet condition for right hand side
  template<typename E, typename X>
  Number ga (const E& e, const X& x) const
  {
    // air saturation determined by water pressure:
    // return 1.-getSw(-g(e,x));
    auto global = e.geometry().global(x);
    return (global[0]>=getwidth()-1e-7) ? 0.228 : 0.151;
    // return (global[0]>=getwidth()-1e-7) ? 0.128 : 0.051;
    // return 0.051+(0.128-0.051)*global[0]/getwidth();
  }

  template<typename I, typename X>
  inline Number ga_nitsche (const I& i, const X& x) const
  {
    return 0.;
  }

  template<typename E, typename X>
  Number getporosity (const E& e, const X& x) const
  {
    // initial porosity
    return getporosity();
  }

  template<typename E, typename X>
  Number getstoredCa (const E& e, const X& x) const
  {
    // initial calcium amount
    return getstoredCa();
  }

  //! Dirichlet extension, water
  template<typename E, typename X>
  Number g_flowright (const E& e, const X& x) const
  {
    auto global = e.geometry().global(x);
    return -pc(0.1)+(pc(0.1)-pc(0.4))*global[0]/getwidth();
  }

  //! Dirichlet extension, air
  // -works as initial condition and dirichlet condition for right hand side
  template<typename E, typename X>
  Number ga_flowright (const E& e, const X& x) const
  {
    // air saturation determined by water pressure:
    return 1.-getSw(-g_flowright(e,x));
  }
};