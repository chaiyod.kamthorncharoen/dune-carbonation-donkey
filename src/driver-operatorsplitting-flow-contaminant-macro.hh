#define FLOWRASPEN true
#define CHEMRASPEN true

template<typename ES, typename FEM, typename GV>
void driver (const ES& es, const FEM& fem, Dune::ParameterTree& ptree, const GV& gv)
{
  // dimension and important types
  // const int dim = ES::dimension;
  typedef double RF;                   // type for computations

  // get parameters
  const int ver = ptree.get<int>("parallel.verbose");
  int print_proc = ptree.get<int>("parallel.rank");
  const int max_newton = ptree.get<int>("parallel.max_newton");
  const int itNum = ptree.get<int>("operatorsplitting.iter",5);
  // const double oserr_Sa = ptree.get<double>("operatorsplitting.errSa");
  // const double oserr_phi = ptree.get<double>("operatorsplitting.errphi");
  const double osdefectf = ptree.get<double>("operatorsplitting.osdefectf");
  const double osdefectc = ptree.get<double>("operatorsplitting.osdefectc");
  const double osreduction = ptree.get<double>("operatorsplitting.osdefreduction");
  constexpr double dtup = 1.5; // 1.25 default till 1.10.2020
  constexpr double dtdown = 0.75; // 0.75 default
  #if FLOWRASPEN || CHEMRASPEN
  const int lineSearch = ptree.get<int>("parallel.lineSearch");
  const double red_outer = ptree.get<double>("parallel.red_outer");
  const double min_lin_red = ptree.get<double>("parallel.min_lin_red",1e-3);
  const bool useMaxNorm = ptree.get<bool>("parallel.useMaxNorm");
  const double abs_limit = ptree.get<double>("parallel.abs_limit");
  const unsigned int restart = ptree.get<int>("parallel.restart");
  const double red_inner1 = ptree.get<double>("parallel.red_inner1");
  const double red_inner2 = ptree.get<double>("parallel.red_inner2");
  #endif
  // const double vtkgap = ptree.get<double>("vtk.vtkgap");

  // Make grid function space
  typedef Dune::PDELab::OverlappingConformingDirichletConstraints COND; // parallel
  // typedef Dune::PDELab::ConformingDirichletConstraints COND; // sequential, or nonoverlapping parallel with ghosts
  // typedef Dune::PDELab::NoConstraints CON;
  typedef Dune::PDELab::ISTL::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<ES,FEM,COND,VBE> GFS;
  GFS gfs(es,fem);
  gfs.name("Vhd");
  int verbose=0;
  if (print_proc == -1)
    print_proc = gfs.gridView().comm().size()-1;
  if (gfs.gridView().comm().rank()==print_proc) verbose=ver;
  std::cout << "verbosity on rank " << gfs.gridView().comm().rank() << " is " << verbose << std::endl;

  using VBES = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  // last argument is either "fixed" or "none" to give istl information about structure
  // tag fixed stands for fixed size of blocks in the matrix
  // using OrderingTag = Dune::PDELab::LexicographicOrderingTag;
  using OrderingTag = Dune::PDELab::EntityBlockedOrderingTag;
  // lists coefficients in order, LexicographicOrderingTag has each vector space compact,
  // EntityBlockedOrderingTag puts DoFs from one entity together (order<=2, unless number of elements per entity may vary)
  using GFSF = Dune::PDELab::PowerGridFunctionSpace<GFS,2,VBES,OrderingTag>;
  using GFSC = Dune::PDELab::PowerGridFunctionSpace<GFS,4,VBES,OrderingTag>;
  // GFSF spaces may differ in constraints
  GFSF gfsf(gfs);
  GFSC gfsc(gfs);
  // gfsf names for VTK output
  using namespace Dune::TypeTree::Indices;
  gfsf.child(_0).name("pw");
  gfsf.child(_1).name("Sa");
  gfsc.child(_0).name("Ca");
  gfsc.child(_1).name("CO_2");
  gfsc.child(_2).name("phi");
  gfsc.child(_3).name("CaStored");

  // Coefficient vector
  using ZF = Dune::PDELab::Backend::Vector<GFSF, RF>;
  ZF zf(gfsf);
  using ZC = Dune::PDELab::Backend::Vector<GFSC, RF>;
  ZC zc(gfsc);

  // Parameter class
  RF time = 0.0;
  using ParamFlow = Parametre<RF>;
  ParamFlow problemd( ptree.get("material.aalfa",      (RF) 1.89e-2   )
                    , ptree.get("material.thetas",     (RF) 3.8e-1    )
                    , ptree.get("material.n",          (RF) 2.81      )
                    , ptree.get("material.Ks",         (RF) 2.4e-4    )
                    , ptree.get("grid.structured.LY",  (RF) 1.e1      )
                    , ptree.get("grid.structured.LX",  (RF) 5.        )
                    , ptree.get("material.grav",       (RF)-9.81      )
                    , ptree.get("material.viscosity_w",(RF) 1.002e-3  )
                    , ptree.get("material.viscosity_a",(RF) 1.8369e-5 )
                    , ptree.get("material.rho_w",      (RF) 9.98205e2 )
                    , ptree.get("material.Swr",        (RF) 0.        )
                    , ptree.get("material.Sar",        (RF) 0.        )
                    , ptree.get("problem.etastab",     (RF) 1.e2      )
                    , ptree.get("material.air.R",      (RF) 8.3144598 )
                    , ptree.get("material.air.T",      (RF) 2.9315e2  )
                    , ptree.get("material.air.Ma",     (RF) 2.89626e-2)
                    , ptree.get("material.air.Mc",     (RF) 4.401e-2  )
                    , ptree.get("material.air.p0",     (RF) 1.01325e5 )
                    , ptree.get("regularization.Ka_correction", (RF) 50.)
                    , ptree.get("regularization.lscheme_co2", (RF) 10.)
                      );
  problemd.setTime(time);
  // parameter class
  Parametre_chem<RF, ParamFlow> param_chem( problemd
                                          , ptree.get("heat.alfa_l",               (RF) 1.      )
                                          , ptree.get("chem.ini_Ca",               (RF) 2.2e1   )
                                          , ptree.get("chem.ini_co2",              (RF) 0.      )
                                          , ptree.get("chem.difCa",                (RF) 2.3e-13 )
                                          , ptree.get("chem.difCae",               (RF) 9.95    )
                                          , ptree.get("chem.difco2",               (RF) 8.e-7   )
                                          , ptree.get("chem.airco2",               (RF) 4.e-4   )
                                          , ptree.get("chem.carbonation.carba",    (RF) 1.5     )
                                          , ptree.get("chem.carbonation.carbb",    (RF) 1.5     )
                                          , ptree.get("chem.carbonation.carbtheta",(RF) 1.23e-1 )
                                          , ptree.get("chem.carbonation.VCaCO_3",  (RF) 3.693e-5)
                                          , ptree.get("material.viscosity_c",      (RF) 1.48e-5 )
                                            );
  // if negative, sets treshold between setting_to_zero and failed_timestep
  RF tresholdCa  = ptree.get("regularization.tresholdCa" ,(RF) -1e-6);
  RF tresholdco2 = ptree.get("regularization.tresholdco2",(RF) -1e-4);
  // initial condition
  auto glambdaf = [&](const auto& e, const auto& x)
  {
    Dune::FieldVector<RF,2> rf;
    rf[0] = problemd.g(e,x);
    rf[1] = problemd.ga(e,x);
    return rf;
  };
  auto glambdac = [&](const auto& e, const auto& x)
  {
    Dune::FieldVector<RF,4> rf;
    rf[0] = param_chem.g_Ca(e,x);
    rf[1] = param_chem.g_co2(e,x);
    rf[2] = param_chem.g_porosity(e,x);
    rf[3] = param_chem.g_CaStored(e,x);
    return rf;
  };
  auto gf = Dune::PDELab::
    makeInstationaryGridFunctionFromCallable(es,glambdaf,problemd);
  auto gc = Dune::PDELab::
    makeInstationaryGridFunctionFromCallable(es,glambdac,problemd);
  Dune::PDELab::interpolate(gf,gfsf,zf);
  Dune::PDELab::interpolate(gc,gfsc,zc);

  // boundary conditions - bool, true==dirichlet
  auto b0lambda = [&](const auto& i, const auto& x){return problemd.b(i,x);};
  auto bf0 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b0lambda);
  auto b1lambda = [&](const auto& i, const auto& x){return problemd.ba(i,x);};
  auto bf1 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b1lambda);
  auto b2lambda = [&](const auto& i, const auto& x){return param_chem.bCa(i,x);};
  auto bf2 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b2lambda);
  auto b3lambda = [&](const auto& i, const auto& x){return param_chem.bco2(i,x);};
  auto bf3 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b3lambda);
  auto b4lambda = [&](const auto& i, const auto& x){return false;};
  auto bf4 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b4lambda);
  auto b5lambda = [&](const auto& i, const auto& x){return false;};
  auto bf5 = Dune::PDELab::makeBoundaryConditionFromCallable(es,b5lambda);

  using BF = Dune::PDELab::CompositeConstraintsParameters<decltype(bf0),decltype(bf1)>;
  using BC = Dune::PDELab::CompositeConstraintsParameters<decltype(bf2),decltype(bf3),decltype(bf4),decltype(bf5)>;
  BF bf(bf0,bf1);
  BC bc(bf2,bf3,bf4,bf5);

  // Assemble constraints
  typedef typename GFSF::template ConstraintsContainer<RF>::Type CF;
  CF cf;
  Dune::PDELab::constraints(bf,gfsf,cf); // assemble constraints
  if (verbose>=1)
    std::cout << "flow part constrained dofs=" << cf.size() << " of "
              << gfsf.globalSize() << std::endl;
  typedef typename GFSC::template ConstraintsContainer<RF>::Type CC;
  CC cc;
  Dune::PDELab::constraints(bc,gfsc,cc); // assemble constraints
  if (verbose>=1)
    std::cout << "transport part constrained dofs=" << cc.size() << " of "
              << gfsc.globalSize() << std::endl;

  // initialize simulation time, the coefficient vector
  ZF zfnew(zf);  // result from apply()
  ZC zcnew(zc);  // result from apply()
  ZF zfstep(zf); // stores previous op-split step to calculate correction obtained with new op-split iteration
  ZC zcstep(zc); // stores previous op-split step to calculate correction obtained with new op-split iteration
  ZF zfdata(zf); // passes flow        data (which are being iterated) to other systems, zfnew suffices unless symmetric addition scheme is used
  ZC zcdata(zc); // passes contaminant data (which are being iterated) to other systems, zcnew suffices unless symmetric addition scheme is used
  ZF zfold(zf); // keeps value of the current time step, to enable better predictor in operator splitting
  ZC zcold(zc); // keeps value of the current time step, to enable better predictor in operator splitting
  ZF zftest(zf);
  ZC zctest(zc);

  // Make instationary grid operator
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  MBE mbe((int)10);
  typedef LOP_spatial_flow<Parametre_chem<RF,ParamFlow>,GFSC,ZC> LOPF;
  // typedef LOP_spatial_flow<Parametre_chem<RF,ParamFlow>,GFSC,ZC> LOPF;
  LOPF lopf(param_chem,gfsc,zcdata,zc);
  typedef LOP_time_flow<Parametre_chem<RF,ParamFlow>,GFSC,ZC> TLOPF;
  TLOPF tlopf(param_chem,gfsc,zcdata,zc);
  #if FLOWRASPEN
  typedef Dune::PDELab::Raspen::GridOperator<GFSF,GFSF,LOPF,MBE,
                                     RF,RF,RF,CF,CF> GOF0;
  typedef Dune::PDELab::Raspen::GridOperator<GFSF,GFSF,TLOPF,MBE,
                                     RF,RF,RF,CF,CF> GOF1;
  typedef Dune::PDELab::Raspen::OneStepGridOperator<GOF0,GOF1> IGOF;
  #else
  typedef Dune::PDELab::GridOperator<GFSF,GFSF,LOPF,MBE,
                                     RF,RF,RF,CF,CF> GOF0;
  typedef Dune::PDELab::GridOperator<GFSF,GFSF,TLOPF,MBE,
                                     RF,RF,RF,CF,CF> GOF1;
  typedef Dune::PDELab::OneStepGridOperator<GOF0,GOF1> IGOF;
  #endif
  GOF0 gof0(gfsf,cf,gfsf,cf,lopf,mbe);
  GOF1 gof1(gfsf,cf,gfsf,cf,tlopf,mbe);
  IGOF igof(gof0,gof1);
  // same for contaminant transport, not sure if I need second MBE
  typedef LOP_spatial_contaminant_reduced<Parametre_chem<RF,ParamFlow>,GFSF,ZF,GFSC,ZC,7> LOPC;
  // typedef LOP_spatial_contaminant<Parametre_chem<RF,ParamFlow>,GFSF,ZF,GFSC,ZC> LOPC;
  LOPC lopc(param_chem,gfsf,zfdata,zf,gfsc,zcdata,zc);
  typedef LOP_time_contaminant_reduced<Parametre_chem<RF,ParamFlow>,GFSF,ZF> TLOPC;
  TLOPC tlopc(param_chem,gfsf,zfdata,zf);
  #if CHEMRASPEN
  typedef Dune::PDELab::Raspen::GridOperator<GFSC,GFSC,LOPC,MBE,
                                     RF,RF,RF,CC,CC> GOC0;
  typedef Dune::PDELab::Raspen::GridOperator<GFSC,GFSC,TLOPC,MBE,
                                     RF,RF,RF,CC,CC> GOC1;
  typedef Dune::PDELab::Raspen::OneStepGridOperator<GOC0,GOC1> IGOC;
  #else
  typedef Dune::PDELab::GridOperator<GFSC,GFSC,LOPC,MBE,
                                     RF,RF,RF,CC,CC> GOC0;
  typedef Dune::PDELab::GridOperator<GFSC,GFSC,TLOPC,MBE,
                                     RF,RF,RF,CC,CC> GOC1;
  typedef Dune::PDELab::OneStepGridOperator<GOC0,GOC1> IGOC;
  #endif
  GOC0 goc0(gfsc,cc,gfsc,cc,lopc,mbe);
  GOC1 goc1(gfsc,cc,gfsc,cc,tlopc,mbe);
  IGOC igoc(goc0,goc1);
  // -----RASPEN-PART-FLOW----------------------------------------------------------------------------------------------------------------------
  #if FLOWRASPEN
  ZF v(gfsf);                 // store internal values
  ZF part_unity(gfsf,1.0);    // Initialize vector to 1

  // Set to zero on subdomain boundaries
  Dune::PDELab::set_constrained_dofs(cf,0.0,part_unity);

  Dune::PDELab::AddDataHandle<GFSF,ZF> partitionunityhandle(gfsf,part_unity);
  gfsf.gridView().communicate(partitionunityhandle,Dune::All_All_Interface,           Dune::ForwardCommunication);
  // gfsf.gridView().communicate(partitionunityhandle,Dune::InteriorBorder_All_Interface,Dune::ForwardCommunication);

  Dune::PDELab::set_constrained_dofs(cf,0.0,part_unity);
  // Zero on subdomain boundary (Need that a 2nd time due to add comm before!)

  // Divide 1/x
  // for (auto iter = part_unity.begin(); iter != part_unity.end(); iter++) { if (*iter > 0) *iter = 1.0 / *iter; }
  //same using range for:
  for (auto& p_u : part_unity)
  {
    if (p_u>0.) p_u=1.0/p_u;
  }


  // Fill the coefficient vector
  Dune::PDELab::interpolate(gf,gfsf,v);
  // my case Dune::PDELab::interpolate(gf,gfsf,zf);

  // make vector consistent NEW IN PARALLEL
  Dune::PDELab::ISTL::ParallelHelper<GFSF> helper(gfsf);
  helper.maskForeignDOFs(v);
  Dune::PDELab::AddDataHandle<GFSF,ZF> addvdh(gfsf,v);
  if (gfsf.gridView().comm().size()>1)
  {
    gfsf.gridView().communicate(addvdh,Dune::InteriorBorder_All_Interface,Dune::ForwardCommunication);
  }

  for (const auto& col : cf)
  {
    v[col.first] = 0.0;
  }
  v -= zf;    //store internal values


  // typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LSF; // RASPEN uses sequential solver for single rank
  typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LSF;
  LSF lsf(100,0); // SEQ_SuperLU, SEQ_UMFPack
  typedef Dune::PDELab::Raspen::RestrainedNewton<IGOF,LSF,ZF> PDESOLVER; // renamed RASPEN
  // typedef Dune::PDELab::Raspen::TwoLevelRASPEN<IGOF,LSF,ZF> PDESOLVER; // renamed RASPEN
  PDESOLVER pdesolver(igof,zf,v,part_unity,lsf); // RASPEN
  pdesolver.setReassembleThreshold(0.0);
  pdesolver.setReduction(red_outer);
  pdesolver.setMinLinearReduction(min_lin_red);
  pdesolver.setMaxIterations(max_newton);
  pdesolver.setLineSearchMaxIterations(lineSearch);
  pdesolver.setAbsoluteLimit(abs_limit);
  pdesolver.setVerbosityLevel(verbose, print_proc); // RASPEN, classic has inly first argument
  pdesolver.setReduction_inner1(red_inner1);  // RASPEN only
  pdesolver.setReduction_inner2(red_inner2);  // RASPEN only
  pdesolver.setRestart(restart);              // RASPEN only
  // pdesolver.setComponents(); // two-level
  pdesolver.setParameters(ptree.sub("flow.LineSearch"));
  pdesolver.setUseMaxNorm(useMaxNorm);

  // write-to-file part

          //print log file
          std::string name = "result from processor_";
          name += std::to_string(gfs.gridView().comm().rank());
          name += ".txt";
          static std::ofstream tfile;
          tfile.open (name);
  #else
  typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_SSORk<GFSF,CF> LSF;
  LSF lsf(gfsf,cf,30,5,verbose); // for overlapping BCGS_SSORk
  typedef Dune::PDELab::NewtonMethod<IGOF,LSF> PDESOLVER;
  PDESOLVER pdesolver(igof,lsf); // classic
  Dune::ParameterTree newtonParam;
  newtonParam = ptree.sub("flow");
  newtonParam["VerbosityLevel"] = std::to_string(verbose);
  pdesolver.setParameters(newtonParam);
  if (verbose > 0)
    pdesolver.printParameters("Flow system");
  #endif

  // -END-RASPEN-PART-FLOW----------------------------------------------------------------------------------------------------------------------
  // -----RASPEN-PART-CONTAMINANT---------------------------------------------------------------------------------------------------------------
  #if CHEMRASPEN
  ZC w(gfsc);                 // store internal values
  ZC part_unityc(gfsc,1.0);    // Initialize vector to 1

  // Set to zero on subdomain boundaries
  Dune::PDELab::set_constrained_dofs(cc,0.0,part_unityc);
  Dune::PDELab::AddDataHandle<GFSC,ZC> partitionunityhandlec(gfsc,part_unityc);
  gfsc.gridView().communicate(partitionunityhandlec,Dune::All_All_Interface,           Dune::ForwardCommunication);
  Dune::PDELab::set_constrained_dofs(cc,0.0,part_unityc);
  // Zero on subdomain boundary (Need that a 2nd time due to add comm before!)
  // Divide 1/x
  for (auto& p_u : part_unityc)
  {
    if (p_u>0.) p_u=1.0/p_u;
  }
  // Fill the coefficient vector
  Dune::PDELab::interpolate(gc,gfsc,w);
  // make vector consistent NEW IN PARALLEL
  Dune::PDELab::ISTL::ParallelHelper<GFSC> helperc(gfsc);
  helperc.maskForeignDOFs(w);
  Dune::PDELab::AddDataHandle<GFSC,ZC> addvdhc(gfsc,w);
  if (gfsc.gridView().comm().size()>1)
  {
    gfsc.gridView().communicate(addvdhc,Dune::InteriorBorder_All_Interface,Dune::ForwardCommunication);
  }

  for (const auto& col : cc)
  {
    w[col.first] = 0.0;
  }
  w -= zc;    //store internal values
  // typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LSC; // RASPEN uses
  typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LSC; // RASPEN uses
  LSC lsc(100,0); // SEQ_SuperLU, SEQ_UMFPack
  typedef Dune::PDELab::Raspen::RestrainedNewton<IGOC,LSC,ZC> PDESOLVERC; // renamed RASPEN
  // typedef Dune::PDELab::Raspen::TwoLevelRASPEN<IGOC,LSC,ZC> PDESOLVERC; // renamed RASPEN
  PDESOLVERC pdesolverc(igoc,zc,w,part_unityc,lsc); // RASPEN
  pdesolverc.setReassembleThreshold(0.0);
  pdesolverc.setReduction(red_outer);
  pdesolverc.setMinLinearReduction(min_lin_red);
  pdesolverc.setMaxIterations(max_newton);
  pdesolverc.setLineSearchMaxIterations(lineSearch);
  pdesolverc.setAbsoluteLimit(abs_limit);
  pdesolverc.setVerbosityLevel(verbose, print_proc); // RASPEN, classic has only first argument
  pdesolverc.setReduction_inner1(red_inner1);  // RASPEN only
  pdesolverc.setReduction_inner2(red_inner2);  // RASPEN only
  pdesolverc.setRestart(restart);              // RASPEN only
  // pdesolverc.setComponents(); // two-level
  pdesolverc.setParameters(ptree.sub("chem.LineSearch"));
  const bool useMaxNormc = ptree.get<bool>("chem.UseMaxNorm");
  pdesolverc.setUseMaxNorm(useMaxNormc);
  // write-to-file part
  //print log file
  std::string namec = "resultc from processor_";
  namec += std::to_string(gfs.gridView().comm().rank());
  namec += ".txt";
  static std::ofstream tfilec;
  tfilec.open (namec);
  #else
  // typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_UMFPack<GFSC,CC> LSC;
  // typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_SuperLU<GFSC,CC> LSC; // slower than UMFPack, took 2.57e+4s to get to 5.26e+6 time in simulation, 10613 "timesteps"
  typedef Dune::PDELab::ISTLBackend_OVLP_BCGS_SSORk<GFSC,CC> LSC;
  // typedef Dune::PDELab::ISTLBackend_OVLP_Base<GFSC,CC,Dune::SeqSSOR,Dune::BiCGSTABSolver> LSC;
  // typedef Dune::PDELab::ISTLBackend_OVLP_Base<GFSC,CC,Dune::SeqOverlappingSchwarz,Dune::BiCGSTABSolver> LSC;
  LSC lsc(gfsc,cc,30,5,verbose); // BCGS_SSORk, and OVLP_Base, parameters gfsc,cc,maxiter=5000,steps=5,verbose=1
  // LSC lsc(gfsc,cc,100,verbose); // BCGS_UMFPack, BCGS_SuperLU
  typedef Dune::PDELab::NewtonMethod<IGOC,LSC> PDESOLVERC; // classic
  PDESOLVERC pdesolverc(igoc,lsc); // classic
  Dune::ParameterTree newtonParam_c;
  newtonParam_c = ptree.sub("chem");
  newtonParam_c["VerbosityLevel"] = std::to_string(verbose);
  pdesolverc.setParameters(newtonParam_c);
  if (verbose > 0)
    pdesolverc.printParameters("Chem system");
  #endif
  // -END-RASPEN-PART-CONTAMINANT---------------------------------------------------------------------------------------------------------------

  // select and prepare time-stepping scheme
  Dune::PDELab::OneStepThetaParameter<RF> method1(1.0);
  Dune::PDELab::TimeSteppingParameterInterface<RF>* pmethod=&method1;
  #if FLOWRASPEN
  typedef Dune::PDELab::Raspen::OneStepMethod<GFSF,RF,IGOF,PDESOLVER,ZF,ZF> OSMF; // RASPEN counterpart has extra GFSF template argument
  OSMF  osmf(*pmethod,igof,pdesolver,print_proc);
  #else
  typedef Dune::PDELab::OneStepMethod<RF,IGOF,PDESOLVER,ZF,ZF> OSMF; // classic
  OSMF  osmf(*pmethod,igof,pdesolver);
  #endif
  #if CHEMRASPEN
  typedef Dune::PDELab::Raspen::OneStepMethod<GFSC,RF,IGOC,PDESOLVERC,ZC,ZC> OSMC; // RASPEN
  OSMC  osmc(*pmethod,igoc,pdesolverc,print_proc);
  #else
  typedef Dune::PDELab::OneStepMethod<RF,IGOC,PDESOLVERC,ZC,ZC> OSMC; // classic
  OSMC  osmc(*pmethod,igoc,pdesolverc);
  #endif

  osmf.setVerbosityLevel(verbose);
  osmc.setVerbosityLevel(verbose);

  // // adapter for calculating corrections of iterative operator splitting
  // typedef Dune::PDELab::GridFunctionSubSpace<GFSF,Dune::TypeTree::HybridTreePath<Dune::index_constant<1>>> GFSF_Sub1;
  // GFSF_Sub1 gfsf_sub1(gfsf);
  // typedef Dune::PDELab::DiscreteGridFunction<GFSF_Sub1,ZF> SubSa;
  // SubSa subSa(gfsf_sub1,zfstep);
  // SubSa subSanew(gfsf_sub1,zfnew);
  // typedef Dune::PDELab::DifferenceSquaredAdapter<SubSa,SubSa> ErrSa;
  // ErrSa errSa(subSa,subSanew);
  // RF sperrSa;
  // typedef Dune::PDELab::GridFunctionSubSpace<GFSC,Dune::TypeTree::HybridTreePath<Dune::index_constant<2>>> GFSC_Sub2;
  // GFSC_Sub2 gfsc_sub2(gfsc);
  // typedef Dune::PDELab::DiscreteGridFunction<GFSC_Sub2,ZC> SubPhi;
  // SubPhi subphi(gfsc_sub2,zcstep);
  // SubPhi subphinew(gfsc_sub2,zcnew);
  // typedef Dune::PDELab::DifferenceSquaredAdapter<SubPhi,SubPhi> Errphi;
  // Errphi errphi(subphi,subphinew);
  // RF sperrphi;

  // prepare VTK writer and write first file
  int subsampling=ptree.get("output.subsampling",(int)0);
  using VTKWRITER = Dune::SubsamplingVTKWriter<GV>;
  VTKWRITER vtkwriter(gv,Dune::refinementIntervals(subsampling));
  std::string filenamef= ptree.get("output.filenameoperatorsplitting","operatorsplitting-flow-contaminant");
  struct stat stf;
  if( stat( filenamef.c_str(), &stf ) != 0 )
  {
    int statf = 0;
    statf = mkdir(filenamef.c_str(),S_IRWXU|S_IRWXG|S_IRWXO);
    if( statf != 0 && statf != -1)
      std::cout << "Error: Cannot create directory "
                << filenamef << std::endl;
  }
  using VTKSEQUENCEWRITER = Dune::VTKSequenceWriter<GV>;
  VTKSEQUENCEWRITER vtkSequenceWriter(
    std::make_shared<VTKWRITER>(vtkwriter),filenamef,filenamef,"");
  // add data field for all components of the space to the VTK writer
  Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter,gfsf,zf);
  Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter,gfsc,zc);
  vtkSequenceWriter.write(0.0,Dune::VTK::appendedraw);
  std::vector<RF> vtktimes;

  // time loop --------------------------------------------------------------------
  RF T = ptree.get("problem.T",(RF)1.0);
  uint screenshots = ptree.get("vtk.screenshots",uint(30));
  screenshots /= 2;
  RF linq = T/RF(screenshots);
  RF geoq = std::pow(T,1./RF(screenshots));
  RF qq = geoq;
  for (uint i=1; i<screenshots; ++i)
  {
    vtktimes.push_back(linq*RF(i));
    vtktimes.push_back(qq);
    qq *= geoq;
  }
  vtktimes.push_back(T);
  std::sort(vtktimes.begin(),vtktimes.end(),std::greater<RF>());
  if (verbose >= 1)
  {
    for (auto v : vtktimes)
      std::cout << v << ", ";
    std::cout << std::endl;
  }
  RF nextvtk = vtktimes[vtktimes.size()-1];
  vtktimes.pop_back();
  unsigned int pocetiteraciif{0};
  unsigned int pocetiteraciic{0};
  unsigned int maxiter{0};
  std::vector<unsigned int> uspesneiterf(max_newton+1);
  std::vector<unsigned int> uspesneiterc(max_newton+1);
  problemd.setTime(time);
  problemd.setOtherTime(time);
  problemd.setTimeStep(ptree.get("fem.dt",(RF)0.1));
  // RF lastvtk = 0.; // the time of last created vtk file, used to avoid creating too many frames if timestep is small
  std::vector<double> timestephistory{};

  mojefunkcie::writer timestephistoryfile(filenamef+".txt");
  mojefunkcie::writer usedOSiter(filenamef+"usedOSiter.txt"); // what and after how many OS iter
  std::map<std::string,int> failstats;
  failstats.insert(std::make_pair("negativeConcentration",0));
  failstats.insert(std::make_pair("solverException",0));
  failstats.insert(std::make_pair("opsplitnotconverged",0));
  failstats.insert(std::make_pair("unknowntype",0));
  // Check in which operator-splitting iteration the computation failed.
  // The way convergence is implemented gives max number of iter itNum+1.
  for (int i=0; i<itNum+2; ++i)
    failstats.insert(std::make_pair("opsplitIter"+std::to_string(i),0));

  int iter_number{0};
  int finish_opsplit{0}; // 0=continue op-splitting iteration, 1=next time step, 100=restart with smaller dt
  int should_throw{0};
  // calculate flow system, update zfdata, and check for negative air saturation
  auto applyflow = [&] (RF dt) // -> void
  {
    if (verbose>=1)
      std::cout << std::endl << std::endl << "flow part:\n";
    #if FLOWRASPEN
    osmf.apply(time,dt,zf,gf,zfnew,v,part_unity,gfsf); // RASPEN has extra parameters
    #else
    osmf.apply(time,dt,zf,gf,zfnew); // classic
    #endif
    // should_throw += mojefunkcie::check_blockvector_for_negatives(zfnew,0.,1,"air saturation");
    // should_throw = gv.comm().max(should_throw);
    // if (should_throw != 0){
    //   ++failstats["negativeConcentration"];
    //   DUNE_THROW(NegativeConcentrationError,"Negative air saturation");
    // }
    zfdata = zfnew;
  };
  // calculate diffusion system, update zcdata, check for too negative Ca, co2
  auto applycontaminant = [&] (RF dt) // -> void
  {
    if (verbose>=1)
      std::cout << std::endl << std::endl << "contaminant part:\n";
    #if CHEMRASPEN
    osmc.apply(time,dt,zc,gc,zcnew,w,part_unityc,gfsc); // RASPEN has extra parameters
    #else
    osmc.apply(time,dt,zc,gc,zcnew); // classic
    #endif
    should_throw = mojefunkcie::check_blockvector_for_negatives(zcnew,tresholdCa,0,"Ca concentration");
    should_throw += mojefunkcie::check_blockvector_for_negatives(zcnew,tresholdco2,1,"co2 concentration");
    should_throw = gv.comm().max(should_throw);
    if (should_throw != 0){
      ++failstats["negativeConcentration"];
      DUNE_THROW(NegativeConcentrationError,"Ca or co2 concentration too negative");
    }
    zcdata = zcnew;
  };
  // updates maxiter for time step size and store iteration numbers for statistics
  auto store_iteration_number = [&] () // -> void
  {
    pocetiteraciif=pdesolver.result().iterations;
    pocetiteraciic=pdesolverc.result().iterations;
    uspesneiterf.at(pocetiteraciif)+=1;
    uspesneiterc.at(pocetiteraciic)+=1;
    maxiter = std::max(maxiter,std::max(pocetiteraciif,pocetiteraciic));
  };
  // // calculate correction sizes of Sa, phi, and returns their L2 norms squared
  // auto compute_splitting_correction = [&] () // -> std::tuple<RF,RF>
  // {
  //   typename ErrSa::Traits::RangeType spliterrorSa;
  //   Dune::PDELab::integrateGridFunction(errSa,spliterrorSa,1);
  //   typename Errphi::Traits::RangeType spliterrorphi;
  //   Dune::PDELab::integrateGridFunction(errphi,spliterrorphi,1);
  //   return std::make_tuple(spliterrorSa.infinity_norm(),spliterrorphi.infinity_norm());
  // };
  // // decide to repeat operator splitting iteration, go to next time step, or restart time step
  // auto check_op_split_convergence = [&] () // -> void
  // {
  //   if (sperrSa < oserr_Sa && sperrphi < oserr_phi && iter_number > 0)
  //   {
  //     if (verbose>=1)
  //       std::cout << "low spliterrors (" << sperrSa << ", " << sperrphi << "), go to next timestep" << std::endl;
  //     finish_opsplit = 0;
  //     finish_opsplit = gv.comm().max(finish_opsplit); // if any rank wants to continue, comm it to others
  //   }
  //   else
  //   {
  //     if (iter_number >= itNum)
  //     {
  //       if (verbose>=1)
  //         std::cout << "max operator splitting iteration number reached, current errors: " << sperrSa << ", " << sperrphi << ", restarting timestep" << std::endl;
  //       finish_opsplit = 100;
  //       finish_opsplit = gv.comm().max(finish_opsplit);
  //     }
  //     else
  //     {
  //       if (verbose>=1)
  //         std::cout << "going to " << iter_number+1 << ". operator splitting iteration, errors: " << sperrSa << ", " << sperrphi << std::endl;
  //       finish_opsplit = 1;
  //       finish_opsplit = gv.comm().max(finish_opsplit);
  //     }
  //   }
  //   if (finish_opsplit == 100){
  //     ++failstats["opsplitnotconverged"];
  //     DUNE_THROW(OperatorSplittingNotConverged,"Operator splitting did not converge");
  //   }
  // };
  RF defectc, defectf;
  RF defectc0, defectf0;
  int passed{0};
  auto compute_defect = [&]()
  {
    if (passed > 1)
    {
      pdesolver.updateDefect(zfdata);
      pdesolverc.updateDefect(zcdata); // this accumulates defect over ranks
      defectf = pdesolver.result().defect;
      defectc = pdesolverc.result().defect;
      if (verbose >=1)
      {
        std::cout << "Defect in flow part is " << defectf << " and reduction is " << defectf/defectf0 << std::endl;
        std::cout << "Defect in chem part is " << defectc << " and reduction is " << defectc/defectc0 << std::endl;
      }
      return std::make_tuple(defectf,defectc);
    }
    else
    {
      return std::make_tuple(RF(10.),RF(10.));
    }
  };
  auto check_op_split_convergence_defect = [&]()
  {
    if ((defectf/defectf0<osreduction || defectf<osdefectf) && (defectc/defectc0<osreduction || defectc<osdefectc) )
    {
      if (verbose >= 1)
        std::cout << "finished op-split iteration, current defects: " << defectf << ", " << defectc
                  << " reductions: " << defectf/defectf0 << ", " << defectc/defectc0 << std::endl;
      finish_opsplit =0;
      // finish_opsplit = gv.comm().max(finish_opsplit); // defect is communicated across ranks
    }
    else
    {
      if (iter_number >= itNum)
      {
        if (verbose >= 1)
          std::cout << "max operator splitting iteration number reached, current defects: " << defectf << ", " << defectc
                    << " reductions: " << defectf/defectf0 << ", " << defectc/defectc0
                    << ", restarting timestep" << std::endl;
        finish_opsplit = 100;
      }
      else
      {
        if (verbose >= 1)
          std::cout << "going to " << iter_number+1 << ". operator splitting iteration, defects: " << defectf << ", " << defectc
                    << " reductions: " << defectf/defectf0 << ", " << defectc/defectc0 << std::endl;
        finish_opsplit = 1;
      }
    }
    if (finish_opsplit == 100){
      ++failstats["opsplitnotconverged"];
      DUNE_THROW(OperatorSplittingNotConverged,"Operator splitting did not converge");
    }
  };
  while (time<T-1e-8)
  {
    try
    {
      // if (time>4.58e+4)
      if (false)
      {
        // set sleep cycle for debugger, REMOVE IF RUNNING OUTSIDE DEBUGGER
        int ifl = 0;
        char hostname[256];
        gethostname(hostname, sizeof(hostname));
        std::cout << "Approaching the critical time step! ";
        std::cout << "Using infinite sleep cycle for attaching debugger!" << std::endl;
        printf("PID %d on %s ready for attach\n", getpid(), hostname);
        fflush(stdout);
        while (0 == ifl)
              sleep(5);
      }
      maxiter = 0;
      iter_number = 0;
      finish_opsplit = 0;
      zfdata = zf;
      zcdata = zc;
      zfold = zf;
      zcold = zc;
      std::tie(defectf0,defectc0) = compute_defect();
      zfnew = zf;
      zcnew = zc;
      if (time+problemd.getTimeStep() > nextvtk)
      {
        problemd.setTimeStep(nextvtk-time);
      }
      do // iterative operator splitting
      {
        should_throw = 0;
        applyflow(problemd.getTimeStep());
        compute_defect();
        applycontaminant(problemd.getTimeStep());
        passed=10;
        store_iteration_number();
        std::tie(defectf,defectc) = compute_defect();
        check_op_split_convergence_defect();
        // zf = zfnew;
        // zc = zcnew;
        ++iter_number;
      } while (finish_opsplit != 0);
      zc=zcdata;
      zf=zfdata;
      time+=problemd.getTimeStep();
      problemd.setOtherTime(time);

      // write solution at vtktimes
      // if (time>lastvtk*vtkgap)
      if (time > nextvtk-1e-3 && time < nextvtk+1e-3)
      {
        vtkSequenceWriter.write(time,Dune::VTK::appendedraw);
        if (!vtktimes.empty())
        {
          nextvtk = vtktimes[vtktimes.size()-1];
          vtktimes.pop_back();
        }
        else
          nextvtk = T*10.;
        // lastvtk = time;
      }
      if (verbose >= 1)
      {
        timestephistoryfile.text << problemd.getTimeStep() << std::endl;
        usedOSiter.text << iter_number << std::endl;
      }
      if ( (maxiter<4) & (iter_number<itNum-1) )
      {
        problemd.adjustTimeStep(dtup);
      }
      else
        if ((maxiter>(max_newton*7.)/10.) && problemd.getTimeStep()>1e-4)
        {
          problemd.adjustTimeStep(dtdown);
        }
    } // end try
    catch(const std::string& e)
    {          // NaN or Inf occured, reduce time step
      ++failstats["opsplitIter"+std::to_string(iter_number)];
      ++failstats["unknowntype"];
      if (verbose>=1)
      {
        std::cout << e << std::endl;
      }
      if (problemd.getTimeStep()<1e-4)
      {
        if (verbose>=1)
        {
          std::cout << "too little time step" << std::endl;
        }
        vtkSequenceWriter.write(time+T,Dune::VTK::appendedraw);
        throw;
      }
      else
      {
        zf = zfold;
        zc = zcold;
        // vtkSequenceWriter.write(time+ problemd.getTimeStep(),Dune::VTK::appendedraw);
        problemd.adjustTimeStep(dtdown);
        if (verbose>=1)
        {
          std::cout << "Reducing TimeStep size to " << problemd.getTimeStep() << std::endl;
        }
      }
    }
    catch(const Dune::Exception& e)
    {
      ++failstats["opsplitIter"+std::to_string(iter_number)];
      ++failstats["solverException"];
      if (verbose>=1)
      {
        std::cout << e.what() << std::endl;
      }
      if (problemd.getTimeStep()<1e-4)
      {
        if (verbose>=1)
        {
          std::cout << "too little time step" << std::endl;
        }
        vtkSequenceWriter.write(time+T,Dune::VTK::appendedraw);
        throw;
      }
      else
      {
        // vtkSequenceWriter.write(time+ problemd.getTimeStep(),Dune::VTK::appendedraw);
        problemd.adjustTimeStep(dtdown);
        zf = zfold;
        zc = zcold;
        if (verbose>=1)
        {
          std::cout << "Reducing TimeStep size to " << problemd.getTimeStep() << std::endl;
        }
      }
    }
    catch(...) // in case of unexpected exception terminate
    {
      ++failstats["opsplitIter"+std::to_string(iter_number)];
      ++failstats["unknowntype"];
      throw;
    }
  } // end while
  if (verbose>=1)
  {
    std::cout << "Successful timesteps needed for flow ";
    for(auto i : uspesneiterf)
    {
      std::cout << i << ", ";
    }
    std::cout << "iterations." << std::endl;
    std::cout << "Successful timesteps needed for contaminant ";
    for(auto i : uspesneiterc)
    {
      std::cout << i << ", ";
    }
    std::cout << "iterations." << std::endl;
  }
  if (verbose>=1)
  {
    // failstats[solverException] contains solver exceptions, and negativeConcentration
    // and opsplitnotconverged, because all are derived from Dune::Exception
    int fsse = failstats["solverException"] - failstats["negativeConcentration"] - failstats["opsplitnotconverged"];
    std::cout << "Failures occured by" << std::endl;
    std::cout << "negative concentration: " << failstats["negativeConcentration"] << std::endl;
    std::cout << "solver exception      : " << fsse << std::endl;
    std::cout << "opslit not converging : " << failstats["opsplitnotconverged"] << std::endl;
    std::cout << "other                 : " << failstats["unknowntype"] << "." << std::endl;
    std::cout << "Failures and operator splitting iteration number: ";
    std::cout << "Os iter 0.." << std::to_string(itNum+1) << " have ";
    for (int i=0; i<itNum+1; ++i)
    {
      std::cout << failstats["opsplitIter"+std::to_string(i)] <<  ", ";
    }
    std::cout << failstats["opsplitIter"+std::to_string(itNum+1)] << std::endl;
  }
} // end driver
