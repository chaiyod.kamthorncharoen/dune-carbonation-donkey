      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                         // overwriting enables using it as a template argument

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      const DF dx = c3[0]-a0[0];
      const DF dy = c3[1]-a0[1];
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kwr, Karc; // better have vector of coefficients than re-compute pow()
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kwr[i] = param.Kwr(Sa[i],phi[i]);
        Karc[i] = param.Karc(Sa[i],pa[i],phi[i],co2[i]);
      }

      std::array<RF,lfsusize> dKwr_dSa;
      std::array<RF,lfsusize> dKwr_dphi;
      std::array<RF,lfsusize> dKarc_dpw;
      std::array<RF,lfsusize> dKarc_dSa;
      std::array<RF,lfsusize> dKarc_dco2;
      std::array<RF,lfsusize> dKarc_dphi;
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        dKwr_dSa[i]   = param.dKwr_dSa(Sa[i],phi[i]);
        dKwr_dphi[i]  = param.dKwr_dphi(Sa[i],phi[i]);
        dKarc_dpw[i]  = param.dKarc_dpw(Sa[i],pa[i],phi[i],co2[i]);
        dKarc_dSa[i]  = param.dKarc_dSa(Sa[i],pa[i],phi[i],co2[i]);
        dKarc_dco2[i] = param.dKarc_dco2(Sa[i],pa[i],phi[i],co2[i]);
        dKarc_dphi[i] = param.dKarc_dphi(Sa[i],pa[i],phi[i],co2[i]);
      }
      std::array<RF,lfsusize> co2_RTMa;
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        co2_RTMa[i] = co2[i]/p_h.getMa(co2[i])*param.getR()*param.getT();
      }

      for (int i=0; i<int(lfsusize); ++i)
      {
        // !!! this way is each edge visited twice = once from each side
        // therefore only one part, lfsv_X,i, is accumulated

        RF co2avg_y = (co2[i]+co2[pairy[i]])/2.;
        RF pa_avg_y = (pa[i]+pa[pairy[i]])/2.;
        RF rho_ay = param.getrho_a(pa_avg_y,co2avg_y);
        // RF qwx = -dy/2. *h2avg(Kwr[i],Kwr[pairx[i]])   * (pw[pairx[i]]-pw[i])/dx;
        // RF qwy = -dx/2. *h2avg(Kwr[i],Kwr[pairy[i]])   *((pw[pairy[i]]-pw[i])/dy-(i-pairy[i])/2.*param.getgrav()*param.getrho_w());
        // RF qax = -dy/2. *h2avg(Karc[i],Karc[pairx[i]]) * (pa[pairx[i]]-pa[i])/dx;
        // RF qay = -dx/2. *h2avg(Karc[i],Karc[pairy[i]]) *((pa[pairy[i]]-pa[i])/dy-(i-pairy[i])/2.*param.getgrav()*rho_ay);
        // water and air diffusion
        RF Kwrx = h2avg(Kwr[i],Kwr[pairx[i]],"Kwr in jacobian_volume");
        RF Kwry = h2avg(Kwr[i],Kwr[pairy[i]],"Kwr in jacobian_volume");
        RF nablawx = (pw[pairx[i]]-pw[i])/dx;
        RF nablawy = (pw[pairy[i]]-pw[i])/dy-(i-pairy[i])/2.*param.getgrav()*param.getrho_w();
        // r.accumulate(lfsv_pw,i, qwx +qwy);
        // derive water pressure gradient by pw (itself)
        mat.accumulate(lfsv_pw,i,lfsu_pw,      i , dy/2.*Kwrx/dx +dx/2.*Kwry/dy);
        mat.accumulate(lfsv_pw,i,lfsu_pw,pairx[i],-dy/2.*Kwrx/dx               );
        mat.accumulate(lfsv_pw,i,lfsu_pw,pairy[i],               -dx/2.*Kwry/dy);
        // derive permeability by Sa
        mat.accumulate(lfsv_pw,i,lfsu_Sa,      i ,-dy/2. *h2avg_deriv(Kwr[i],Kwr[pairx[i]],"Kwr deriv in jacobian_volume") *dKwr_dSa[      i ] *nablawx);
        mat.accumulate(lfsv_pw,i,lfsu_Sa,      i ,-dx/2. *h2avg_deriv(Kwr[i],Kwr[pairy[i]],"Kwr deriv in jacobian_volume") *dKwr_dSa[      i ] *nablawy);
        mat.accumulate(lfsv_pw,i,lfsu_Sa,pairx[i],-dy/2. *h2avg_deriv(Kwr[pairx[i]],Kwr[i],"Kwr deriv in jacobian_volume") *dKwr_dSa[pairx[i]] *nablawx);
        mat.accumulate(lfsv_pw,i,lfsu_Sa,pairy[i],-dx/2. *h2avg_deriv(Kwr[pairy[i]],Kwr[i],"Kwr deriv in jacobian_volume") *dKwr_dSa[pairy[i]] *nablawy);

        RF Karcx = h2avg(Karc[i],Karc[pairx[i]],"Karc in jacobian_volume");
        RF Karcy = h2avg(Karc[i],Karc[pairy[i]],"Karc in jacobian_volume");
        RF nablaax = (pa[pairx[i]]-pa[i])/dx;
        RF nablaay = (pa[pairy[i]]-pa[i])/dy-(i-pairy[i])/2.*param.getgrav()*rho_ay;
        // r.accumulate(lfsv_Sa,i, qax +qay);
        // derive air pressure pa by pw
        mat.accumulate(lfsv_Sa,i,lfsu_pw,      i ,  dy/2.*Karcx/dx +dx/2.*Karcy/dy );
        mat.accumulate(lfsv_Sa,i,lfsu_pw,pairx[i], -dy/2.*Karcx/dx                 );
        mat.accumulate(lfsv_Sa,i,lfsu_pw,pairy[i],                 -dx/2.*Karcy/dy );
        // derive air pressure pa by Sa
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,      i ,( dy/2.*Karcx/dx +dx/2.*Karcy/dy) *dpc[      i ] );
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairx[i],(-dy/2.*Karcx/dx                ) *dpc[pairx[i]] );
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairy[i],(                -dx/2.*Karcy/dy) *dpc[pairy[i]] );
        // derive density term by pw
        RF drho_a_dpa  = -(i-pairy[i])/2.*param.getgrav()*param.drho_a_dpa(pa_avg_y,co2avg_y);
        // RF drho_a_dco2 = -(i-pairy[i])/2.*param.getgrav()*param.drho_a_dco2(pa_avg_y,co2avg_y);
        mat.accumulate(lfsv_Sa,i,lfsu_pw,      i ,-dx/2.*Karcy *drho_a_dpa/2. );
        mat.accumulate(lfsv_Sa,i,lfsu_pw,pairy[i],-dx/2.*Karcy *drho_a_dpa/2. );
        // derive density term by Sa
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,      i ,-dx/2.*Karcy *drho_a_dpa/2. *dpc[      i ] );
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairy[i],-dx/2.*Karcy *drho_a_dpa/2. *dpc[pairy[i]] );
        // derive permeability by pw (that density term...)
        mat.accumulate(lfsv_Sa,i,lfsu_pw,      i ,-dy/2. *h2avg_deriv(Karc[i],Karc[pairx[i]],"Karc deriv in jacobian_volume") *dKarc_dpw[      i ] *nablaax);
        mat.accumulate(lfsv_Sa,i,lfsu_pw,      i ,-dx/2. *h2avg_deriv(Karc[i],Karc[pairy[i]],"Karc deriv in jacobian_volume") *dKarc_dpw[      i ] *nablaay);
        mat.accumulate(lfsv_Sa,i,lfsu_pw,pairx[i],-dy/2. *h2avg_deriv(Karc[pairx[i]],Karc[i],"Karc deriv in jacobian_volume") *dKarc_dpw[pairx[i]] *nablaax);
        mat.accumulate(lfsv_Sa,i,lfsu_pw,pairy[i],-dx/2. *h2avg_deriv(Karc[pairy[i]],Karc[i],"Karc deriv in jacobian_volume") *dKarc_dpw[pairy[i]] *nablaay);
        // derive permeability by Sa (includes density term)
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,      i ,-dy/2. *h2avg_deriv(Karc[i],Karc[pairx[i]],"Karc deriv in jacobian_volume") *dKarc_dSa[      i ] *nablaax);
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,      i ,-dx/2. *h2avg_deriv(Karc[i],Karc[pairy[i]],"Karc deriv in jacobian_volume") *dKarc_dSa[      i ] *nablaay);
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairx[i],-dy/2. *h2avg_deriv(Karc[pairx[i]],Karc[i],"Karc deriv in jacobian_volume") *dKarc_dSa[pairx[i]] *nablaax);
        mat.accumulate(lfsv_Sa,i,lfsu_Sa,pairy[i],-dx/2. *h2avg_deriv(Karc[pairy[i]],Karc[i],"Karc deriv in jacobian_volume") *dKarc_dSa[pairy[i]] *nablaay);
      }