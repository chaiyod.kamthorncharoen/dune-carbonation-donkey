      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                     // overwriting enables using it as a template argument

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      DF dx = c3[0]-a0[0];
      DF dy = c3[1]-a0[1];
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kwr, Karc; // better have vector of coefficients than re-compute pow()
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kwr[i] = param.Kwr(Sa[i],phi[i]);
        Karc[i] = param.Karc(Sa[i],pa[i],phi[i],co2[i]);
      }

      for (int i=0; i<int(lfsusize); ++i)
      {
        // water and air diffusion
        RF rho_ay = p_h.getrho_a((pa[i]+pa[pairy[i]])/2.,(co2[i]+co2[pairy[i]])/2.);
        // RF rho_ay = (p_h.getrho_a(pa[i],co2[i])+p_h.getrho_a(pa[pairy[i]],co2[pairy[i]]))/2.; // TODO: check difference of inner and outer arit average
        RF qwx = -dy/2.*h2avg(Kwr[i],Kwr[pairx[i]],"Kwr in alpha_volume")  * (pw[pairx[i]]-pw[i])/dx;
        RF qwy = -dx/2.*h2avg(Kwr[i],Kwr[pairy[i]],"Kwr in alpha_volume")  *((pw[pairy[i]]-pw[i])/dy-(i-pairy[i])/2.*param.getgrav()*param.getrho_w());
        RF qax = -dy/2.*h2avg(Karc[i],Karc[pairx[i]],"Karc in alpha_volume")* (pa[pairx[i]]-pa[i])/dx;
        RF qay = -dx/2.*h2avg(Karc[i],Karc[pairy[i]],"Karc in alpha_volume")*((pa[pairy[i]]-pa[i])/dy-(i-pairy[i])/2.*param.getgrav()*rho_ay);
        // TODO? some models have also mixed, coupling terms for twophase flow
        // I access all vertices, so fluxes in the opposite way are accumulated there
        r.accumulate(lfsv_pw,i, qwx + qwy);
        r.accumulate(lfsv_Sa,i, qax + qay);
      }