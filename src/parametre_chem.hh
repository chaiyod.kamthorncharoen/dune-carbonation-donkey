template<typename Number, typename Param, int N=6>
class Parametre_chem
{
public:
  Param& param;          // flow parameter class
  std::array<int,4> branchinfo{0,0,0,0};
  int substep{0};
  int totalsubsteps{3};
private:
  Number alfa_l, alfa_t;       // dispersion transversal and longitudinal
  Number ini_Ca, ini_co2;      // initial Ca concentration and CO_2 partial pressure
  Number difCa, difCae, difco2;// diffusion coefficient of Ca in water and CO_2 in air, both are dynamic though
  Number airco2;               // fraction of CO_2 in air
  Number carba, carbb;         // speed of carbonation parameters, weigths for saturations
  Number carbtheta;            // speed of carbonation coefficient, Inf is instant reaction
  Number V_CaCO_3 = 36.93e-6;  // volume of Calcite per mol, [m³/mol]
  Number Mco2 = 44.01e-3;      // CO_2 molar mass [kg/mol]
  Number Mh2o = 18.01528e-3;   // H_2O molar mass [kg/mol]
  Number MCaO = 56.0774e-3;    // CaO  molar mass [kg/mol]
  Number visc_c = 1.48e-5;     // CO_2 viscosity  [kg/(m s)]
  Number L = 0.0;               // L-scheme coefficient
  const std::array<Number,N+1> interval   {-1., 0.4,    1.6,     9.3,   14.8,   21.,   22.}; // zones of chem reactions
  const std::array<Number,N>  incrementVdt   {0.,  63e-6,   10e-6,   2e-6,  30e-6, 145e-6}; // leached CaStored volume
  const std::array<Number,N>  incrementCaConc{0.,   3e-3, 0.01e-3, 0.4e-3, 0.5e-3, 5.2e-3}; // CaO molar concentration gained by leaching
  const std::array<Number,N>  incrementwls   {0., 7.4e-3, 0.01e-3, 1.7e-3, 0.2e-3, 4.4e-3}; // H2O molar concentration gained by leaching
  Number Vdt_coef, CaC_coef, wls_coef;
  Number margin{0.0}; // smoothing Leaching
  Number overlap{0.0};
public:
  typedef Number value_type;
  typedef Param Param_wflow;

  //! Constructor filling material parameters
  Parametre_chem (        Param & param_
                  , const Number& alfa_l_
                  , const Number& ini_Ca_
                  , const Number& ini_co2_
                  , const Number& difCa_
                  , const Number& difCae_
                  , const Number& difco2_
                  , const Number& airco2_
                  , const Number& carba_
                  , const Number& carbb_
                  , const Number& carbtheta_
                  , const Number& VCaCO_3_
                  , const Number& visc_c_)
                :   param(param_)
                  , alfa_l(alfa_l_), alfa_t(alfa_l_/8.)
                  , ini_Ca(ini_Ca_)
                  , ini_co2(ini_co2_)
                  , difCa(difCa_)
                  , difCae(difCae_)
                  , difco2(difco2_)
                  , airco2(airco2_)
                  , carba(carba_)
                  , carbb(carbb_)
                  , carbtheta(carbtheta_)
                  , V_CaCO_3(VCaCO_3_)
                  , visc_c(visc_c_)
  {
    Vdt_coef = 4e2; //1e12;// 4.e2;
    CaC_coef = 1.5e5; //1e6; // 1.5e5;
    wls_coef = 1e4; //1e6; // 1.e4;
    param.setvisc_c(visc_c);
    L = param.l_co2;
  }

  inline void branchinfozero()
  {
    for (auto& i:branchinfo)
      i=0;
  }

  inline Number getalfat() const
  {
    return alfa_t;
  }

  inline Number getalfal() const
  {
    return alfa_l;
  }

  inline Number getmolco2() const
  {
    return Mco2;
  }

  inline Number getMco2() const
  {
    return Mco2;
  }

  inline Number getMh2o() const
  {
    return Mh2o;
  }

  inline Number getMCaO() const
  {
    return MCaO;
  }

  inline Number getL() const
  {
    return L;
  }

  Number getMa(const Number& co2_) const
  {
    return (param.getMa()-0.0004*Mco2)/(1.-0.0004)*(1.-co2_)+co2_*Mco2;
  }

  Number getMa_deriv(const Number& co2_) const
  {
    return (param.getMa()-0.0004*Mco2)/(1.-0.0004)*(-1.)+Mco2;
    // return (param.getMa()-0.0004*Mco2())/(1.-0.0004)*(1.-co2_)+co2_*Mco2;
  }

  inline Number getV_CaCO_3() const
  {
    return V_CaCO_3;
  }

  inline Number getrhoco2(const Number& pco2_) const
  {
     return Mco2*pco2_/(param.getR()*param.getT());
  }

  inline Number getrho_a(const Number& pa_, const Number& co2_) const
  {
    return (param.getpa0()+pa_)/(param.getR()*param.getT())*( (param.getMa()-0.0004*Mco2)/(1.-0.0004)*(1.-co2_)+Mco2*co2_ );
    // (molar mass of air without CO_2) = (air_molar_mass - CO2_molar_mass*CO2_typical_part)/(1-CO_2_typical_part) -> removed CO2 part and divided to get 1 mol again
    // (molar mass of mixture) = (molar mass of air without CO_2)*(1-co2_) + (molar mass of CO_2)*co2_    -> weighted average, concentration gives weight
    // rho_a = (molar mass of mixture) * pressure * constant
  }

  Number visc_mix(const Number& co2_) const // viscosity of air mixture
  {
    Number Ma = param.getMa();
    Number visc_a = param.getvisc_a();
    Number fi_ac = (1.+sqrt(visc_a/visc_c)*sqrt(sqrt(Mco2/Ma)));
    fi_ac *= fi_ac;
    fi_ac /= (4./sqrt(2.)*sqrt(1.+Ma/Mco2));
    Number fi_ca = (1.+sqrt(visc_c/visc_a)*sqrt(sqrt(Ma/Mco2)));
    fi_ca *= fi_ca;
    fi_ca /= (4./sqrt(2.)*sqrt(1.+Mco2/Ma));
    Number visc_mix = (1.-co2_)*visc_a/((1.-co2_)+  co2_   *fi_ac)
                     +    co2_ *visc_c/(  co2_   +(1.-co2_)*fi_ca);
    // Number visc_mix = (1.-co2_+0.0004)*visc_a/((1.-co2_)+co2_*fi_ac) // since normal air contains 0.0004 CO2, it is weirdly shifted
    //                  +(   co2_-0.0004)*visc_c/(co2_+(1.-co2_)*fi_ca);// but it has no visible effect on resulting value
    return visc_mix;
  }

  Number dvisc_mix_dco2(const Number& co2_) const
  {
    Number Ma = param.getMa();
    Number visc_a = param.getvisc_a();
    Number fi_ac = (1.+sqrt(visc_a/visc_c)*sqrt(sqrt(Mco2/Ma)));
    fi_ac *= fi_ac;
    fi_ac /= (4./sqrt(2.)*sqrt(1.+Ma/Mco2));
    Number fi_ca = (1.+sqrt(visc_c/visc_a)*sqrt(sqrt(Ma/Mco2)));
    fi_ca *= fi_ca;
    fi_ca /= (4./sqrt(2.)*sqrt(1.+Mco2/Ma));
    Number dvisc_mixdco2 = (-1.)*visc_a/((1.-co2_)+  co2_   *fi_ac)+(1.-co2_)*visc_a*(-1.)/(((1.-co2_)+  co2_   *fi_ac)*((1.-co2_)+  co2_   *fi_ac))*(-1.+fi_ac)
                            +    visc_c/(  co2_   +(1.-co2_)*fi_ca)+    co2_ *visc_c*(-1.)/((  co2_   +(1.-co2_)*fi_ca)*(  co2_   +(1.-co2_)*fi_ca))*( 1.-fi_ca);
    return dvisc_mixdco2;
  }

  inline Number getairco2() const
  {
    return airco2;
  }

  inline Number getdifCa(const Number& porosity_, const Number& Sw_) const
  {
    Number denom = 5.*(1.-Sw_); // adding to 1, division by 0 not risked
    denom *= denom;
    return difCa*exp(difCae*porosity_)/(1.+denom*denom);
    // denom = 2.3e-13*exp(9.95*porosity_)/(1.+denom*denom);
    // std::cout << "CO_2 diffusion coefficient is " << denom << std::endl;
    // return denom;
  }

  inline Number ddifCa_dSw(const Number& porosity_, const Number& Sw_) const
  {
    Number denom = 5.*(1.-Sw_);
    Number outerder = denom*denom;
    outerder = 1.+outerder*outerder;
    outerder *= outerder;
    return difCa*exp(difCae*porosity_)*(-1.)/outerder*4.*denom*denom*denom*(-5.);
  }

  inline Number ddifCa_dphi(const Number& porosity_, const Number& Sw_) const
  {
    return getdifCa(porosity_,Sw_)*difCae; // deriving exp function just adds factor
    // Number denom = 5.*(1.-Sw_);
    // denom *=denom;
    // return difCa*exp(difCae*porosity_)*difCae/(1.+denom*denom);
  }

  inline Number getdifco2(const Number& porosity_, const Number& Sa_) const
  {
    // return difco2*pow(porosity_,4./3.)*pow(Sa_,10./3.);
    return difco2*porosity_*Sa_*Sa_*Sa_*cbrt(porosity_*Sa_); // same as one above, using one cbrt() instead two pow()
    // uses (Sa*\phi)^(10/3)/(\phi^2) as an effective diffusion coefficient
    // one author suggests using (Sa*\phi)/(\phi^(2/3)) as eff diff coef, because of better agreement with experiment results
    // btw Sa*\phi is in literature often reffered as \theta_a = volumetric air content   (=saturation_a*porosity)
  }

  inline Number ddifco2_dSa(const Number& porosity_, const Number& Sa_) const
  {
    // return difco2*pow(porosity_,4./3.)*pow(Sa_,7./3.)*(10./3.);
    return difco2*porosity_*Sa_*Sa_*cbrt(porosity_*Sa_)*(10./3.); // same as above, using one cbrt() instead of two pow()
  }

  inline Number ddifco2_dphi(const Number& porosity_, const Number& Sa_) const
  {
    // return difco2*pow(porosity_,1./3.)*pow(Sa_,10./3.)*(4./3.);
    return difco2*Sa_*Sa_*Sa_*cbrt(porosity_*Sa_)*(4./3.); // same as above, using one cbrt() instead of two pow()
  }

  inline Number getcarbspeed(const Number& Sw) const
  {
    Number kgl = (carba+carbb)/carba;
    kgl = carba/carbb*pow(kgl,kgl);
      // \vartheta (a/b) ((a+b)/a)^((a+b)/a) S_w^a (1-S_w^b)
    return carbtheta*kgl*pow(Sw,carba)*(1.-pow(Sw,carbb));
  }

  inline Number getcarbspeed_deriv(const Number& Sw) const
  {
    Number kgl = (carba+carbb)/carba;
    kgl = carba/carbb*pow(kgl,kgl);
      // \vartheta (a/b) ((a+b)/a)^((a+b)/a) S_w^a (1-S_w^b)
    return carbtheta*kgl*( carba*pow(Sw,carba-1.)*(1.-pow(Sw,carbb))
                          +carbb*pow(Sw,carba)*(-pow(Sw,carbb-1.))  );
  }

  inline Number getleachspeed(const Number& Sw_, const Number& Ca_) const
  {
    // currently only describing k_{sl}
    Number denom = (5.*(1.-Sw_));
    denom *= denom;
    return 1./(1.+denom*denom);
  }

  Number dleachspeed_dSw(const Number& Sw_, const Number& Ca_) const
  {
    Number denom = 5.*(1.-Sw_);
    Number outerder = denom*denom;
    outerder = 1.+outerder*outerder;
    outerder *= outerder;
    return (-1.)/outerder*4.*denom*denom*denom*(-5.);
  }

  inline Number dleachspeed_dCa(const Number& Sw_, const Number& Ca_) const
  {
    return 0.;
  }

  Number leached(const Number& Ca_, const std::array<Number,N>& increment) const
  {
    if (Ca_<=interval[0])
    {
      return 0.;
    }
    Number accum=0.;
    for (std::size_t i=0; i<N; ++i)
    {
      if (Ca_<interval[i+1])
      {
        accum += increment[i]*(Ca_-interval[i]);
        return accum;
      }
      accum += increment[i]*(interval[i+1]-interval[i]);
    }
    return accum;
  }

  Number leached_deriv(const Number& Ca_, const std::array<Number,N>& increment) const
  {
    if (Ca_<= interval[0])
    {
      return 0.;
    }
    for (std::size_t i=0; i<N; ++i)
    {
      if (Ca_<interval[i+1])
      {
        return increment[i];
      }
    }
    return increment[N-1];
  }

  Number leachedSmoothed(const Number& Ca_, const std::array<Number,N>& increment) const
  {
    if (Ca_<interval[0])
      // if outside interval zone, simply extend value[0]
      return 0.;
    for (std::size_t i=1; i<interval.size()-1; ++i)
    {
      // check if it is around node[i], where it should be interpolated
      auto bottom = interval[i]-margin*(interval[i]-interval[i-1]);
      auto top = interval[i]+margin*(interval[i+1]-interval[i]);
      bool notunder = Ca_ > bottom;
      bool notover = Ca_ < top;
      if (notunder && notover)
      {
        auto interv = std::make_tuple(bottom,top);
        auto vals = std::make_tuple(leached((interval[i-1]+interval[i])/2.,increment),leached((interval[i]+interval[i+1])/2.,increment));
        auto derivs = std::make_tuple(leached_deriv((interval[i-1]+interval[i])/2.,increment),leached_deriv((interval[i]+interval[i+1])/2.,increment));
        auto csm = mojefunkcie::CubicSmoother(interv,vals,derivs);
        return csm.value(Ca_);
      }
      else
      {
        // give flat value otside interpolate zone
        if (Ca_ <= bottom)
          return leached(Ca_,increment);
      }
    }
    // Ca_ is between the last two interval or higher
    // Ca_ >= top( interval[interval.size()-2] )
    return leached(Ca_,increment);
  }

  Number leachedSmoothed_deriv(const Number& Ca_, const std::array<Number,N>& increment) const
  {
    if (Ca_<interval[0])
      // if outside interval zone, simply extend value[0]
      return 0.;
    for (std::size_t i=1; i<interval.size()-1; ++i)
    {
      // check if it is around node[i], where it should be interpolated
      auto bottom = interval[i]-margin*(interval[i]-interval[i-1]);
      auto top = interval[i]+margin*(interval[i+1]-interval[i]);
      bool notunder = Ca_ > bottom;
      bool notover = Ca_ < top;
      if (notunder && notover)
      {
        auto interv = std::make_tuple(bottom,top);
        auto vals = std::make_tuple(leached((interval[i-1]+interval[i])/2.,increment),leached((interval[i]+interval[i+1])/2.,increment));
        auto derivs = std::make_tuple(leached_deriv((interval[i-1]+interval[i])/2.,increment),leached_deriv((interval[i]+interval[i+1])/2.,increment));
        auto csm = mojefunkcie::CubicSmoother(interv,vals,derivs);
        return csm.derivation(Ca_);
      }
      else
      {
        // give flat value otside interpolate zone
        if (Ca_ <= bottom)
          return leached_deriv(Ca_,increment);
      }
    }
    // Ca_ is between the last two interval or higher
    // Ca_ >= top( interval[interval.size()-2] )
    return leached_deriv(Ca_,increment);
  }

  void showSmoothing(const std::string& variant)
  {
    std::array<Number,N> increment;
    if (variant == std::string("Vdt"))
      increment = incrementVdt;
    else if (variant == std::string("CaC"))
      increment = incrementCaConc;
    else if (variant == std::string("wls"))
      increment = incrementwls;
    else
    {
      std::cout << "No variant chosen!" << std::endl;
      return;
    }
    std::vector<Number> values;
    Number gap = 0.5;
    values.push_back(0.);
    while (values[values.size()-1.]<22.)
    {
      values.push_back(values[values.size()-1.]+gap);
    }
    for (const auto& v: values)
    {
      std::cout << "leached: " << leached(v,increment) << "  smoothed: " << integratedSmoother(v,increment) << std::endl;
    }
  }

  Number integratedSmoother(const Number& Ca_, const std::array<Number,N>& increment) const
  {
    // smoothes continuous, piecewise linear function (+1 derivation)
    // value is an integral around the selected point extending "overlap" to both sides (1D)
    // exploits piecewise linear design and directly calculates value
    if (Ca_<interval[0])
      // if outside interval zone, simply extend value[0]
      return leached(Ca_,increment);
    for (std::size_t i=1; i<interval.size()-1; ++i)
    {
      // check if it is around node[i], where it should be interpolated
      if (std::abs(Ca_-interval[i])<overlap)
      {
        Number b = Ca_ - overlap;
        Number t = Ca_ + overlap;
        Number m = interval[i];
        Number fb = leached(b,increment);
        Number ft = leached(t,increment);
        Number fm = leached(m,increment);
        return (m-b)/4./overlap * fb + (t-m)/4./overlap * ft + fm/2.;
      }
      else
      {
        // give flat value otside interpolate zone
        if (Ca_ <= interval[i]) // interval[i]-overlap
          return leached(Ca_,increment);
      }
    }
    // Ca_ is between the last two interval or higher
    // Ca_ >= top( interval[interval.size()-2] )
    return leached(Ca_,increment);
  }
  Number integratedSmoother_deriv(const Number& Ca_, const std::array<Number,N>& increment) const
  {
    if (Ca_<interval[0])
      // if outside interval zone, simply extend value[0]
      return leached_deriv(Ca_,increment);
    for (std::size_t i=1; i<interval.size()-1; ++i)
    {
      // check if it is around node[i], where it should be interpolated
      if (std::abs(Ca_-interval[i])<overlap)
      {
        Number b = Ca_ - overlap;
        Number t = Ca_ + overlap;
        Number m = interval[i];
        Number fb = leached(b,increment);
        Number ft = leached(t,increment);
        Number fbd = leached_deriv(b,increment);
        Number ftd = leached_deriv(t,increment);
        Number o4 = 4.*overlap;
        return (m-b)/o4*fbd - fb/o4 + (t-m)/o4*ftd + ft/o4;
      }
      else
      {
        // give flat value otside interpolate zone
        if (Ca_ <= interval[i])
          return leached_deriv(Ca_,increment);
      }
    }
    // Ca_ is between the last two interval or higher
    // Ca_ >= top( interval[interval.size()-2] )
    return leached_deriv(Ca_,increment);
  }

  inline Number getVdt(const Number& Ca_) const
  {
    // return leached(Ca_,incrementVdt)*Vdt_coef;
    return integratedSmoother(Ca_,incrementVdt)*Vdt_coef;
  }

  inline Number dVdt_dCa(const Number& Ca_) const
  {
    // return leached_deriv(Ca_,incrementVdt)*Vdt_coef;
    return integratedSmoother_deriv(Ca_,incrementVdt)*Vdt_coef;
  }

  inline Number getCaC(const Number& Ca_) const
  {
    // return leached(Ca_,incrementCaConc)*CaC_coef;
    return integratedSmoother(Ca_,incrementCaConc)*CaC_coef;
  }

  inline Number dCaC_dCa(const Number& Ca_) const
  {
    // return leached_deriv(Ca_, incrementCaConc)*CaC_coef;
    return integratedSmoother_deriv(Ca_, incrementCaConc)*CaC_coef;
  }

  inline Number getwls(const Number& Ca_) const
  {
    // return leached(Ca_,incrementwls)*wls_coef;
    return integratedSmoother(Ca_,incrementwls)*wls_coef;
  }

  inline Number dwls_dCa(const Number& Ca_) const
  {
    // return leached_deriv(Ca_,incrementwls)*wls_coef;
    return integratedSmoother_deriv(Ca_,incrementwls)*wls_coef;
  }

  //! boundary condition type function (true = Dirichlet)
  template<typename I, typename X>
  inline bool bCa (const I& i, const X& x) const
  {
    return false;
    // return param.b(i,x); // condition for water boundary
    // auto global = i.geometry().global(x);
    // return (global[0]>=param.getwidth()-1e-7) ? true : false;
    // return (global[0]<=1e-7 || global[0]>=param.getwidth()-1e-7) ? true : false;
  }

  //! boundary condition type function (true = Dirichlet)
  template<typename I, typename X>
  inline bool bco2 (const I& i, const X& x) const
  {
    auto global = i.geometry().global(x);
    return (global[0]>= param.getwidth()-1e-7) ? true : false;
  }

  template<typename E, typename X>
  inline Number g_Ca (const E& e, const X& x) const
  {
    // return ini_Ca;
    Number t = 0.95; // width of transition region, relative to total width
    auto global = e.geometry().global(x);
    if (global[0]<param.getwidth()*t)
      return ini_Ca;
    else
    {
      // return mojefunkcie::smooth_cirq(std::make_tuple(param.getwidth()*t,param.getwidth()),
      //                                 std::make_tuple(ini_Ca,0.),
      //                                 global[0]);
      return ini_Ca+(0.-ini_Ca)*(global[0]/param.getwidth()-t)*(global[0]/param.getwidth()-t)/((1.-t)*(1.-t));
    }

    // right boundary smoothed and shifted inside
    // auto global = e.geometry().global(x);
    // if (global[0]>param.getwidth()*0.8)
    //   return (ini_Ca-0.1)*std::sqrt(std::max(0.,1.-(10.*(global[0]-0.8*param.getwidth())/param.getwidth())
    //                                         *(10.*(global[0]-0.8*param.getwidth())/param.getwidth()) ));
    // else return ini_Ca-0.1*global[0]/(param.getwidth()*0.8);

    // left boundary smoothed
    // if (global[0]<param.getwidth()*0.1)
    //   return ini_Ca*std::sqrt(1.-(1.-10.*global[0]/param.getwidth())*(1.-10.*global[0]/param.getwidth()));
    // else return ini_Ca;
    // return std::min(global[0],param.getwidth()*0.1)*10./param.getwidth()*ini_Ca;
    // return (global[0]>=param.getwidth()-1e-7) ? 0. : ini_Ca;
    // return ini_Ca+(0.-ini_Ca)*std::max(0.,global[0]-param.getwidth()*0.9)/(param.getwidth()*0.1);
  }

  template<typename E, typename X>
  inline Number g_co2 (const E& e, const X& x) const
  {
    auto global = e.geometry().global(x);
    return (global[0]>= param.getwidth()-1e-7) ? getairco2() : ini_co2;
    // return ini_co2+(getairco2()-ini_co2)*std::max(0.,global[0]-param.getwidth()*0.9)/(param.getwidth()*0.1);

    // if (global[0]<=param.getwidth()*0.9)
    //   return ini_co2;
    // else 
    //   return mojefunkcie::smooth_cirq(std::make_tuple(param.getwidth()*0.9,param.getwidth()),
    //                                   std::make_tuple(ini_co2,getairco2()),
    //                                   global[0]);
  }

  template<typename E, typename X>
  inline Number g_porosity (const E& e, const X& x) const
  {
    return param.getporosity(e,x);
    // return param.getporosity(e,x)+getVdt(ini_Ca-g_Ca(e,x));
  }

  template<typename E, typename X>
  inline Number g_CaStored (const E& e, const X& x) const
  {
    return param.getstoredCa();
    // return param.getstoredCa()-getVdt(ini_Ca-g_Ca(e,x));
  }
};

class OperatorSplittingNotConverged : public Dune::Exception
{};

class NegativeConcentrationError : public Dune::Exception
{};