      constexpr std::size_t lfsusize = 4; // this file is inside if(lfsusize==4)
                                         // overwriting enables using it as a template argument

      const auto& a0 = eg.geometry().corner(0);
      const auto& c3 = eg.geometry().corner(3);
      const DF dx = c3[0]-a0[0];
      const DF dy = c3[1]-a0[1];
      constexpr std::array<int,lfsusize> pairx{1,0,3,2};
      constexpr std::array<int,lfsusize> pairy{2,3,0,1};

      std::array<RF,lfsusize> Kwr, Karc; // better have vector of coefficients than re-compute pow()
      for (size_t i=0; i<lfsusize; ++i)
      {
        Kwr[i] = param.Kwr(Sa[i],phi[i]);
        Karc[i] = param.Karc(Sa[i],pa[i],phi[i],co2[i]);
      }

      // advection part
      if (osType%2==1)
      {
        std::array<RF,lfsusize> dKwr_dphi;
        std::array<RF,lfsusize> dKarc_dco2;
        std::array<RF,lfsusize> dKarc_dphi;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          dKwr_dphi[i]  = param.dKwr_dphi(Sa[i],phi[i]);
          dKarc_dco2[i] = param.dKarc_dco2(Sa[i],pa[i],phi[i],co2[i]);
          dKarc_dphi[i] = param.dKarc_dphi(Sa[i],pa[i],phi[i],co2[i]);
        }
        std::array<RF,lfsusize> co2_RTMa;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          co2_RTMa[i] = co2[i]/p_h.getMa(co2[i])*param.getR()*param.getT();
        }

        for (int i=0; i<int(lfsusize); ++i)
        {
          // !!! this way is each edge visited twice = once from each side
          // therefore only one part, lfsv_X,i, is accumulated

          RF co2avg_y = (co2[i]+co2[pairy[i]])/2.;
          RF pa_avg_y = (pa[i]+pa[pairy[i]])/2.;
          RF rho_ay = param.getrho_a(pa_avg_y,co2avg_y);
          RF qwx = -dy/2. *h2avg(Kwr[i],Kwr[pairx[i]],"Kwr in jacobian_volume")   * (pw[pairx[i]]-pw[i])/dx;
          RF qwy = -dx/2. *h2avg(Kwr[i],Kwr[pairy[i]],"Kwr in jacobian_volume")   *((pw[pairy[i]]-pw[i])/dy-(i-pairy[i])/2.*param.getgrav()*param.getrho_w());
          RF qax = -dy/2. *h2avg(Karc[i],Karc[pairx[i]],"Karc in jacobian_volume") * (pa[pairx[i]]-pa[i])/dx;
          RF qay = -dx/2. *h2avg(Karc[i],Karc[pairy[i]],"Karc in jacobian_volume") *((pa[pairy[i]]-pa[i])/dy-(i-pairy[i])/2.*param.getgrav()*rho_ay);
          // water and air diffusion
          // RF Kwrx = h2avg(Kwr[i],Kwr[pairx[i]]);
          // RF Kwry = h2avg(Kwr[i],Kwr[pairy[i]]);
          RF nablawx = (pw[pairx[i]]-pw[i])/dx;
          RF nablawy = (pw[pairy[i]]-pw[i])/dy-(i-pairy[i])/2.*param.getgrav()*param.getrho_w();

          // RF Karcx = h2avg(Karc[i],Karc[pairx[i]]);
          RF Karcy = h2avg(Karc[i],Karc[pairy[i]],"Karc in jacobian_volume");
          RF nablaax = (pa[pairx[i]]-pa[i])/dx;
          RF nablaay = (pa[pairy[i]]-pa[i])/dy-(i-pairy[i])/2.*param.getgrav()*rho_ay;
          // derive density term by pw
          // RF drho_a_dpa  = -(i-pairy[i])/2.*param.getgrav()*param.drho_a_dpa(pa_avg_y,co2avg_y);
          RF drho_a_dco2 = -(i-pairy[i])/2.*param.getgrav()*param.drho_a_dco2(pa_avg_y,co2avg_y);
          // Ca and CO_2 convection
          // qwy /= param.getrho_w();
          // qwx /= param.getrho_w();
          // qay *= param.getR()*param.getT();
          // qax *= param.getR()*param.getT();

          // either I access the same edge twice and divide it by 2,
          // or accumulate only one edge like in water-air part
          // TODO !!! finish rectangular part
          std::size_t branchCax = qwx>0 ? i : pairx[i];
          std::size_t branchCay = qwy>0 ? i : pairy[i];
          // derive Ca convection by Ca
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,branchCax, qwx/param.getrho_w());
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,branchCay, qwy/param.getrho_w());
          // derive Ca convection by phi -flow permeability
          mat.accumulate(lfsv_Ca,i,lfsu_phi,      i ,-dy/2.*Ca[branchCax] *h2avg_deriv(Kwr[i],Kwr[pairx[i]],"Kwr derived") *dKwr_dphi[      i ]/param.getrho_w() *nablawx);
          mat.accumulate(lfsv_Ca,i,lfsu_phi,      i ,-dx/2.*Ca[branchCay] *h2avg_deriv(Kwr[i],Kwr[pairy[i]],"Kwr derived") *dKwr_dphi[      i ]/param.getrho_w() *nablawy);
          mat.accumulate(lfsv_Ca,i,lfsu_phi,pairx[i],-dy/2.*Ca[branchCax] *h2avg_deriv(Kwr[pairx[i]],Kwr[i],"Kwr derived") *dKwr_dphi[pairx[i]]/param.getrho_w() *nablawx);
          mat.accumulate(lfsv_Ca,i,lfsu_phi,pairy[i],-dx/2.*Ca[branchCay] *h2avg_deriv(Kwr[pairy[i]],Kwr[i],"Kwr derived") *dKwr_dphi[pairy[i]]/param.getrho_w() *nablawy);

          std::size_t branchco2x = qax>0 ? i : pairx[i];
          std::size_t branchco2y = qay>0 ? i : pairy[i];
          // derive co2 convection by co2
          mat.accumulate(lfsv_co2,i,lfsu_co2,branchco2x, qax*param.getR()*param.getT()*(1./p_h.getMa(co2[branchco2x])+(p_h.getMa(co2[branchco2x])*p_h.getMa(co2[branchco2x]))*(-1.)*p_h.getMa_deriv(co2[branchco2x])*co2[branchco2x]) );
          mat.accumulate(lfsv_co2,i,lfsu_co2,branchco2y, qay*param.getR()*param.getT()*(1./p_h.getMa(co2[branchco2y])+(p_h.getMa(co2[branchco2y])*p_h.getMa(co2[branchco2y]))*(-1.)*p_h.getMa_deriv(co2[branchco2y])*co2[branchco2y]) );
          // derive co2 convection by co2 (-> flow -> density inside gravity term)
          mat.accumulate(lfsv_co2,i,lfsu_co2,      i ,-dx/2.*co2_RTMa[branchco2y]*Karcy*drho_a_dco2/2. );
          mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i],-dx/2.*co2_RTMa[branchco2y]*Karcy*drho_a_dco2/2. );
          // derive co2 convection by co2 -permeability (density term only)
          mat.accumulate(lfsv_co2,i,lfsu_co2,      i ,-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[i],Karc[pairx[i]],"Karc derived") *dKarc_dco2[      i ] *nablaax);
          mat.accumulate(lfsv_co2,i,lfsu_co2,      i ,-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[i],Karc[pairy[i]],"Karc derived") *dKarc_dco2[      i ] *nablaay);
          mat.accumulate(lfsv_co2,i,lfsu_co2,pairx[i],-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[pairx[i]],Karc[i],"Karc derived") *dKarc_dco2[pairx[i]] *nablaax);
          mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i],-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[pairy[i]],Karc[i],"Karc derived") *dKarc_dco2[pairy[i]] *nablaay);
          // derive co2 convection by phi -permeability
          mat.accumulate(lfsv_co2,i,lfsu_phi,      i ,-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[i],Karc[pairx[i]],"Karc derived") *dKarc_dphi[      i ] *nablaax);
          mat.accumulate(lfsv_co2,i,lfsu_phi,      i ,-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[i],Karc[pairy[i]],"Karc derived") *dKarc_dphi[      i ] *nablaay);
          mat.accumulate(lfsv_co2,i,lfsu_phi,pairx[i],-dy/2.*co2_RTMa[branchco2x] *h2avg_deriv(Karc[pairx[i]],Karc[i],"Karc derived") *dKarc_dphi[pairx[i]] *nablaax);
          mat.accumulate(lfsv_co2,i,lfsu_phi,pairy[i],-dx/2.*co2_RTMa[branchco2y] *h2avg_deriv(Karc[pairy[i]],Karc[i],"Karc derived") *dKarc_dphi[pairy[i]] *nablaay);
        }
      }
      // Ca and CO_2 diffusion term -------------------------------------------------------------------------------------------------------------
      if((osType%4)/2==1)
      {
        std::array<RF,lfsusize> Dw, Da, pa_Da_Ma, Ma;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          Dw[i] = p_h.getdifCa(phi[i],1.-Sa[i]);
          pa[i] = pa[i]+param.getpa0();
          Da[i] = p_h.getdifco2(phi[i],Sa[i]);
          Ma[i] = p_h.getMa(co2[i])/p_h.getMco2();
          pa_Da_Ma[i] = pa[i]*Da[i]*Ma[i];
        }

        for (std::size_t i=0; i<lfsusize; ++i)
        {
          // derive Ca gradient by Ca
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,      i,  dy/2./dx *h2avg(Dw[i],Dw[pairx[i]],"Dw derived") );
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,      i,  dx/2./dy *h2avg(Dw[i],Dw[pairy[i]],"Dw derived") );
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,pairx[i],-dy/2./dx *h2avg(Dw[i],Dw[pairx[i]],"Dw derived") );
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,pairy[i],-dx/2./dy *h2avg(Dw[i],Dw[pairy[i]],"Dw derived") );
          // derive Dw by phi
          mat.accumulate(lfsv_Ca,i,lfsu_phi,      i,( dy/2.*(Ca[i]-Ca[pairx[i]])/dx *h2avg_deriv(Dw[i],Dw[pairx[i]],"Dw derived")
                                                     +dx/2.*(Ca[i]-Ca[pairy[i]])/dy *h2avg_deriv(Dw[i],Dw[pairy[i]],"Dw derived"))*p_h.ddifCa_dphi(phi[      i ],1.-Sa[      i ]) );
          mat.accumulate(lfsv_Ca,i,lfsu_phi,pairx[i], dy/2.*(Ca[i]-Ca[pairx[i]])/dx *h2avg_deriv(Dw[pairx[i]],Dw[i],"Dw derived") *p_h.ddifCa_dphi(phi[pairx[i]],1.-Sa[pairx[i]]) );
          mat.accumulate(lfsv_Ca,i,lfsu_phi,pairy[i], dx/2.*(Ca[i]-Ca[pairy[i]])/dy *h2avg_deriv(Dw[pairy[i]],Dw[i],"Dw derived") *p_h.ddifCa_dphi(phi[pairy[i]],1.-Sa[pairy[i]]) );

          // derive co2 gradient by co2
          mat.accumulate(lfsv_co2,i,lfsv_co2,      i,  dy/2./dx *h2avg(pa_Da_Ma[i] ,pa_Da_Ma[pairx[i]],"pa_Da_Ma derived") );
          mat.accumulate(lfsv_co2,i,lfsv_co2,      i,  dx/2./dy *h2avg(pa_Da_Ma[i] ,pa_Da_Ma[pairy[i]],"pa_Da_Ma derived") );
          mat.accumulate(lfsv_co2,i,lfsv_co2,pairx[i],-dy/2./dx *h2avg(pa_Da_Ma[i] ,pa_Da_Ma[pairx[i]],"pa_Da_Ma derived") );
          mat.accumulate(lfsv_co2,i,lfsv_co2,pairy[i],-dx/2./dy *h2avg(pa_Da_Ma[i] ,pa_Da_Ma[pairy[i]],"pa_Da_Ma derived") );

          RF factor_iy = dx/2.*(co2[i]-co2[pairy[i]])/dy *h2avg_deriv(pa_Da_Ma[i],pa_Da_Ma[pairy[i]],"pa_Da_Ma derived");
          RF factor_ix = dy/2.*(co2[i]-co2[pairx[i]])/dx *h2avg_deriv(pa_Da_Ma[i],pa_Da_Ma[pairx[i]],"pa_Da_Ma derived");
          RF factor_yi = dx/2.*(co2[i]-co2[pairy[i]])/dy *h2avg_deriv(pa_Da_Ma[pairy[i]],pa_Da_Ma[i],"pa_Da_Ma derived");
          RF factor_xi = dy/2.*(co2[i]-co2[pairx[i]])/dx *h2avg_deriv(pa_Da_Ma[pairx[i]],pa_Da_Ma[i],"pa_Da_Ma derived");
          // derive Da by phi
          mat.accumulate(lfsv_co2,i,lfsu_phi,      i,( factor_ix
                                                  +factor_iy)*pa[      i ]*Ma[      i ]*p_h.ddifco2_dphi(phi[      i ],Sa[      i ]) );
          mat.accumulate(lfsv_co2,i,lfsu_phi,pairx[i], factor_xi *pa[pairx[i]]*Ma[pairx[i]]*p_h.ddifco2_dphi(phi[pairx[i]],Sa[pairx[i]]) );
          mat.accumulate(lfsv_co2,i,lfsu_phi,pairy[i], factor_yi *pa[pairy[i]]*Ma[pairy[i]]*p_h.ddifco2_dphi(phi[pairy[i]],Sa[pairy[i]]) );
          // derive Ma by co2
          mat.accumulate(lfsv_co2,i,lfsu_co2,      i,( factor_ix
                                                  +factor_iy)*pa[      i ]*Da[      i ]*p_h.getMa_deriv(co2[      i ])/p_h.getMco2() );
          mat.accumulate(lfsv_co2,i,lfsu_co2,pairx[i], factor_xi *pa[pairx[i]]*Da[pairx[i]]*p_h.getMa_deriv(co2[pairx[i]])/p_h.getMco2() );
          mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i], factor_yi *pa[pairy[i]]*Da[pairy[i]]*p_h.getMa_deriv(co2[pairy[i]])/p_h.getMco2() );

          // r.accumulate(lfsv_Ca,i,
          //       dx/2.*(Ca[i]-Ca[pairy[i]])/dy*h2avg(Dw[i],Dw[pairy[i]])
          //      +dy/2.*(Ca[i]-Ca[pairx[i]])/dx*h2avg(Dw[i],Dw[pairx[i]]) );
          // r.accumulate(lfsv_co2,i,
          //       dx/2.*( co2[i]-co2[pairy[i]] )/dy
          //            *  h2avg( (pa[i]+param.getpa0())*Da[i],(pa[pairy[i]]+param.getpa0())*Da[pairy[i]] )
          //      +dy/2.*( co2[i]-co2[pairx[i]] )/dx
          //            *  h2avg( (pa[i]+param.getpa0())*Da[i],(pa[pairx[i]]+param.getpa0())*Da[pairx[i]] ) );
        }

        // dispersion term TODO!
        const RF& a_T = p_h.getalfat();
        const RF& a_L = p_h.getalfal();
        RF avgKwr = mojefunkcie::hNavg(Kwr,"Kwr for dispersion in jacobian_volume");
        RF vwx = -avgKwr* (pw[3]-pw[2]+pw[1]-pw[0])/2./dx/param.getrho_w();
        RF vwy = -avgKwr*((pw[3]-pw[1]+pw[2]-pw[0])/2./dy/param.getrho_w()-param.getgrav());
        RF vwsqrt = std::max(1e-5, sqrt(vwx*vwx+vwy*vwy));
        RF sat{0};
        for (const auto& v : Sa)
          sat+=v;
        sat/=static_cast<RF>(lfsusize);
        RF Dw11 = (a_L*vwx*vwx+a_T*vwy*vwy)/vwsqrt/((1.-sat)*(1-sat));
        RF Dw22 = (a_T*vwx*vwx+a_L*vwy*vwy)/vwsqrt/((1.-sat)*(1-sat));
        RF Dw12 = (a_L-a_T)*vwx*vwy/vwsqrt;
        // const RF Caderx = (Ca[3]-Ca[2]+Ca[1]-Ca[0])/dx/2.;
        // const RF Cadery = (Ca[3]-Ca[1]+Ca[2]-Ca[0])/dy/2.;

        RF avgKarc = mojefunkcie::hNavg(Karc,"Karc for dispersion in jacobian_volume");
        RF rhoa = param.getrho_a(mojefunkcie::aritavg(pa));
        RF vax = -avgKarc* (pa[3]-pa[2]+pa[1]-pa[0])/2./dx/rhoa;
        RF vay = -avgKarc*((pa[3]-pa[1]+pa[2]-pa[0])/2./dy/rhoa-param.getgrav());
        RF vasqrt = std::max(1e-5, sqrt(vax*vax+vay*vay));
        RF Da11 = (a_L*vax*vax+a_T*vay*vay)/vasqrt/sat/sat;
        RF Da22 = (a_T*vax*vax+a_L*vay*vay)/vasqrt/sat/sat;
        RF Da12 = (a_L-a_T)*vax*vay/vasqrt;
        // const RF co2derx = (co2[3]-co2[2]+co2[1]-co2[0])/dx/2.;
        // const RF co2dery = (co2[3]-co2[1]+co2[2]-co2[0])/dy/2.;

        for (int i=0; i<int(lfsusize); ++i)
        {
          int sign = (i-pairx[i])*(i-pairy[i])/2;
          // calcium terms
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,i,
                   dy/2.*(  Dw11/dx + Dw12/dy/2.*  sign)
                  +dx/2.*(  Dw22/dy + Dw12/dx/2.*  sign) );
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,pairx[i],
                   dy/2.*( -Dw11/dx + Dw12/dy/2.*  sign )    // pairx[i] has different sign at Dw11, but same at Dw12
                  +dx/2.*(            Dw12/dx/2.*(-sign)) ); // does not appear at Dw22, different sign at Dw21=Dw12
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,pairy[i],
                   dy/2.*(            Dw12/dy/2.*(-sign))
                  +dx/2.*( -Dw22/dy + Dw12/dx/2.*  sign ) );
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,3-i, // 3-i == pairx[pairy[i]] == pairy[pairx[i]]
                   dy/2.*(            Dw12/dy/2.*(-sign))
                  +dx/2.*(            Dw12/dx/2.*(-sign)) );
          // carbon dioxide terms
          mat.accumulate(lfsv_co2,i,lfsu_co2,i,
                   dy/2.*(  Da11/dx + Da12/dy/2.*  sign)
                  +dx/2.*(  Da22/dy + Da12/dx/2.*  sign) );
          mat.accumulate(lfsv_co2,i,lfsu_co2,pairx[i],
                   dy/2.*( -Da11/dx + Da12/dy/2.*  sign )
                  +dx/2.*(            Da12/dx/2.*(-sign)) );
          mat.accumulate(lfsv_co2,i,lfsu_co2,pairy[i],
                   dy/2.*(            Da12/dy/2.*(-sign))
                  +dx/2.*( -Da22/dy + Da12/dx/2.*  sign ) );
          mat.accumulate(lfsv_co2,i,lfsu_co2,3-i,
                   dy/2.*(            Da12/dy/2.*(-sign))
                  +dx/2.*(            Da12/dx/2.*(-sign)) );
          // // here (i-pair[]) is used to determine signum, i must be int!
          // // TODO: this is valid for saturated flow, unsaturated is balanced by Sw^{-2.2} (not sure which exact power to use)
          // r.accumulate(lfsv_Ca,i,
          //       dy/2.*(Dw11*(Ca[i]-Ca[pairx[i]])/dx+Dw12*Cadery*(i-pairx[i])  )
          //      +dx/2.*(Dw22*(Ca[i]-Ca[pairy[i]])/dy+Dw12*Caderx*(i-pairy[i])/2) );
          // r.accumulate(lfsv_co2,i,
          //       dy/2.*(Da11*(co2[i]-co2[pairx[i]])/dx+Da12*co2dery*(i-pairx[i])  )
          //      +dx/2.*(Da22*(co2[i]-co2[pairy[i]])/dy+Da12*co2derx*(i-pairy[i])/2) );
        }
      }