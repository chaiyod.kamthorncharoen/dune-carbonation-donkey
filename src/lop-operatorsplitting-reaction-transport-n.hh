/* A system solving contaminant transport, chemical reactions, and
 * changing porosity. Part of a system including also two-phase flow
 * in porous medium, which is solved by operator splitting method.
 * Contaminants used are Ca ions dissolved in water and CO_2 in air.
 * Ca in terms of molar concentration and CO_2 in terms of partial
 * concentration in air.
 * Chemical reactions considered are carbonation ad leaching.
 * Carbonation is a reaction of dissolved Ca ions with dissolved CO_2
 * (CO_3 ions) forming Calcite CaCO_3. Since reaction is instaneous,
 * concentration of CO_2 in water is not modelled. Carbonation takes
 * place only in region with both phases present, reduces porosity.
 * Leaching is a dissolution of hydrated products inside concrete
 * (not Calcite). Depends on the water pH (Ca ions content). Leaching
 * increases porosity. Various types of minerals are leached at 
 * different concentrations, so it gives different volumes of leached
 * material, different Ca concentrations and amount of water into the
 * system. Total leached amount is tracked by CaStored variable.
 */

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/type.hh>

#include<dune/geometry/referenceelements.hh>
#include<dune/pdelab/common/quadraturerules.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/finiteelement/localbasiscache.hh>

/** a local operator for solving contaminant transport and
 *    chemical reactions in porous medium using vertex-centered
 *    finite volume method. Elements are either triangles or
 *    axiparallel rectangles, or mixed (not tested).
 *  Variables are Calcium molar concentration in water Ca, and CO_2 concentration in air \bar{c}.
 *  \phi is porosity, lower indices _w, _a stand for water and air, S_* is saturation,
 *  \vec{q_*} is flow, p_* pressure, \rho_* density, D_* dispersion+diffusion coeff. (flow-dep., no dispersion yet),
 *  \sigma(S_w) is speed of carbonation, k_{s,l} speed of leaching/decalcification, and
 *  C_{aS} is amount of dissolved Ca based on its concentration. Others are constants.
 *
 * \f{align*}{
 *   \partial_t(\phi S_w Ca) + \mathrm{div}[ \frac{\vec{q_w}}{\rho_w} Ca - D_w\nabla Ca ]
 *       &=& -\sigma(S_w)\phi S_w Ca p_{\bar{c}} + k_{s,l}\frac{\mathrm{d}C_{aS}(Ca)}{\mathrm{d}t} \in \Omega,\\
 *   \partial_t(\phi S_a \rho_{\bar{c}}) + \mathrm{div}[ \frac{\vec{q_a}}{\rho_a}\rho_{\bar{c}}
 *       -D_a\frac{M_{\bar{c}}}{RT}p_a\nabla\left(\frac{p_{\bar{c}}}{p_a}\right) ]
 *       &=& -\sigma(S_w)\phi S_w M_{\bar{c}} Ca p_{\bar{c}} \in \Omega,\\
 *                 \vec{q}_{Ca} \cdot \nu &=& 0             \ x=0     \\
 *                 \vec{q}_{co2}\cdot \nu &=& 0             \ x=0     \\
 *                       p_{co2}\cdot \nu &=& 0.5(pa+pa0)   \ x=MAX_x \\ %Dirichlet, accelerated conditions, high CO_2 pressure
 *                     \nabla Ca\cdot \nu &=& 0             \ x=Max_x \\ %free outflow, convection applies - can be flushed out
 // TODO: boundary conditions and initial conditions
 // for now I use temporary:
 *                 \vec{q}_{Ca} \cdot \nu &=& 0       \ y=0     \\
 *                 \vec{q}_{co2}\cdot \nu &=& 0       \ y=0     \\
 *                 \vec{q}_{Ca} \cdot \nu &=& 0       \ y=MAX_y \\
 *                 \vec{q}_{co2}\cdot \nu &=& 0       \ y=MAX_y \\
 *
 * \f}
 *
 */

class WrongConstructor : public Dune::Exception
{};

template<typename Param_chem, typename GFSF, typename ZF, typename GFSC=GFSF, typename ZC=ZF, int osType=7>
// GFSF,ZF are wflow type
class LOP_spatial_contaminant
  : public Dune::PDELab::FullVolumePattern
  , public Dune::PDELab::FullSkeletonPattern
  , public Dune::PDELab::LocalOperatorDefaultFlags
  , public Dune::PDELab::NumericalJacobianVolume<LOP_spatial_contaminant<Param_chem,GFSF,ZF,GFSC,ZC, osType> >
  , public Dune::PDELab::NumericalJacobianBoundary<LOP_spatial_contaminant<Param_chem,GFSF,ZF,GFSC,ZC, osType> >
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
  // osType = operator splitting type
  // osType == 1 ~~> advection part
  // osType == 2 ~~> diffusion part
  // osType == 4 ~~> reaction  part
  // sum of parts decides, for which parts variables are used (other parts use loaded data) eg:
  // osType == 3 ~~> advection and diffusion parts
  // osType == 7 ~~> full contaminant, all parts
  Param_chem& p_h;
  typename Param_chem::Param_wflow& param;
  typedef Dune::PDELab::LocalFunctionSpace<GFSF> LFS;
  typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
  typedef typename LFS::template Child<0>::Type::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeTypeC;
  typedef typename LFS::template Child<0>::Type::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
  typedef typename LFS::template Child<0>::Type::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;
  CachedParam<GFSF,ZF,RF> datafnew; // stored values from water flow -current, iterated
  CachedParam<GFSF,ZF,RF> datafold; // stored values from water flow -previous time step
  CachedParam<GFSC,ZC,RF> datacnew; // stored values from contaminant -other operator splitting part result, iterated
  CachedParam<GFSC,ZC,RF> datacold; // stored values from contaminant -previous time step

public:
  // pattern assembly flags
  enum { doPatternVolume = true };
  //  enum { doPatternSkeleton = true }; // skeleton terms "faked" in alpha_volume

  // residual assembly flags
  //  enum { doLambdaBoundary = true };// side boundaries in alpha
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary  = true };// boundaries

  //! constructor stores a copy of the parameter object, flow data, and ontaminant data for operator splitting
  LOP_spatial_contaminant (Param_chem&  param_chem_, const GFSF& gfsf_, ZF& zfnew_, ZF& zfold_, const GFSC& gfsc_, ZC& zcnew_, ZC& zcold_)
    : p_h(param_chem_)
    , param(param_chem_.param)
    , datafnew(gfsf_,zfnew_)
    , datafold(gfsf_,zfold_)
    , datacnew(gfsc_,zcnew_)
    , datacold(gfsc_,zcold_)
  {}
  //! Backward compatible constructor. Data for contaminant are not used in osType==7 case.
  LOP_spatial_contaminant (Param_chem&  param_chem_, const GFSF& gfsf_, ZF& zfnew_, ZF& zfold_, const GFSC& gfsc_, ZC& zcold_)
    : p_h(param_chem_)
    , param(param_chem_.param)
    , datafnew(gfsf_,zfnew_)
    , datafold(gfsf_,zfold_)
    , datacnew(gfsc_,zcold_) // not used, but does not accept empty constructor...
    , datacold(gfsc_,zcold_)
  {
    if (osType!=7)
      DUNE_THROW(WrongConstructor,"Supply chem data vector if chem part is computed using operator splitting!");
  }

  void setTime(double t)
  {
    param.setTime(t);
  }

  void setTimeStep(double dt)
  {
    param.setTimeStep(dt);
  }

  void adjustTimeStep(double coef)
  {
    param.adjustTimeStep(coef);
  }

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X,
           typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, R& r) const
  {
    using mojefunkcie::h2avg;
    using mojefunkcie::hNavg;
    // get access to each vector space
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_Ca = lfsu.child(_0);
    const auto& lfsu_co2 = lfsu.child(_1);
    const auto& lfsu_phi = lfsu.child(_2);
    // const auto& lfsu_CaStored = lfsu.child(_3);
    const auto& lfsv_Ca = lfsv.child(_0);
    const auto& lfsv_co2 = lfsv.child(_1);
    const auto& lfsv_phi = lfsv.child(_2);
    const auto& lfsv_CaStored = lfsv.child(_3);
    // typedef decltype(makeZeroBasisFieldValue(lfsu_Ca)) RFU;
    const std::size_t& lfsusize = lfsv_Ca.size();
    const auto& inside_cell = eg.entity();
    std::vector<RF> pw(lfsusize);
    std::vector<RF> Sa(lfsusize);
    std::vector<RF> pa(lfsusize);
    // std::vector<RF> pwold(lfsusize);
    // std::vector<RF> Saold(lfsusize);
    // std::vector<RF> paold(lfsusize);
    std::vector<RF> Ca(lfsusize);
    std::vector<RF> CaData(lfsusize);
    std::vector<RF> co2(lfsusize);
    std::vector<RF> co2Data(lfsusize);
    std::vector<RF> phi(lfsusize);
    std::vector<RF> Caold(lfsusize);
    std::vector<RF> co2old(lfsusize);
    std::vector<RF> phiold(lfsusize);
    // std::vector<RF> CaStored(lfsusize);
    std::vector<RF> clf=datafnew.read(inside_cell);
    std::vector<RF> clfold=datafold.read(inside_cell);
    std::vector<RF> clc=datacold.read(inside_cell);
    std::vector<RF> clcnew=datacnew.read(inside_cell);
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pw[i] = clf[lfsusize*0+i];
      Sa[i] = clf[lfsusize*1+i];
      pa[i] = pw[i] + param.pc(Sa[i]);
      // pwold[i] = clfold[lfsusize*0+i];
      // Saold[i] = clfold[lfsusize*1+i];
      // paold[i] = pwold[i] + param.pc(Saold[i]);
      Ca[i] = x(lfsu_Ca,i);
      CaData[i] = clcnew[lfsusize*0+i];
      co2[i] = x(lfsu_co2,i);
      co2Data[i] = clcnew[lfsusize*1+i];
      phi[i] = (osType >= 4) ? x(lfsu_phi,i) : clcnew[lfsusize*2+i];
      Caold[i] = clc[lfsusize*0+i];
      // co2old[i] = clc[lfsusize*1+i];
      // phiold[i] = clc[lfsusize*2+i];
      // Caold[i] = std::max(Ca[i],clf[lfsusize*0+i]); // avoid using std::max(0., ...) in accumulate
    }
    const auto& volume = eg.geometry().volume();

    std::vector<RF>& Car{osType>=4 ? Ca : CaData};
    std::vector<RF>& co2r{osType>=4 ? co2 : co2Data};
    // // nonnegative variables for reaction part
    // std::vector<RF> Capos(lfsusize);
    // std::vector<RF> co2pos(lfsusize);
    // for(std::size_t i=0; i<lfsusize; ++i)
    // {
    //   Capos[i]    = std::max(0.,Ca[i] );
    //   co2pos[i]   = std::max(0.,co2[i]);
    // }

    // source terms, carbonation and leaching
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      // RF carbonation = std::min(p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Car[i]*co2r[i]*(pa[i]+param.getpa0()),std::min(co2r[i],Car[i]))*volume/RF(lfsusize);
      RF carbonation = p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Car[i]*co2r[i]*(pa[i]+param.getpa0())*volume/RF(lfsusize);
      // r.accumulate(lfsv_Ca,i,carbonation*p_h.getMCaO() ); // a lot faster with this, even though change to water is negligible
      // r.accumulate(lfsv_co2,i,carbonation*p_h.getmolco2() );
      r.accumulate(lfsv_Ca,i,carbonation );
      r.accumulate(lfsv_co2,i,carbonation*param.getR()*param.getT() );
      r.accumulate(lfsv_phi,i,carbonation*p_h.getV_CaCO_3() );
      // Car uses molar concentration, does not need any weights
      // CO_2 uses concentration, the equation was multiplied by RT/M_{\bar{c}}, (concentration*pressure yield partial CO2 pressure)
      // porosity uses volume fraction (dimensionless), needs Calcite volume

      // if (Car[i] < Caold[i]) // leaching is one-way only, deposition not possible
      // {
        RF ksl = p_h.getleachspeed(1.-Sa[i],Car[i])/param.getTimeStep()*volume/RF(lfsusize);  // currently describes k_{sl} and does not depend on Car
        // r.accumulate(lfsv_Ca,i, ksl*(p_h.getwls(Car[i])-p_h.getwls(Caold[i]))*p_h.getMh2o() ); // released water from hydrated products
        // r.accumulate(lfsv_Ca,i, ksl*(p_h.getCaC(Car[i])-p_h.getCaC(Caold[i]))*p_h.getMCaO() ); // released CaO into water - a tad faster without it
        r.accumulate(lfsv_Ca,i, ksl*(p_h.getCaC(Car[i])-p_h.getCaC(Caold[i])) );
        r.accumulate(lfsv_phi,i, ksl*(p_h.getVdt(Car[i])-p_h.getVdt(Caold[i])) );
        r.accumulate(lfsv_CaStored,i,-ksl*(p_h.getVdt(Car[i])-p_h.getVdt(Caold[i])) );
      // }

      // // compensation for the approximated derivation in time step
      // const auto tvol = 1./param.getTimeStep()*volume/RF(lfsusize);
      // r.accumulate(lfsv_Ca,i,((1.-Sa[i])-(1.-Saold[i]))*phi[i]*Ca[i]*tvol); // old Sw in temporal part, 2 jacobian terms
      // // r.accumulate(lfsv_Ca,i,((1.-Sa[i])-(1.-Saold[i]))*phiold[i]*Caold[i]*tvol); // current Sw in temporal part, no jacobian terms
      // r.accumulate(lfsv_co2,i,(Sa[i]*(pa[i]+param.getpa0())-Saold[i]*(paold[i]+param.getpa0()))*phi[i]*co2[i]*tvol); // old Sa*pa in temporal part, 2 jacobian terms
      // // r.accumulate(lfsv_co2,i,(Sa[i]*pa[i]-Saold[i]*paold[i])*phiold[i]*co2old[i]*tvol); // curent Sa*Pa in temporal part, no jacobian terms

    }

    std::vector<RF>& Ca_adv{osType%2==1 ? Ca : CaData};
    std::vector<RF>& Ca_dif{(osType%4)/2==1 ? Ca : CaData};
    std::vector<RF>& co2_adv{osType%2==1 ? co2 : co2Data};
    std::vector<RF>& co2_dif{(osType%4)/2==1 ? co2 : co2Data};
    // skeleton terms, convection and diffusion (and later maybe dispersion) of water, air, Ca, and CO_2
    if (lfsusize==4)
    {
      #include "lop-operatorsplitting-contaminant-alphavolume-lfsusize4-advection-diffusion-n.hh"
    } // end lfsusize==4
    else if (lfsusize == 3)
    {
      throw std::string("not finished operatorsplitting contaminant alphavolume lfsusize==3");
    } //end lfsusize==3
    else
      throw std::string("wrong lfsusize");
  } // end alpha_volume

  template<typename EG, typename LFSU, typename X,
           typename LFSV, typename MAT>
  void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, MAT& mat) const
  {
    using mojefunkcie::h2avg;
    using mojefunkcie::h2avg_deriv;
    using mojefunkcie::hNavg;
    // get access to each vector space
    assert(LFSU::CHILDREN==4);
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_Ca = lfsu.child(_0);
    const auto& lfsu_co2 = lfsu.child(_1);
    const auto& lfsu_phi = lfsu.child(_2);
    // const auto& lfsu_CaStored = lfsu.child(_3);
    const auto& lfsv_Ca = lfsv.child(_0);
    const auto& lfsv_co2 = lfsv.child(_1);
    const auto& lfsv_phi = lfsv.child(_2);
    const auto& lfsv_CaStored = lfsv.child(_3);
    // typedef decltype(makeZeroBasisFieldValue(lfsu_Ca)) RFU;
    const std::size_t& lfsusize = lfsv_Ca.size();
    std::vector<RF> pw(lfsusize);
    std::vector<RF> pwold(lfsusize);
    std::vector<RF> Sa(lfsusize);
    std::vector<RF> Saold(lfsusize);
    std::vector<RF> pa(lfsusize);
    std::vector<RF> paold(lfsusize);
    std::vector<RF> Ca(lfsusize);
    std::vector<RF> co2(lfsusize);
    std::vector<RF> phi(lfsusize);
    // std::vector<RF> CaStored(lfsusize);
    std::vector<RF> dpc(lfsusize);
    const auto& inside_cell = eg.entity();
    std::vector<RF> clf=datafnew.read(inside_cell);
    std::vector<RF> clfold=datafold.read(inside_cell);
    std::vector<RF> clc=datacold.read(inside_cell);
    std::vector<RF> clcnew=datacnew.read(inside_cell);
    std::vector<RF> Caold(lfsusize);
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pw[i]       = clf[lfsusize*0+i];
      Sa[i]       = clf[lfsusize*1+i];
      pa[i]       = pw[i] + param.pc(Sa[i]);
      // pwold[i] = clfold[lfsusize*0+i];
      // Saold[i] = clfold[lfsusize*1+i];
      // paold[i]    = pwold[i] + param.pc(Saold[i]);
      Ca[i]       = x(lfsu_Ca,i);
      co2[i]      = x(lfsu_co2,i);
      phi[i]      = x(lfsu_phi,i);
      dpc[i]      = param.pc_deriv(Sa[i]);
      Caold[i]    = clc[lfsusize*0+i];
    }
    const auto& volume = eg.geometry().volume();
    if (osType >= 4) // reaction part
    {
      // source terms, carbonation and leaching
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        // RF carbonation = std::min(p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())*volume/RF(lfsusize),std::min(co2[i],Ca[i]));
        // // RF carbonation = p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())*volume/RF(lfsusize);
        // r.accumulate(lfsv_Ca,i,carbonation*p_h.getMCaO() ); // a lot faster with this, even though change to water is negligible
        // r.accumulate(lfsv_co2,i,carbonation*p_h.getmolco2() );
        // r.accumulate(lfsv_phi,i,carbonation);
        // r.accumulate(lfsv_CaStored,i,carbonation*param.getR()*param.getT());
        // r.accumulate(lfsv_4,i,carbonation*p_h.getV_CaCO_3());

        // if (p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())<std::min(co2[i],Ca[i]))
        // if (p_h.getcarbspeed(1.-Sa[i])*phi[i]*(1.-Sa[i])*Ca[i]*co2[i]*(pa[i]+param.getpa0())<=std::min(co2[i],Ca[i]))
        if (true) // no "complementarity condition"
        {
          RF carbspeed = p_h.getcarbspeed(1.-Sa[i])*volume/RF(lfsusize);
          // RF carbonation_dpw = carbspeed*phi[i]*(1.-Sa[i])*Ca[i]*co2[i];
          // RF carbonation_dSa = ( p_h.getcarbspeed_deriv(1.-Sa[i])*(-1.)*(1.-Sa[i])*(pa[i]+param.getpa0())*volume/RF(lfsusize)
                                 // +carbspeed                            *(  -1.   )*(pa[i]+param.getpa0())
                                 // +carbspeed                            *(1.-Sa[i])* dpc[i]                )
                                // *phi[i]*Ca[i]*co2[i];
          carbspeed *= (1.-Sa[i])*(pa[i]+param.getpa0());
          RF carbonation_dCa = carbspeed*phi[i]*co2[i];
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,i,carbonation_dCa);
          mat.accumulate(lfsv_co2,i,lfsu_Ca,i,carbonation_dCa*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_Ca,i,carbonation_dCa*p_h.getV_CaCO_3());

          carbspeed *= Ca[i];
          RF carbonation_dco2 = carbspeed*phi[i];
          mat.accumulate(lfsv_Ca,i,lfsu_co2,i,carbonation_dco2);
          mat.accumulate(lfsv_co2,i,lfsu_co2,i,carbonation_dco2*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_co2,i,carbonation_dco2*p_h.getV_CaCO_3());

          carbspeed *= co2[i]; // ==carbonation_dphi
          mat.accumulate(lfsv_Ca,i,lfsu_phi,i,carbspeed);
          mat.accumulate(lfsv_co2,i,lfsu_phi,i,carbspeed*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_phi,i,carbspeed*p_h.getV_CaCO_3());
          // it may be easier to just divide "carbonation" by deriver, but zero_division must be avoided
        }
        // else if (Ca[i]<co2[i]) // & Ca[i] <= carbonation
        else if (Ca[i]<=co2[i]) // & Ca[i] < carbonation term
        {
          mat.accumulate(lfsv_Ca,i,lfsu_phi,i,volume/RF(lfsusize) );
          mat.accumulate(lfsv_co2,i,lfsu_phi,i,volume/RF(lfsusize)*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_phi,i,volume/RF(lfsusize)*p_h.getV_CaCO_3());
        }
        else // if (co2[i]<=Ca[i]<=carbonation) // & co2 partial pressure < carbonation term
        {
          // d_co2
          mat.accumulate(lfsv_Ca,i,lfsu_co2,i,volume/RF(lfsusize) );
          mat.accumulate(lfsv_co2,i,lfsu_co2,i,volume/RF(lfsusize)*param.getR()*param.getT());
          mat.accumulate(lfsv_phi,i,lfsu_co2,i,volume/RF(lfsusize)*p_h.getV_CaCO_3());
          // d_pw
          // mat.accumulate(lfsv_Ca,i,lfsu_Ca,i,co2[i]*volume/RF(lfsusize)*p_h.getMCaO());
          // mat.accumulate(lfsv_co2,i,lfsu_Ca,i,co2[i]*volume/RF(lfsusize)*p_h.getmolco2());
          // mat.accumulate(lfsv_phi,i,lfsu_Ca,i,co2[i]*volume/RF(lfsusize) );
          // mat.accumulate(lfsv_CaStored,i,lfsu_Ca,i,co2[i]*volume/RF(lfsusize)*param.getR()*param.getT());
          // mat.accumulate(lfsv_4,i,lfsu_Ca,i,co2[i]*volume/RF(lfsusize)*p_h.getV_CaCO_3());
          // d_Sa
          // mat.accumulate(lfsv_Ca,i,lfsu_co2,i,co2[i]*dpc[i]*volume/RF(lfsusize)*p_h.getMCaO());
          // mat.accumulate(lfsv_co2,i,lfsu_co2,i,co2[i]*dpc[i]*volume/RF(lfsusize)*p_h.getmolco2());
          // mat.accumulate(lfsv_phi,i,lfsu_co2,i,co2[i]*dpc[i]*volume/RF(lfsusize) );
          // mat.accumulate(lfsv_CaStored,i,lfsu_co2,i,co2[i]*dpc[i]*volume/RF(lfsusize)*param.getR()*param.getT());
          // mat.accumulate(lfsv_4,i,lfsu_co2,i,co2[i]*dpc[i]*volume/RF(lfsusize)*p_h.getV_CaCO_3());
        }


        // if (Ca[i] < Caold[i]) // leaching is one-way only, deposition not possible
        // {
          // r.accumulate(lfsv_Ca,i, ksl*(p_h.getwls(Capos[i])-p_h.getwls(Caold[i]))*p_h.getMh2o() );
          // r.accumulate(lfsv_Ca,i, ksl*(p_h.getCaC(Capos[i])-p_h.getCaC(Caold[i]))*p_h.getMCaO() );
          // r.accumulate(lfsv_phi,i, ksl*(p_h.getCaC(Capos[i])-p_h.getCaC(Caold[i])) );
          // r.accumulate(lfsv_4,i, ksl*(p_h.getVdt(Capos[i])-p_h.getVdt(Caold[i])) );
          // r.accumulate(lfsv_5,i,-ksl*(p_h.getVdt(Capos[i])-p_h.getVdt(Caold[i])) );
          RF ksl = p_h.getleachspeed(1.-Sa[i],Ca[i])/param.getTimeStep()*volume/RF(lfsusize);
          // RF ksl = p_h.getleachspeed(1.-Sa[i],Capos[i])/param.getTimeStep()*volume/RF(lfsusize);
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,i, ksl*p_h.dCaC_dCa(Ca[i]) );
          mat.accumulate(lfsv_phi,i,lfsu_Ca,i, ksl*p_h.dVdt_dCa(Ca[i]) );
          mat.accumulate(lfsv_CaStored,i,lfsu_Ca,i,-ksl*p_h.dVdt_dCa(Ca[i]) );
          // RF ksl_dSa = p_h.dleachspeed_dSw(1.-Sa[i],Ca[i])*(-1.)/param.getTimeStep()*volume/RF(lfsusize);
          // RF ksl_dSa = p_h.dleachspeed_dSw(1.-Sa[i],Capos[i])*(-1.)/param.getTimeStep()*volume/RF(lfsusize);
          RF ksl_dCa = p_h.dleachspeed_dCa(1.-Sa[i],Ca[i])/param.getTimeStep()*volume/RF(lfsusize);
          // RF ksl_dCa = p_h.dleachspeed_dCa(1.-Sa[i],Capos[i])/param.getTimeStep()*volume/RF(lfsusize);
          mat.accumulate(lfsv_Ca,i,lfsu_Ca,i, ksl_dCa*(p_h.getCaC(Ca[i])-p_h.getCaC(Caold[i])) );
          mat.accumulate(lfsv_phi,i,lfsu_Ca,i, ksl_dCa*(p_h.getVdt(Ca[i])-p_h.getVdt(Caold[i])) );
          mat.accumulate(lfsv_CaStored,i,lfsu_Ca,i,-ksl_dCa*(p_h.getVdt(Ca[i])-p_h.getVdt(Caold[i])) );
        // }

        // // compensation for the approximated derivation in time step
        // const auto tvol = 1./param.getTimeStep()*volume/RF(lfsusize);
        // // r.accumulate(lfsv_Ca,i,((1.-Sa[i])-(1.-Saold[i]))*phi[i]*Ca[i]*tvol); // old Sw in temporal part
        // mat.accumulate(lfsv_Ca,i,lfsu_Ca,i,((1.-Sa[i])-(1-Saold[i]))*phi[i]*tvol);
        // mat.accumulate(lfsv_Ca,i,lfsu_phi,i,((1.-Sa[i])-(1-Saold[i]))* Ca[i]*tvol);
        // // r.accumulate(lfsv_co2,i,(Sa[i]*(pa[i]+param.getpa0())-Saold[i]*(paold[i]+param.getpa0()))*phi[i]*co2[i]*tvol); // old Sa*pa in temporal part
        // mat.accumulate(lfsv_co2,i,lfsu_co2,i,(Sa[i]*(pa[i]+param.getpa0())-Saold[i]*(paold[i]+param.getpa0()))*phi[i]*tvol);
        // mat.accumulate(lfsv_co2,i,lfsu_phi,i,(Sa[i]*(pa[i]+param.getpa0())-Saold[i]*(paold[i]+param.getpa0()))*co2[i]*tvol);
      }
    }

    if (osType%2==1 || (osType%4)/2==1) // transport part
    {
      // skeleton terms, convection and diffusion (and later maybe dispersion) of water, air, Ca, and CO_2
      if (lfsusize==4)
      {
        #include "lop-operatorsplitting-contaminant-jacobianvolume-lfsusize4-advection-diffusion-n.hh"
      } // end lfsusize==4
      else if (lfsusize == 3)
      {
        throw std::string("not finished operatorsplitting contaminant jacobianvolume lfsusize==3");
      } //end lfsusize==3
      else
        throw std::string("wrong lfsusize");
    }
  } //end jacobian_volume */


  template<typename IG, typename LFSU, typename X,
           typename LFSV, typename R>
  void alpha_boundary (const IG& ig,
                       const LFSU& lfsu_i, const X& x_i,
                       const LFSV& lfsv_i, R& r_i) const
  {
    using mojefunkcie::h2avg;
    //const auto& geo = ig.inside().geometry(); // inside Entity
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_Ca = lfsu_i.child(_0);
    const auto& lfsu_co2 = lfsu_i.child(_1);
    const auto& lfsu_phi = lfsu_i.child(_2);
    // const auto& lfsu_CaStored = lfsu_i.child(_3);
    const auto& lfsv_Ca = lfsv_i.child(_0);
    const auto& lfsv_co2 = lfsv_i.child(_1);
    // const auto& lfsv_phi = lfsv_i.child(_2);
    // const auto& lfsv_CaStored = lfsv_i.child(_3);
    const std::size_t& lfsusize = lfsv_Ca.size();
    // do not forget to divide by 2, I integrate only on half of face (or less in higher dim)
    const auto& face_volume = ig.geometry().volume();

    const auto& global = ig.geometry().center();
    const auto& inside_cell = ig.inside();
    std::vector<RF> pw(lfsusize);
    std::vector<RF> Sa(lfsusize);
    std::vector<RF> Ca(lfsusize);
    std::vector<RF> co2(lfsusize);
    std::vector<RF> pa(lfsusize);
    std::vector<RF> phi(lfsusize);
    std::vector<RF> clf=datafnew.read(inside_cell);
    std::vector<RF> clcnew=datacnew.read(inside_cell);

    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pw[i] = clf[lfsusize*0+i];
      Sa[i] = clf[lfsusize*1+i];
      pa[i] = pw[i]+param.pc(Sa[i]);
      Ca[i]  = (osType%4==3) ? x_i(lfsu_Ca,i) : clcnew[lfsusize*0+i];
      co2[i] = (osType%4==3) ? x_i(lfsu_co2,i) : clcnew[lfsusize*1+i];
      phi[i] = (osType%4==3) ? x_i(lfsu_phi,i) : clcnew[lfsusize*2+i];
    }

    if (lfsusize==4)
    {
      //right boundary  fixed CO_2 pressure
      //                Ca fixed concentration 0, or Ca can be flushed out with water, no inflow
      if (global[0]>=param.getwidth()-1e-7)
      {
        // water and air flow have dirichlet coditions

        // RF idx = 1./(ig.inside().geometry().corner(1)[0]-ig.inside().geometry().corner(0)[0]);
        // const RF& dy = face_volume;
        // right boundary condition
        // dirichlet condition for CO_2, not specified here
        // free outflow Ca

        // // convection term
        // RF bfloa = -param.Kar(clf[1+lfsusize],pa[1],porosity[1])*(param.ga_nitsche(ig,x_i)-pa[0])*idx;
        // if (bfloa<0) // inflow, inflowing air has outside CO_2 partial_pressure/density
        //   {
        //     const RF& pco2 = p_h.getrhoco2(p_h.getairco2()*param.getpa0());
        //     r_i.accumulate(lfsv_co2,1,face_volume/2*bfloa/param.getrho_a(pa[1])*pco2);
        //   }
        // else // outflow, outflowing air has boundary CO_2 concentration/mass
        //   {
        //     r_i.accumulate(lfsv_co2,1,face_volume/2*bfloa/param.getrho_a(pa[1])*co2[1]);
        //   }
        // bfloa = -param.Kar(clf[3+lfsusize],pa[3],porosity[3])*(param.ga_nitsche(ig,x_i)-pa[3])*idx;
        // if (bfloa<0) // inflow, inflowing air has outside CO_2 partial_pressure/density
        //   {
        //     const RF& pco2 = p_h.getrhoco2(p_h.getairco2()*param.getpa0());
        //     r_i.accumulate(lfsv_co2,3,face_volume/2*bfloa/param.getrho_a(pa[3])*pco2);
        //   }
        // else // outflow, outflowing air has boundary CO_2 concentration/mass
        //   {
        //     r_i.accumulate(lfsv_co2,3,face_volume/2*bfloa/param.getrho_a(pa[3])*co2[3]);
        //   }

        // // Ca insulated, convection may flush it out
        // RF idx = 1./(ig.inside().geometry().corner(1)[0]-ig.inside().geometry().corner(0)[0]);
        // RF bflow1, bflow3; // consider not average permeabilities
        // bflow1 = -h2avg(param.Kwr(Sa[0],phi[0]),param.Kwr(Sa[1],phi[1]))*(pw[1]-pw[0])*idx;
        // bflow3 = -h2avg(param.Kwr(Sa[2],phi[2]),param.Kwr(Sa[3],phi[3]))*(pw[3]-pw[2])*idx;
        // if (bflow1>0) // outflow, outflowing water has boundary Ca concentration/mass
        // {
        //   r_i.accumulate(lfsv_Ca,1,face_volume/2.*bflow1/param.getrho_w()*Ca[1]);
        // }
        // if (bflow3>0) // outflow, outflowing water has boundary Ca concentration/mass
        // {
        //   r_i.accumulate(lfsv_Ca,3,face_volume/2.*bflow3/param.getrho_w()*Ca[3]);
        // }
      }
      // bottom boundary, symmetry:  \partial_y = 0  for Ca and CO_2
      // this practically removes diffusion term and accounts only for convection
      else if (global[1]<=1e-7)
      {
        RF bflow0 =  param.Kw(Sa[0],phi[0])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[1],phi[1])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        r_i.accumulate(lfsv_Ca,0,face_volume/2.*bflow0*Ca[0]);
        r_i.accumulate(lfsv_Ca,1,face_volume/2.*bflow1*Ca[1]);
        RF bfloa0 =  param.Karc(Sa[0],pa[0],phi[0],co2[0])*(-p_h.getrho_a(pa[0],co2[0])*param.getgrav())/param.rho_a_deriv();
        RF bfloa1 =  param.Karc(Sa[1],pa[1],phi[1],co2[1])*(-p_h.getrho_a(pa[1],co2[1])*param.getgrav())/param.rho_a_deriv();
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        r_i.accumulate(lfsv_co2,0,face_volume/2.*bfloa0*co2[0]);
        r_i.accumulate(lfsv_co2,1,face_volume/2.*bfloa1*co2[1]);
      }
      // top boundary, symmetry:  \partial_y = 0  for Ca and CO_2
      else if (global[1]>=param.getheight()-1e-7)
      {
        RF bflow2 = -param.Kw(Sa[2],phi[2])*(-param.getrho_w()*param.getgrav());
        RF bflow3 = -param.Kw(Sa[3],phi[3])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        r_i.accumulate(lfsv_Ca,2,face_volume/2.*bflow2*Ca[2]);
        r_i.accumulate(lfsv_Ca,3,face_volume/2.*bflow3*Ca[3]);
        RF bfloa2 = -param.Karc(Sa[2],pa[2],phi[2],co2[2])*(-param.getrho_a(pa[2])*param.getgrav())/param.rho_a_deriv();
        RF bfloa3 = -param.Karc(Sa[3],pa[3],phi[3],co2[3])*(-param.getrho_a(pa[3])*param.getgrav())/param.rho_a_deriv();
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        r_i.accumulate(lfsv_co2,2,face_volume/2.*bfloa2*co2[2]);
        r_i.accumulate(lfsv_co2,3,face_volume/2.*bfloa3*co2[3]);
      }
    }
    else // lfsusize==3
    {
      if (global[0]>=param.getwidth()-1e-7) // right boundary condition
      {
        // water, air have dirichlet conditions
        // Ca is insulated, but has free outflow with convection
        // CO_2 has dirichlet condition

        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        // compute outflow
        std::array<Dune::FieldVector<RF,2>, 3> vertex;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          vertex[i] = inside_geo.corner(i);
        }
        auto lambda_notrightvertex = [&](std::size_t i) ->bool {return vertex[i][0]<param.getwidth()-1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_notrightvertex,lfsusize);

        // dirichlet condition for CO_2, not specified here
        // free outflow Ca

        // std::array<RF,2> gradpa;
        // gradpa = param.ReconstructFlow(pa,vertex);
        // // gradpa[1]+= -param.getgrav()*param.getrho_a(apa); // I need only x direction
        // RF bfloa = -param.Kar(clf[active[0]+lfsusize],pa[active[0]],porosity[active[0]])*gradpa[1];
        // if (bfloa<0) // inflow, inflowing air has outside CO_2 partial_pressure/density
        //   {
        //     const RF& pco2 = p_h.getrhoco2(p_h.getairco2()*param.getpa0());
        //     r_i.accumulate(lfsv_co2,active[0],face_volume/2*bfloa/param.getrho_a(pa[active[0]])*pco2);
        //   }
        // else // outflow, outflowing air has boundary CO_2 concentration/mass
        //   {
        //     r_i.accumulate(lfsv_co2,active[0],face_volume/2*bfloa/param.getrho_a(pa[active[0]])*co2[active[0]]);
        //   }
        // bfloa = -param.Kar(clf[active[1]+lfsusize],pa[active[1]],porosity[active[1]])*gradpa[1];
        // if (bfloa<0) // inflow, inflowing air has outside CO_2 partial_pressure/density
        //   {
        //     const RF& pco2 = p_h.getrhoco2(p_h.getairco2()*param.getpa0());
        //     r_i.accumulate(lfsv_co2,active[1],face_volume/2*bfloa/param.getrho_a(pa[active[1]])*pco2);
        //   }
        // else // outflow, outflowing air has boundary CO_2 concentration/mass
        //   {
        //     r_i.accumulate(lfsv_co2,active[1],face_volume/2*bfloa/param.getrho_a(pa[active[1]])*co2[active[1]]);
        //   }

        // free outflow Ca, may be flushed out by convection
        std::array<RF,2> nablaw = mojefunkcie::ReconstructFlow(pw,vertex);
        std::array<RF,3> Kw;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          Kw[i] = param.Kw(Sa[i],phi[i]);
        }
        const RF avgKw = mojefunkcie::hNavg(Kw,"Kw in alpha_boundary");
        const RF bflow = -avgKw*nablaw[0];
        if (bflow>0) // outflowing water contains Ca, inflowing does not -no term for bflow<0
        {
          r_i.accumulate(lfsv_Ca,active[0],face_volume/2.*bflow*Ca[active[0]]);
          r_i.accumulate(lfsv_Ca,active[1],face_volume/2.*bflow*Ca[active[1]]);
          // r_i.accumulate(lfsv_phi,active[0],face_volume/2*(-Kw[active[0]]*nablaw[0])*Ca[active[0]]);
          // r_i.accumulate(lfsv_phi,active[1],face_volume/2*(-Kw[active[1]]*nablaw[0])*Ca[active[1]]);
          // commented slightly improves performance, but outflow is too slow, Ca is accumulated at boundary
        }
      }
      // bottom
      else if (global[1]<=1e-7)
      {
        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        auto lambda_notbottomvertex = [&](std::size_t i) ->bool {return inside_geo.corner(i)[1]>1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_notbottomvertex,lfsusize);

        std::array<RF,2> viscmix {param.getvisc_a()/p_h.visc_mix(co2[active[0]]),param.getvisc_a()/p_h.visc_mix(co2[active[1]])};
        RF bflow0 =  param.Kw(Sa[active[0]],phi[active[0]])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[active[1]],phi[active[1]])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        r_i.accumulate(lfsv_Ca,active[0],face_volume/2.*bflow0*Ca[active[0]]);
        r_i.accumulate(lfsv_Ca,active[1],face_volume/2.*bflow1*Ca[active[1]]);
        RF bfloa0 =  param.Ka(Sa[active[0]],phi[active[0]])*viscmix[0]*(-p_h.getrho_a(pa[active[0]],co2[active[0]])*param.getgrav());
        RF bfloa1 =  param.Ka(Sa[active[1]],phi[active[1]])*viscmix[1]*(-p_h.getrho_a(pa[active[1]],co2[active[1]])*param.getgrav());
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        r_i.accumulate(lfsv_co2,active[0],face_volume/2.*bfloa0*co2[active[0]]);
        r_i.accumulate(lfsv_co2,active[1],face_volume/2.*bfloa1*co2[active[1]]);
      }
      // top
      else if (global[1]>=param.getheight()-1e-7)
      {
        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        auto lambda_nottopvertex = [&](std::size_t i) ->bool {return inside_geo.corner(i)[1]<param.getheight()-1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_nottopvertex,lfsusize);
        std::array<RF,2> viscmix {param.getvisc_a()/p_h.visc_mix(co2[active[0]]),param.getvisc_a()/p_h.visc_mix(co2[active[1]])};

        RF bflow0 =  param.Kw(Sa[active[0]],phi[active[0]])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[active[1]],phi[active[1]])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        r_i.accumulate(lfsv_Ca,active[0],face_volume/2.*bflow0*Ca[active[0]]);
        r_i.accumulate(lfsv_Ca,active[1],face_volume/2.*bflow1*Ca[active[1]]);
        RF bfloa0 =  param.Ka(Sa[active[0]],phi[active[0]])*viscmix[0]*(-p_h.getrho_a(pa[active[0]],co2[active[0]])*param.getgrav());
        RF bfloa1 =  param.Ka(Sa[active[1]],phi[active[1]])*viscmix[1]*(-p_h.getrho_a(pa[active[1]],co2[active[1]])*param.getgrav());
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        r_i.accumulate(lfsv_co2,active[0],face_volume/2.*bfloa0*co2[active[0]]);
        r_i.accumulate(lfsv_co2,active[1],face_volume/2.*bfloa1*co2[active[1]]);
      }
    } // end lfsusize==3
  } // end alpha_boundary
/*
  template<typename IG, typename LFSU, typename X,
           typename LFSV, typename M>
  void jacobian_boundary (const IG& ig,
                          const LFSU& lfsu_i, const X& x_i,
                          const LFSV& lfsv_i, M& mat_ii) const
  {
    using mojefunkcie::h2avg;
    //const auto& geo = ig.inside().geometry(); // inside Entity
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_Ca = lfsu_i.child(_0);
    const auto& lfsu_co2 = lfsu_i.child(_1);
    const auto& lfsu_phi = lfsu_i.child(_2);
    const auto& lfsu_CaStored = lfsu_i.child(_3);
    const auto& lfsu_4 = lfsu_i.child(_4);
    // const auto& lfsu_5 = lfsu_i.child(_5);
    //assert(LFSV::CHILDREN==4);
    const auto& lfsv_Ca = lfsv_i.child(_0);
    const auto& lfsv_co2 = lfsv_i.child(_1);
    const auto& lfsv_phi = lfsv_i.child(_2);
    const auto& lfsv_CaStored = lfsv_i.child(_3);
    // const auto& lfsv_4 = lfsv_i.child(_4);
    // const auto& lfsv_5 = lfsv_i.child(_5);
    const std::size_t& lfsusize = lfsv_Ca.size();
    // do not forget to divide by 2, I integrate only on half of face (or less in higher dim)
    const auto& face_volume = ig.geometry().volume();

    const auto& global = ig.geometry().center();
    auto fillvector = [&](decltype(lfsu_Ca) lfsuch){
      std::vector<RF> v;
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        v[i] = x_i(lfsuch,i);
      }

    std::vector<RF> pw{fillvector(lfsu_Ca)};
    std::vector<RF> Sa{fillvector(lfsu_co2)};
    std::vector<RF> Ca{fillvector(lfsu_phi)};
    std::vector<RF> co2{fillvector(lfsu_CaStored)};
    std::vector<RF> pa(lfsusize);
    std::vector<RF> phi{fillvector(lfsu_4)};
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pa[i] = pw[i]+param.pc(Sa[i]);
    }

    if (lfsusize==4)
    {
      throw "jacobian boundary for lfsusize==4 is not finished yet";
    }
    else // lfsusize==3
    {
      if (global[0]>=param.getwidth()-1e-7) // right boundary condition
      {
        // water, air have dirichlet conditions
        // Ca is insulated, but has free outflow with convection
        // CO_2 has dirichlet condition

        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        // compute outflow
        std::array<Dune::FieldVector<RF,2>, 3> vertex;
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          vertex[i] = inside_geo.corner(i);
        }
        auto lambda_notrightvertex = [&](std::size_t i) ->bool {return vertex[i][0]<param.getwidth()-1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_notrightvertex,lfsusize);

        // free outflow Ca, may be flushed out by convection
        std::array<RF,2> nablaw = param.ReconstructFlow(pw,vertex);
        std::vector<RF> Kw(3);
        for (std::size_t i=0; i<lfsusize; ++i)
        {
          Kw[i] = param.Kw(Sa[i],phi[i]);
        }
        const RF avgKw = mojefunkcie::hNavg(Kw);
        const RF bflow = -avgKw*nablaw[0];
        if (bflow>0) // outflowing water contains Ca, inflowing does not -no term for bflow<0
        {
          // r_i.accumulate(lfsv_phi,active[0],face_volume/2*bflow*Ca[active[0]]);
          // r_i.accumulate(lfsv_phi,active[1],face_volume/2*bflow*Ca[active[1]]);
          mat_ii.accumulate(lfsv_phi,active[0],lfsu_phi,active[0],face_volume/2*bflow);
          mat_ii.accumulate(lfsv_phi,active[1],lfsu_phi,active[1],face_volume/2*bflow);
          std::array<std::array<RF,2>, 3> derw = param.ReconstructFlowDerived(vertex);
          for (std::size_t j=0; j<lfsusize; ++j)
          {
            // derive gradient pw
            mat_ii.accumulate(lfsv_phi,active[0],lfsu_Ca,j,face_volume/2*(-avgKw*derw[0][j])*Ca[active[0]]);
            mat_ii.accumulate(lfsv_phi,active[1],lfsu_Ca,j,face_volume/2*(-avgKw*derw[0][j])*Ca[active[1]]);
            // derive permeability by Sa
            RF davgKw_dj = avgKw*avgKw/(lfsusize*Kw[j]*Kw[j]);
            RF davgKw_dSaj = davgKw_dj*param.Kwr_deriv(Sa[j],phi[j])/param.getrho_w();
            mat_ii.accumulate(lfsv_phi,active[0],lfsu_co2,j,face_volume/2*(-davgKw_dSaj *nablaw[0])*Ca[active[0]]);
            mat_ii.accumulate(lfsv_phi,active[1],lfsu_co2,j,face_volume/2*(-davgKw_dSaj *nablaw[0])*Ca[active[1]]);
            // derive permeability by phi
            RF davgKw_dphij = davgKw_dj*param.dKwr_dphi(Sa[j],phi[j])/param.getrho_w();
            mat_ii.accumulate(lfsv_phi,active[0],lfsu_4,j,face_volume/2*(-davgKw_dphij*nablaw[0])*Ca[active[0]]);
            mat_ii.accumulate(lfsv_phi,active[1],lfsu_4,j,face_volume/2*(-davgKw_dphij*nablaw[0])*Ca[active[1]]);
          }
        }
      }
      // bottom
      else if (global[1]<=1e-7)
      {
        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        auto lambda_notbottomvertex = [&](std::size_t i) ->bool {return inside_geo.corner(i)[1]>1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_notbottomvertex,lfsusize);
        // water, air zero pressure derivation (gravity induced inflow)
        // r_i.accumulate(lfsv_Ca,active[0],
        //       face_volume/2*param.Kwr(Sa[active[0]],phi[active[0]])*(-param.getgrav()*param.getrho_w()) );
        // r_i.accumulate(lfsv_Ca,active[1],
        //       face_volume/2*param.Kwr(Sa[active[1]],phi[active[1]])*(-param.getgrav()*param.getrho_w()) );
        RF pom = face_volume/2*(-param.getgrav());
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_co2,active[0], pom*param.Kwr_deriv(Sa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_co2,active[1], pom*param.Kwr_deriv(Sa[active[1]],phi[active[1]]) );
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_4,active[0], pom*param.dKwr_dphi(Sa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_4,active[1], pom*param.dKwr_dphi(Sa[active[1]],phi[active[1]]) );
        // r_i.accumulate(lfsv_co2,active[0],
        //        face_volume/2*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*(-param.getgrav()*p_h.getrho_a(pa[active[0]],co2[active[0]])) );
        // r_i.accumulate(lfsv_co2,active[1],
        //        face_volume/2*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*(-param.getgrav()*p_h.getrho_a(pa[active[1]],co2[active[1]])) );
        // derive permeability by pw, Sa and phi
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_Ca,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()* p_h.getrho_a(pa[active[0]],co2[active[0]]) );
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_Ca,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()* p_h.getrho_a(pa[active[1]],co2[active[1]]) );
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_co2,active[0],pom*param.Kar_deriv(Sa[active[0]],pa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_co2,active[1],pom*param.Kar_deriv(Sa[active[1]],pa[active[1]],phi[active[1]]) );
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_4,active[0],pom*param.dKar_dphi(Sa[active[0]],pa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_4,active[1],pom*param.dKar_dphi(Sa[active[1]],pa[active[1]],phi[active[1]]) );
        // derive density from gravity term by pw and Sa
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_Ca,active[0],pom*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*param.rho_a_deriv());
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_Ca,active[1],pom*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*param.rho_a_deriv());
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_co2,active[0],pom*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[0]]));
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_co2,active[1],pom*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[1]]));

        // since \partial_y=0 for flow, flows are determined by gravity
        // canceled minus sign comes from unit outer normal vector
        RF bflow0 =  param.Kw(Sa[active[0]],phi[active[0]])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[active[1]],phi[active[1]])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        // r_i.accumulate(lfsv_phi,active[0],face_volume/2*bflow0*Ca[active[0]]);
        // r_i.accumulate(lfsv_phi,active[1],face_volume/2*bflow1*Ca[active[1]]);
        mat_ii.accumulate(lfsv_phi,active[0],lfsu_phi,active[0],face_volume/2*bflow0);
        mat_ii.accumulate(lfsv_phi,active[1],lfsu_phi,active[1],face_volume/2*bflow1);
        mat_ii.accumulate(lfsv_phi,active[0],lfsu_Ca,active[0],pom*param.Kwr_deriv(Sa[active[0]],phi[active[0]])*Ca[active[0]]);
        mat_ii.accumulate(lfsv_phi,active[1],lfsu_Ca,active[1],pom*param.Kwr_deriv(Sa[active[1]],phi[active[1]])*Ca[active[1]]);
        mat_ii.accumulate(lfsv_phi,active[0],lfsu_Ca,active[0],pom*param.dKwr_dphi(Sa[active[0]],phi[active[0]])*Ca[active[0]]);
        mat_ii.accumulate(lfsv_phi,active[1],lfsu_Ca,active[1],pom*param.dKwr_dphi(Sa[active[1]],phi[active[1]])*Ca[active[1]]);

        RF bfloa0 =  param.Ka(Sa[active[0]],phi[active[0]])*(-p_h.getrho_a(pa[active[0]],co2[active[0]])*param.getgrav());
        RF bfloa1 =  param.Ka(Sa[active[1]],phi[active[1]])*(-p_h.getrho_a(pa[active[1]],co2[active[1]])*param.getgrav());
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        // r_i.accumulate(lfsv_CaStored,active[0],face_volume/2*bfloa0*co2[active[0]]);
        // r_i.accumulate(lfsv_CaStored,active[1],face_volume/2*bfloa1*co2[active[1]]);
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_CaStored,active[0],face_volume/2*bfloa0);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_CaStored,active[1],face_volume/2*bfloa1);
        // derive permeability by Sa and phi
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_co2,active[0],pom*param.Kar_deriv(Sa[active[0]],pa[active[0]],phi[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_co2,active[1],pom*param.Kar_deriv(Sa[active[1]],pa[active[1]],phi[active[1]])*co2[active[1]]);
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_4,active[0],pom*param.dKar_dphi(Sa[active[0]],pa[active[0]],phi[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_4,active[1],pom*param.dKar_dphi(Sa[active[1]],pa[active[1]],phi[active[1]])*co2[active[1]]);
        // derive density from gravity term by pw and Sa
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_Ca,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()*co2[active[0]]);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_Ca,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()*co2[active[1]]);
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_co2,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_co2,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[1]])*co2[active[1]]);
      }
      // top
      else if (global[1]>=param.getheight()-1e-7)
      {
        // find indices of boundary vertices
        const auto& inside_geo = ig.inside().geometry();
        auto lambda_nottopvertex = [&](std::size_t i) ->bool {return inside_geo.corner(i)[1]<param.getheight()-1e-7;};
        std::array<std::size_t,2> active = mojefunkcie::setactive(lambda_nottopvertex,lfsusize);
        // water, air zero pressure derivation (gravity induced inflow)
        // r_i.accumulate(lfsv_Ca,active[0],
        //       face_volume/2*param.Kwr(Sa[active[0]],phi[active[0]])*(-param.getgrav()*param.getrho_w()) );
        // r_i.accumulate(lfsv_Ca,active[1],
        //       face_volume/2*param.Kwr(Sa[active[1]],phi[active[1]])*(-param.getgrav()*param.getrho_w()) );
        RF pom = -face_volume/2*(-param.getgrav());
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_co2,active[0],pom*param.Kwr_deriv(Sa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_co2,active[1],pom*param.Kwr_deriv(Sa[active[1]],phi[active[1]]) );
        mat_ii.accumulate(lfsv_Ca,active[0],lfsu_4,active[0],pom*param.dKwr_dphi(Sa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_Ca,active[1],lfsu_4,active[1],pom*param.dKwr_dphi(Sa[active[1]],phi[active[1]]) );
        // r_i.accumulate(lfsv_co2,active[0],
        //        face_volume/2*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*(-param.getgrav()*p_h.getrho_a(pa[active[0]],cow[active[0]])) );
        // r_i.accumulate(lfsv_co2,active[1],
        //        face_volume/2*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*(-param.getgrav()*p_h.getrho_a(pa[active[1]],cow[active[1]])) );
        // derive permeability by pw, Sa and phi
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_Ca,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()* p_h.getrho_a(pa[active[0]],cow[active[0]]) );
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_Ca,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()* p_h.getrho_a(pa[active[1]],cow[active[1]]) );
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_co2,active[0],pom*param.Kar_deriv(Sa[active[0]],pa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_co2,active[1],pom*param.Kar_deriv(Sa[active[1]],pa[active[1]],phi[active[1]]) );
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_4,active[0],pom*param.dKar_dphi(Sa[active[0]],pa[active[0]],phi[active[0]]) );
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_4,active[1],pom*param.dKar_dphi(Sa[active[1]],pa[active[1]],phi[active[1]]) );
        // derive density from gravity term by pw and Sa
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_Ca,active[0],pom*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*param.rho_a_deriv());
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_Ca,active[1],pom*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*param.rho_a_deriv());
        mat_ii.accumulate(lfsv_co2,active[0],lfsu_co2,active[0],pom*param.Kar(Sa[active[0]],pa[active[0]],phi[active[0]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[0]]));
        mat_ii.accumulate(lfsv_co2,active[1],lfsu_co2,active[1],pom*param.Kar(Sa[active[1]],pa[active[1]],phi[active[1]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[1]]));

        pom*=-1.; // water and air flow have different sign on top and bottom, other not...
                  // should be checked when gravity!=0, TODO!

        // since \partial_y=0 for flow, flows are determined by gravity
        // canceled minus sign comes from unit outer normal vector
        RF bflow0 =  param.Kw(Sa[active[0]],phi[active[0]])*(-param.getrho_w()*param.getgrav());
        RF bflow1 =  param.Kw(Sa[active[1]],phi[active[1]])*(-param.getrho_w()*param.getgrav());
        // sign of bflow determines if it is outflow or inflow, \partial_y Ca=0 guarantees correctness of Ca[i] weight
        // r_i.accumulate(lfsv_phi,active[0],face_volume/2*bflow0*Ca[active[0]]);
        // r_i.accumulate(lfsv_phi,active[1],face_volume/2*bflow1*Ca[active[1]]);
        mat_ii.accumulate(lfsv_phi,active[0],lfsu_phi,active[0],face_volume/2*bflow0);
        mat_ii.accumulate(lfsv_phi,active[1],lfsu_phi,active[1],face_volume/2*bflow1);
        mat_ii.accumulate(lfsv_phi,active[0],lfsu_Ca,active[0],pom*param.Kwr_deriv(Sa[active[0]],phi[active[0]])*Ca[active[0]]);
        mat_ii.accumulate(lfsv_phi,active[1],lfsu_Ca,active[1],pom*param.Kwr_deriv(Sa[active[1]],phi[active[1]])*Ca[active[1]]);
        mat_ii.accumulate(lfsv_phi,active[0],lfsu_Ca,active[0],pom*param.dKwr_dphi(Sa[active[0]],phi[active[0]])*Ca[active[0]]);
        mat_ii.accumulate(lfsv_phi,active[1],lfsu_Ca,active[1],pom*param.dKwr_dphi(Sa[active[1]],phi[active[1]])*Ca[active[1]]);

        RF bfloa0 =  param.Ka(Sa[active[0]],phi[active[0]])*(-p_h.getrho_a(pa[active[0]],co2[active[0]])*param.getgrav());
        RF bfloa1 =  param.Ka(Sa[active[1]],phi[active[1]])*(-p_h.getrho_a(pa[active[1]],co2[active[1]])*param.getgrav());
        // sign of bfloa determines if it is outflow or inflow, \partial_y CO_2=0 guarantees correctness of co2[i] weight
        // r_i.accumulate(lfsv_CaStored,active[0],face_volume/2*bfloa0*co2[active[0]]);
        // r_i.accumulate(lfsv_CaStored,active[1],face_volume/2*bfloa1*co2[active[1]]);
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_CaStored,active[0],face_volume/2*bfloa0);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_CaStored,active[1],face_volume/2*bfloa1);
        // derive permeability by Sa and phi
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_co2,active[0],pom*param.Kar_deriv(Sa[active[0]],pa[active[0]],phi[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_co2,active[1],pom*param.Kar_deriv(Sa[active[1]],pa[active[1]],phi[active[1]])*co2[active[1]]);
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_4,active[0],pom*param.dKar_dphi(Sa[active[0]],pa[active[0]],phi[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_4,active[1],pom*param.dKar_dphi(Sa[active[1]],pa[active[1]],phi[active[1]])*co2[active[1]]);
        // derive density from gravity term by pw and Sa
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_Ca,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()*co2[active[0]]);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_Ca,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()*co2[active[1]]);
        mat_ii.accumulate(lfsv_CaStored,active[0],lfsu_co2,active[0],pom*param.Ka(Sa[active[0]],phi[active[0]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[0]])*co2[active[0]]);
        mat_ii.accumulate(lfsv_CaStored,active[1],lfsu_co2,active[1],pom*param.Ka(Sa[active[1]],phi[active[1]])*param.rho_a_deriv()*param.pc_deriv(Sa[active[1]])*co2[active[1]]);
      }
    } // end lfsusize==3
  } // end jacobian_boundary */

}; // end LOP_spatial_contaminant

template<typename Param_chem, typename GFSF, typename ZF, typename GFSC=GFSF, typename ZC=ZF>
class LOP_time_contaminant
  : public Dune::PDELab::FullVolumePattern
  , public Dune::PDELab::LocalOperatorDefaultFlags
  , public Dune::PDELab::NumericalJacobianVolume<LOP_time_contaminant<Param_chem,GFSF,ZF,GFSC,ZC> >
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
  Param_chem& p_h;
  typename Param_chem::Param_wflow& param;
  using RF = typename Param_chem::value_type;
  CachedParam<GFSF,ZF,RF> datafnew; // stored values from current flow
  CachedParam<GFSF,ZF,RF> datafold; // stored values from previous flow
  CachedParam<GFSC,ZC,RF> datacold; // stored values from previous contaminant
  bool chemold;
public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };

  LOP_time_contaminant(Param_chem& param_chem_, const GFSF& gfsf_, ZF& zfnew_, ZF& zfold_)
    : p_h(param_chem_)
    , param(p_h.param)
    , datafnew(gfsf_,zfnew_)
    , datafold(gfsf_,zfold_)
    , datacold(gfsf_,zfold_)
    , chemold(false)
  {
  }

  LOP_time_contaminant(Param_chem& param_chem_, const GFSF& gfsf_, ZF& zfnew_, ZF& zfold_, const GFSC& gfsc_, ZC& zcold_)
    : p_h(param_chem_)
    , param(p_h.param)
    , datafnew(gfsf_,zfnew_)
    , datafold(gfsf_,zfold_)
    , datacold(gfsc_,zcold_)
    , chemold(true)
  {
  }

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X,
           typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, R& r) const
  {
    // types & dimension
    //const int dim = EG::Entity::dimension;
    // get access to each vector space
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_Ca = lfsu.child(_0);
    const auto& lfsu_co2 = lfsu.child(_1);
    const auto& lfsu_phi = lfsu.child(_2);
    const auto& lfsu_CaStored = lfsu.child(_3);
    const auto& lfsv_Ca = lfsv.child(_0);
    const auto& lfsv_co2 = lfsv.child(_1);
    const auto& lfsv_phi = lfsv.child(_2);
    const auto& lfsv_CaStored = lfsv.child(_3);
    const size_t & lfsusize = lfsu_Ca.size();
    // typedef decltype(makeZeroBasisFieldValue(lfsu_Ca)) RF;
    const auto& inside_cell = eg.entity();

    std::vector<RF> pw(lfsusize);
    std::vector<RF> Sa(lfsusize);
    std::vector<RF> Ca(lfsusize);
    std::vector<RF> co2(lfsusize);
    std::vector<RF> pa(lfsusize);
    std::vector<RF> phi(lfsusize);
    std::vector<RF> CaStored(lfsusize);
    // load flow data on the same time-level as contaminant data
    bool currentTimeStep = param.getTime()>param.getOtherTime();
    const CachedParam<GFSF,ZF,RF>& dat = currentTimeStep ? datafnew : datafold;
    // const CachedParam<GFSF,ZF,RF>& dat = datafold;
    std::vector<RF> clf=dat.read(inside_cell);

    if (chemold && (!currentTimeStep))
    {
      std::vector<RF> clc=datacold.read(inside_cell);
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        Ca[i] = clc[lfsusize*0+i];
        co2[i] = clc[lfsusize*1+i];
        phi[i] = clc[lfsusize*2+i];
        CaStored[i] = clc[lfsusize*3+i];
      }
    }
    else
    {
      for (std::size_t i=0; i<lfsusize; ++i)
      {
        Ca[i] = x(lfsu_Ca,i);
        co2[i] = x(lfsu_co2,i);
        phi[i] = x(lfsu_phi,i);
        CaStored[i] = x(lfsu_CaStored,i);
      }
    }
    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pw[i] = clf[lfsusize*0+i];
      Sa[i] = clf[lfsusize*1+i];
      pa[i] = pw[i]+param.pc(Sa[i]);
    }

    const RF volume = eg.geometry().volume()/RF(lfsusize);
    for (std::size_t i=0; i<lfsusize; ++i)
      {
        r.accumulate(lfsv_Ca,i, phi[i]*(1.-Sa[i])*Ca[i]*volume);
        r.accumulate(lfsv_co2,i, phi[i]*std::max(Sa[i],1e-16)*(pa[i]+param.getpa0())*co2[i]*volume); // regularization, else matrix is singular
        // in terms of partial pressure (not concentration) r.accumulate(lfsv_CaStored,i,phi[i]*std::max(Sa[i],1e-16)*co2*volume); // regularization, else matrix is singular
        r.accumulate(lfsv_phi,i, phi[i]     *volume);
        r.accumulate(lfsv_CaStored,i, CaStored[i]*volume);

        // // leaching, now moved to spatial term to make backward "leaching" impossible,
        // // only negative Ca derivations in time should be enabled (positive won't generate leaching)
        // RF ksl = p_h.getleachspeed(1.-Sa,Ca);
        // r.accumulate(lfsv_Ca,i, ksl*p_h.getMh2o()*p_h.getwls(Ca)*volume);
        // r.accumulate(lfsv_phi,i, ksl*p_h.getCaC(Ca)*volume);
        // r.accumulate(lfsv_4,i, ksl*p_h.getVdt(Ca)*volume); // strengthen binding? write phi+CaStored instead of por[i]-leaching*Ca?
        // r.accumulate(lfsv_5,i,-ksl*p_h.getVdt(Ca)*volume);
        // // porosity describes the fraction of empty volume, CaStored is the volume of unreacted Ca compounds
        // // therefore leaching affects porosity and CaStored in exactly opposite way, the amount of lost CaStored is reflected in porosity change
      }
  }

  //! jacobian contribution of volume term
  template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
  void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                        M& mat) const
  {
    // types & dimension
    //const int dim = EG::Entity::dimension;
    // get access to each vector space
    assert(LFSU::CHILDREN==4);
    using namespace Dune::TypeTree::Indices;
    const auto& lfsu_Ca = lfsu.child(_0);
    const auto& lfsu_co2 = lfsu.child(_1);
    const auto& lfsu_phi = lfsu.child(_2);
    const auto& lfsu_CaStored = lfsu.child(_3);
    const auto& lfsv_Ca = lfsv.child(_0);
    const auto& lfsv_co2 = lfsv.child(_1);
    const auto& lfsv_phi = lfsv.child(_2);
    const auto& lfsv_CaStored = lfsv.child(_3);
    const size_t & lfsusize = lfsu_Ca.size();
    // typedef decltype(makeZeroBasisFieldValue(lfsu_Ca)) RF;
    const auto& inside_cell = eg.entity();

    std::vector<RF> pw(lfsusize);
    std::vector<RF> Sa(lfsusize);
    std::vector<RF> Ca(lfsusize);
    std::vector<RF> co2(lfsusize);
    std::vector<RF> pa(lfsusize);
    std::vector<RF> phi(lfsusize);
    std::vector<RF> CaStored(lfsusize);
    // load flow data on the same time-level as contaminant data
    const CachedParam<GFSF,ZF,RF>& dat = param.getTime()>param.getOtherTime() ? datafnew : datafold;
    std::vector<RF> clf=dat.read(inside_cell);

    for (std::size_t i=0; i<lfsusize; ++i)
    {
      pw[i] = clf[lfsusize*0+i];
      Sa[i] = clf[lfsusize*1+i];
      pa[i] = pw[i]+param.pc(Sa[i]);
      Ca[i] = x(lfsu_Ca,i);
      co2[i] = x(lfsu_co2,i);
      phi[i] = x(lfsu_phi,i);
      CaStored[i] = x(lfsu_CaStored,i);
    }
    const RF volume = eg.geometry().volume()/RF(lfsusize);

    for (std::size_t i=0; i<lfsusize; ++i)
      {
        mat.accumulate(lfsv_Ca,i,lfsu_Ca,i, phi[i]*(1.-Sa[i])*volume);
        mat.accumulate(lfsv_Ca,i,lfsu_phi,i, Ca[i] *(1.-Sa[i])*volume );

        mat.accumulate(lfsv_co2,i,lfsu_co2,i, phi[i]*std::max(Sa[i],1e-16)*(pa[i]+param.getpa0())*volume);
        mat.accumulate(lfsv_co2,i,lfsu_phi,i, co2[i]*std::max(Sa[i],1e-16)*(pa[i]+param.getpa0())*volume);

        mat.accumulate(lfsv_phi,i,lfsu_phi,i, volume);
        mat.accumulate(lfsv_CaStored,i,lfsu_CaStored,i, volume);

        // // leaching -moved to spatial term
        // RF ksl = p_h.getleachspeed(1.-Sa,Ca);
        // RF dksl_dSa = p_h.dleachspeed_dSw(1.-Sa,Ca)*(-1.);
        // RF dksl_dCa = p_h.dleachspeed_dCa(1.-Sa,Ca);
        // // r.accumulate(lfsv_Ca,i,                +ksl*p_h.getMh2o()*p_h.getwls(Ca)*volume);
        // mat.accumulate(lfsv_Ca,i,lfsu_co2,i,- dksl_dSa*p_h.getMh2o()*p_h.getwls(Ca)*volume);
        // mat.accumulate(lfsv_Ca,i,lfsu_phi,i,-(dksl_dCa*p_h.getwls(Ca)+ksl*p_h.dwls_dCa(Ca))*p_h.getMh2o()*volume);
        // // r.accumulate(lfsv_phi,i, ksl*p_h.getCaC(Ca)*volume);
        // mat.accumulate(lfsv_phi,i,lfsu_co2,i, dksl_dSa*p_h.getCaC(Ca)*volume);
        // mat.accumulate(lfsv_phi,i,lfsu_phi,i, dksl_dCa*p_h.getCaC(Ca)*volume+ksl*p_h.dCaC_dCa(Ca)*volume );
        // // r.accumulate(lfsv_4,i, phi     *volume+ksl*p_h.getVdt(Ca)*volume);
        // mat.accumulate(lfsv_4,i,lfsu_co2,i,  dksl_dSa*p_h.getVdt(Ca)*volume);
        // mat.accumulate(lfsv_4,i,lfsu_phi,i, (dksl_dCa*p_h.getVdt(Ca)+ksl*p_h.dVdt_dCa(Ca))*volume);
        // // r.accumulate(lfsv_5,i, CaStored*volume-ksl*p_h.getVdt(Ca)*volume);
        // mat.accumulate(lfsv_5,i,lfsu_co2,i, - dksl_dSa*p_h.getVdt(Ca)*volume);
        // mat.accumulate(lfsv_5,i,lfsu_phi,i, -(dksl_dCa*p_h.getVdt(Ca)+ksl*p_h.dVdt_dCa(Ca))*volume);
        // // porosity describes the fraction of empty volume, CaStored is the volume of unreacted Ca compounds
        // // therefore leaching affects porosity and CaStored in exactly opposite way, the amount of lost CaStored is reflected in porosity change
      }
  } // end jac-vol */
}; // end LOP_time_contaminant
